import derelict.sdl2.sdl;
import derelict.sdl2.mixer;

import std.conv;
import std.string;

import graphics.window;
import graphics.image;
import levels.levelmanager;
import gamedata;
import config;
import drawers.text;
import system.fpscounter;
import player;
import music;
import inputhandler;
import gamecore;
import std.stdio;


class GameManager
{
    private:
    Window window;
    SDL_Renderer* renderer;
    SDL_Event* mainEvent;
    TextDrawer textDrawer;
    GameData gameData;
    LevelManager levelManager;
    static const int MIN_FRAME_TIME = 16;
    static bool quitGame = false;
    long frameTime;

    // Fps counter widget
    FpsCounter fpsCounter;

    Image FONT;

    TextDrawer myText;

    // ----- INPUT
    static bool movePressed, keyMenuPressed, keyS, keyW, keyA, keyD, keyShift;
    static bool keyAPressed, keyDPressed;

    // ----- true = RIGHT, false = LEFT
    bool firstDir;

    public:

    this(Window window)
    {
        this.window = window;
        this.renderer = SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED);
        this.textDrawer = new TextDrawer(this.renderer, "font");
        this.fpsCounter = new FpsCounter(this.textDrawer);
        Player player  = new Player(this.renderer, to!float(84), to!float(368));
        this.gameData = new GameData(this.renderer, player);
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
        //SDL_RenderSetLogicalSize(this.renderer, 800, 600);

        mainEvent = new SDL_Event();

        InputHandler ih = new InputHandler(mainEvent);
        this.levelManager = new LevelManager(this.gameData, this.textDrawer, this.renderer, ih);

        Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
        Music music = new Music();

        Core.instance().setTextDrawer(this.textDrawer);
        Core.instance().setGameData(this.gameData);
        Core.instance().setLevelManager(this.levelManager);
        Core.instance().setRenderer(this.renderer);
        Core.instance().setMusic(music);
        Core.instance().setPlayer(player);
        //music.changeMusic(true, true);
        // music.PlayMusic();
    }

    ~this()
    {
        SDL_DestroyRenderer(this.renderer);
        destroy(this.window);
    }

    SDL_Renderer* getRenderer()
    {
        return this.renderer;
    }

    void mainLoop()
    {
        this.levelManager.loadLevel();

        while(!quitGame && mainEvent.type != SDL_QUIT) {
            frameTime = SDL_GetTicks();
            //SDL_PollEvent(mainEvent);
            SDL_RenderClear(this.renderer);

              while (SDL_PollEvent(mainEvent)) {
                if (this.levelManager.getCurrentLevel().getID() != 0) {
                    inputPlayer();
                }
              }

            this.update();
            this.draw();

            if (GameConfig.showFpsCounter)
                this.fpsCounter.draw();

            SDL_RenderPresent(this.renderer);

            if(SDL_GetTicks() - frameTime < MIN_FRAME_TIME) {
                SDL_Delay(to!uint(MIN_FRAME_TIME - (SDL_GetTicks () - frameTime)));
            }
        }
    }

    void inputPlayer()
    {
      	// if(mainEvent.type == SDL_WINDOWEVENT) {
      	// 	switch(mainEvent.window.event) {
      	// 		case SDL_WINDOWEVENT_FOCUS_LOST:
      	// 			//CCFG::getMM().resetActiveOptionID(CCFG::getMM().ePasue);
      	// 			//CCFG::getMM().setViewID(CCFG::getMM().ePasue);
      	// 			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cPASUE);
      	// 			Core.instance().getMusic().PauseMusic();
      	// 			break;
        //       default: break;
      	// 	}
      	// }

      	if(mainEvent.type == SDL_KEYUP) {
      		if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDD) {
      				if(firstDir) {
      					firstDir = false;
      				}
      				keyDPressed = false;
      			}

      			if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDS) {
      				Core.instance().getPlayer().setSquat(false);
      				keyS = false;
      			}

      			if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDA) {
      				if(!firstDir) {
      					firstDir = true;
      				}

      				keyAPressed = false;
      			}

      			if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDSpace) {
      				GameConfig.keySpace = false;
      			}

      			if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDShift) {
      				if(keyShift) {
      					Core.instance().getPlayer().resetRun();
      					keyShift = false;
      				}
      			}
      		switch(mainEvent.key.keysym.sym) {
      			case SDLK_KP_ENTER: case SDLK_RETURN: case SDLK_ESCAPE:
      				keyMenuPressed = false;
      				break;
              default: break;
      		}
      	}

      	if(mainEvent.type == SDL_KEYDOWN) {
      		if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDD) {
      			keyDPressed = true;

      			if(!keyAPressed) {
      				firstDir = true;
      			}
      		}

      		if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDS) {
      			if(!keyS) {
      				keyS = true;
              Core.instance().getPlayer().setSquat(true);
      				if(!Core.instance().getLevel().getUnderWater() && !Core.instance().getPlayer().getInLevelAnimation()) Core.instance().getPlayer().setSquat(true);
      			}
      		}

      		if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDA) {
      			keyAPressed = true;
      			if(!keyDPressed) {
      				firstDir = false;
      			}
      		}

      		if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDSpace) {
      			if(!GameConfig.keySpace) {
      				Core.instance().getPlayer().jump();
      				GameConfig.keySpace = true;
      			}
      		}

      		if(mainEvent.key.keysym.sym == GameConfig.controls.keyIDShift) {
      			if(!keyShift) {
      				Core.instance().getPlayer().startRun();
      				keyShift = true;
      			}
      		}

      		switch(mainEvent.key.keysym.sym) {
      			case SDLK_KP_ENTER:
            case SDLK_RETURN:
      				if(!keyMenuPressed) {
      					//CCFG::getMM().enter();
      					keyMenuPressed = true;
      				}
            break;
      			case SDLK_ESCAPE:
              writeln("WEq");
      				// if(!keyMenuPressed && CCFG::getMM().getViewID() == CCFG::getMM().eGame) {
      				// 	// CCFG::getMM().resetActiveOptionID(CCFG::getMM().ePasue);
      				// 	// CCFG::getMM().setViewID(CCFG::getMM().ePasue);
      				// 	Core.instance().getMusic().PlayChunk(Core.instance().getMusic().cPASUE);
      				// 	Core.instance().getMusic().PauseMusic();
      				// 	keyMenuPressed = true;
      				// }
      				break;
              default: break;
      	}

      	if(keyAPressed) {
      		if(!Core.instance().getPlayer().getMove() && firstDir == false && !Core.instance().getPlayer().getChangeMoveDirection() && !Core.instance().getPlayer().getSquat()) {
      			Core.instance().getPlayer().startMove();
      			Core.instance().getPlayer().setMoveDirection(false);
      		} else if(!keyDPressed && Core.instance().getPlayer().getMoveSpeed() > 0 && firstDir != Core.instance().getPlayer().getMoveDirection()) {
      			Core.instance().getPlayer().setChangeMoveDirection();
      		}
      	}

      	if(keyDPressed) {
      		if(!Core.instance().getPlayer().getMove() && firstDir == true && !Core.instance().getPlayer().getChangeMoveDirection() && !Core.instance().getPlayer().getSquat()) {
      			Core.instance().getPlayer().startMove();
      			Core.instance().getPlayer().setMoveDirection(true);
      		} else if(!keyAPressed && Core.instance().getPlayer().getMoveSpeed() > 0 && firstDir != Core.instance().getPlayer().getMoveDirection()) {
      			Core.instance().getPlayer().setChangeMoveDirection();
      		}
      	}
      }

      if(Core.instance().getPlayer().getMove() && !keyAPressed && !keyDPressed) {
        Core.instance().getPlayer().resetMove();
      }

    }

    void iputMenu()
    {

    }


    void draw()
    {
        this.copyright();
        this.levelManager.draw();
    }

    void update()
    {
        this.levelManager.update();
    }

    void copyright()
    {
        this.textDrawer.draw("BY MEIRELES (C) NINTENDO", 4, GameConfig.window.HEIGHT - 4 - 8, 8, 0, 0, 0);
        this.textDrawer.draw("BY MEIRELES (C) NINTENDO", 5, GameConfig.window.HEIGHT - 5 - 8, 8, 255, 255, 255);
    }
}
