module enemies.goomba;

import derelict.sdl2.sdl;
import graphics.minion;
import graphics.image;
import gamecore;
import std.conv;

class Goombas : Minion
{
public:
	this(int iX, int iY, int iBlockID, bool moveDirection)
  {
    this.fXPos = cast(float)iX;
    this.fYPos = cast(float)iY;
    this.iBlockID = iBlockID;
    this.moveDirection = moveDirection;
    this.moveSpeed = 1;
  }

	override void update()
  {
		if (minionState == 0) {
			updateXPos();
		} else if(minionState == -2) {
			this.minionDeathAnimation();
		} else if (SDL_GetTicks() - 500 >= cast(uint)deadTime) {
			minionState = -1;
		}
  }

	override void draw(Image iIMG)
  {
		if(minionState != -2) {
			iIMG.draw(to!int(fXPos + Core.instance().getLevel().getXPos()), to!int(fYPos + 2), false);
		} else {
			iIMG.drawVert(to!int(fXPos +  Core.instance().getLevel().getXPos()), to!int(fYPos) + 2);
		}
  }

	override void collisionWithPlayer(bool TOP)
  {
			if(Core.instance().getPlayer().getStarEffect()) {
				setMinionState(-2);
			} else if(TOP) {
				if(minionState == 0) {
					minionState = 1;
					iBlockID = Core.instance().getLevelManager().getLevelType() == 0 || Core.instance().getLevelManager().getLevelType() == 4 ? 1 : Core.instance().getLevelManager().getLevelType() == 1 ? 9 : 11;
					deadTime = SDL_GetTicks();
					Core.instance().getPlayer().resetJump();
					Core.instance().getPlayer().startJump(1);
					points(100);
					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cSTOMP);
				}
			} else {
				Core.instance().getLevel().playerDeath(true, false);
		}
  }

	override void setMinionState(int minionState)
  {
		this.minionState = minionState;

		if (this.minionState == 1) {
			deadTime = SDL_GetTicks();
		}

		super.setMinionState(minionState);
  }

	override void updateYPos(int iN)
	{

	}

};
