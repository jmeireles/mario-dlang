module graphics.sprite;

import derelict.sdl2.sdl;
import std.conv;
import graphics.image;

class Sprite
{

private:
	Image[] tSprite;
	uint[] iDelay;
	bool bRotate;
	int iCurrentFrame;
	int iStartFrame;
	int iEndFrame;
	ulong lTimePassed;

public:

	this(){}

  this(SDL_Renderer* rR, string[] tSprite, uint[] iDelay, bool bRotate)
  {
    this.iDelay = iDelay;
    this.bRotate = bRotate;

    this.iCurrentFrame = 0;
    this.iStartFrame = 0;
    this.iEndFrame = to!int(tSprite.length) - 1;

    foreach (string sprite; tSprite) {
      this.tSprite ~= new Image(sprite, rR);
    }

    this.lTimePassed = 0;
  }

	~this()
  {
      foreach (Image sprite; this.tSprite) {
          destroy(sprite);
      }
  }

	void update()
  {
    if(SDL_GetTicks() - iDelay[iCurrentFrame] > lTimePassed) {
      lTimePassed = SDL_GetTicks();

      if(iCurrentFrame == iEndFrame) {
        iCurrentFrame = 0;
      } else {
        ++iCurrentFrame;
      }
    }
  }

	Image getTexture()
  {
      return this.tSprite[this.iCurrentFrame];
  }

	Image getTexture(int iID)
  {
    	return this.tSprite[iID];
  }

};
