module graphics.block;

import graphics.sprite;
import derelict.sdl2.sdl;

class Block
{
private :
	Sprite sSprite;
	int iBlockID;
	bool bCollision;
	bool bDeath;
	bool bUse;
	bool bVisible;

public:
	this() {}
	this(int iBlockID, Sprite sSprite, bool bCollision, bool bDeath, bool bUse, bool bVisible)
  {
    this.iBlockID = iBlockID;
    this.sSprite = sSprite;
    this.bCollision = bCollision;
    this.bDeath = bDeath;
    this.bUse = bUse;
    this.bVisible = bVisible;
  }

	~this()
  {
     destroy(this.sSprite);
  }

	void draw(int iOffsetX, int iOffsetY)
  {
    	this.sSprite.getTexture().draw(iOffsetX, iOffsetY);
  }

	/* ----- get & set ----- */
	int getBlockID()
  {
      return this.iBlockID;
  }

	void setBlockID(int iID)
  {
      this.iBlockID = iID;
  }

	Sprite getSprite()
  {
      return this.sSprite;
  }

	bool getCollision()
  {
      return this.bCollision;
  }

	bool getDeath()
  {
      return this.bDeath;
  }

	bool getUse()
  {
      return this.bUse;
  }

	bool getVisible()
  {
      return this.bVisible;
  }

};
