module graphics.points;

import gamecore;

class Points
{
private:
	int iXPos, iYPos, iYLEFT;
	string sText;
	int iW, iH;
	bool bDelete;

public:
	this(int iXPos, int iYPos, string sText)
  {
    this.iXPos = iXPos;
    this.iYPos = iYPos;
    this.sText = sText;
    this.iYLEFT = 96;

    this.iW = 8;
    this.iH = 16;

    this.bDelete = false;
  }

	this(int iXPos, int iYPos, string sText, int iW, int iH)
  {
    this.iXPos = iXPos;
    this.iYPos = iYPos;
    this.sText = sText;
    this.iYLEFT = 96;

    this.iW = iW;
    this.iH = iH;

    this.bDelete = false;
  }

	void update()
  {
    if(iYLEFT > 0) {
  		iYPos -= 2;
  		iYLEFT -= 2;
  	} else {
  		bDelete = true;
  	}
  }

	void draw()
  {
      Core.instance().getTextDrawer().draw( sText, iXPos + cast(int)Core.instance().getLevel().getXPos(), iYPos, iW, iH);
  }

	bool getDelete()
  {
    	return bDelete;
  }
};
