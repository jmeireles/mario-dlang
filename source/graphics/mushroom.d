module graphics.mushroom;

import graphics.minion;
import graphics.image;
import gamecore;
import std.stdio;

class Mushroom : Minion
{
protected:
	bool inSpawnState;
	int inSpawnY;
	bool powerUP;
	int iX, iY; // inSpawnState draw Block

public:
	this(int iXPos, int iYPos, bool powerUP, int iX, int iY)
  {
    this.fXPos = cast(float)iXPos;
    this.fYPos = cast(float)iYPos - 2;

    this.iBlockID = powerUP ? 2 : Core.instance().getLevelManager().getLevelType() == 1 ? 25 : 3;
    this.moveSpeed = 2;
    this.inSpawnState = true;
    this.minionSpawned = true;
    this.inSpawnY = 30;
    this.moveDirection = false;
    this.powerUP = powerUP;
    this.collisionOnlyWithPlayer = true;

    this.minionState = 0;

    this.iX = iX;
    this.iY = iY;
  }

	override void update()
  {
    if (inSpawnState) {
  		if (inSpawnY <= 0) {
  			inSpawnState = false;
  		} else {
  			if (fYPos > -5) {
  				inSpawnY -= 2;
  				fYPos -= 2;
  			} else {
  				inSpawnY -= 1;
  				fYPos -= 1;
  			}
  		}
  	} else {
  		updateXPos();
  	}
  }

	override bool updateMinion()
  {
    if (!inSpawnState) {
  		minionPhysics();
  	}

  	return minionSpawned;
  }

	override void draw(Image iIMG)
  {
    if(minionState >= 0) {
  		iIMG.draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos + 2, false);
  		if (inSpawnState) {
  			Core.instance().getGameData().getBlock(Core.instance().getLevel().getLevelType() == 0 || Core.instance().getLevel().getLevelType() == 4 ? 9 : 56).getSprite().getTexture().draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos + (32 - inSpawnY) - Core.instance().getLevel().getMapBlock(iX, iY).getYPos() + 2, false);
  		}
  	}
  }
	import std.stdio;
	override void collisionWithPlayer(bool TOP)
  {
    if(!inSpawnState && minionState == 0) {

      if(powerUP) {
        Core.instance().getPlayer().setPowerLVL(Core.instance().getPlayer().getPowerLVL() + 1);
      } else {
        Core.instance().getPlayer().setNumOfLives(Core.instance().getPlayer().getNumOfLives() + 1);
        Core.instance().getLevel().addPoints(cast(int)fXPos, cast(int)fYPos, "1UP", 10, 14);
        Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cONEUP);
      }
      minionState = -1;
    }
  }

	override void setMinionState(int minionState)
  {

  }
};
