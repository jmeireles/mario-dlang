module graphics.playerfireball;

import graphics.minion;
import gamecore;
import config;
import graphics.image;

class PlayerFireBall : Minion
{
private:
	bool bDestroy;
	int destroyFrameID;
public:
this(int iXPos, int iYPos, bool moveDirection)
{
  this.fXPos = cast(float)iXPos;
	this.fYPos = cast(float)iYPos;

	this.moveDirection = moveDirection;

	this.killOtherUnits = true;

	this.minionSpawned = true;

	this.moveSpeed = 14;

	this.iBlockID = 62;

	this.jumpState = 2;

	this.iHitBoxX = 16;
	this.iHitBoxY = 16;

	this.bDestroy = false;

	this.destroyFrameID = 15;
}

override void minionPhysics()
{
	 if(!bDestroy) {
		   super.minionPhysics();
	 }
}

override void update()
{
  if(bDestroy) {
		if(destroyFrameID > 10) {
			this.iBlockID = 63;
		} else if(destroyFrameID > 5) {
			this.iBlockID = 64;
		} else if(destroyFrameID > 0) {
			this.iBlockID = 65;
		} else {
			minionState = -1;
		}
		--destroyFrameID;
	} else {
		if(jumpState == 0) {
			jumpState = 1;
			currentJumpSpeed = startJumpSpeed;
			jumpDistance = 34.0f;
			currentJumpDistance = 0;
		} else if(jumpState == 1) {
			updateYPos(-1);
			currentJumpDistance += 1;

			if (jumpDistance <= currentJumpDistance) {
				jumpState = 2;
			}
		} else {
			if (!Core.instance().getLevel().checkCollisionLB(cast(int)fXPos + 2,cast (int)fYPos + 2, iHitBoxY, true) && !Core.instance().getLevel().checkCollisionRB(cast(int)fXPos - 2, cast(int)fYPos + 2, iHitBoxX, iHitBoxY, true) && !onAnotherMinion) {
				updateYPos(1);

				jumpState = 2;

				if (fYPos >= GameConfig.window.HEIGHT) {
					minionState = -1;
				}
			} else {
				jumpState = 0;
				onAnotherMinion = false;
			}
		}

		updateXPos();
	}
}

override void updateXPos()
{
	// ----- LEFT
	if (moveDirection) {
		if (Core.instance().getLevel().checkCollisionLB(cast(int)fXPos - moveSpeed, cast(int)fYPos - 2, iHitBoxY, true) || Core.instance().getLevel().checkCollisionLT(cast(int)fXPos - moveSpeed, cast(int)fYPos + 2, true)) {
			bDestroy = true;
			collisionOnlyWithPlayer = true;
			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cBLOCKHIT);
		} else {
			fXPos -= (jumpState == 0 ? moveSpeed : moveSpeed/2.0f);
		}
	}
	// ----- RIGHT
	else {
		if (Core.instance().getLevel().checkCollisionRB(cast(int)fXPos + moveSpeed, cast(int)fYPos - 2, iHitBoxX, iHitBoxY, true) || Core.instance().getLevel().checkCollisionRT(cast(int)fXPos + moveSpeed, cast(int)fYPos + 2, iHitBoxX, true)) {
			bDestroy = true;
			collisionOnlyWithPlayer = true;
			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cBLOCKHIT);
		} else {
			fXPos += (jumpState == 0 ? moveSpeed : moveSpeed/2.0f);
		}
	}

	if(fXPos < -iHitBoxX) {
		minionState = -1;
	}
}

override void draw(Image iIMG)
{
  if(!bDestroy) {
		iIMG.draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos, !moveDirection);
	} else {
		iIMG.draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos() - (moveDirection ? 16 : 0), cast(int)fYPos - 8, !moveDirection);
	}
}

override void collisionWithAnotherUnit() {
	bDestroy = true;
	collisionOnlyWithPlayer = true;
}

override void collisionEffect() {

}


override void collisionWithPlayer(bool TOP)
{
}

override void setMinionState(int minionState)
{

}
};
