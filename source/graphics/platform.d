module graphics.platform;

import std.conv;
import gamecore;
import config;

class Platform
{
private:
	// -- 0 = BOT, 1 = TOP, 2 = TOP & BOT, 3 = RIGHT & LEFT, 4 = BONUS, 5 = FALLING, 6 = SEESAW, 7 = FALLING SEESAW
	int iType;
	int iXStart, iXEnd, iYStart, iYEnd;
	float fXPos, fYPos;

	// -- true = RIGHT | TOP, false = LEFT | BOT
	bool direction;

	int iSize;

	bool ON; // -- iType = 4, BONUS, OFF & ON

	int seesawPlatformID;
public:
	this(int iSize, int iType, int iXStart, int iXEnd, int iYStart, int iYEnd, float fXPos, float fYPos, bool direction, int seesawPlatformID = 0)
  {
    this.iSize = iSize;
    this.iType = iType;
    this.iXStart = iXStart;
    this.iXEnd = iXEnd;
    this.iYStart = iYStart;
    this.iYEnd = iYEnd;
    this.fXPos = fXPos;
    this.fYPos = fYPos;

    this.direction = direction;
    this.seesawPlatformID = seesawPlatformID;

    if(iType == 6) {
      this.iXStart = to!int(fYPos);
    }

    this.ON = false;
  }

	void update()
  {
    switch(iType) {
      case 0:
        if(fYPos > iYEnd) {
          fYPos = cast(float)iYStart;
        } else {
          fYPos += 2;
        }
        break;
      case 1:
        if(fYPos < iYStart) {
          fYPos = cast(float)iYEnd;
        } else {
          fYPos -= 2;
        }
        break;
      case 2: // -- TOP & BOT
        if(direction) {
          if(fYPos < iYStart) {
            direction = false;
          } else if(fYPos < iYStart + 32) {
            fYPos -= 1;
          } else {
            fYPos -= 2;
          }
        } else {
          if(fYPos > iYEnd) {
            direction = true;
          } else if(fYPos > iYEnd - 32) {
            fYPos += 1;
          } else {
            fYPos += 2;
          }
        }
        break;
      case 3: // -- LEFT & RIGHT
        if(direction) {
          if(fXPos > iXEnd) {
            direction = false;
          }
          else if(fXPos < iXStart + 16 || fXPos + 16 > iXEnd) {
            fXPos += 0.5f;
          } else {
            fXPos += 1;
          }
        } else {
          if(fXPos < iXStart) {
            direction = true;
          } else if(fXPos - 16 < iXStart || fXPos + 16 > iXEnd) {
            fXPos -= 0.5f;
          } else {
            fXPos -= 1;
          }
        }
        break;
      case 4:
        if(fXPos > iXEnd) {
          iSize = 0;
        } else if(ON) {
          fXPos += 3;
        }
        break;
      case 7:
        if(fYPos < GameConfig.window.HEIGHT + 32) {
          fYPos += 4;
        }
        break;
      default: break;
    }
  }

	void draw()
  {
    if(iType == 6 || iType == 7) { // -- iXStart = YPos 2
  		for(int i = 1; i < (iXStart - iYEnd)/16 + 2; i++) {
  			Core.instance().getGameData().getBlock(Core.instance().getLevel().getID() == 22 ? 165 : 135).getSprite().getTexture().draw(cast(int)(iXEnd + cast(int)Core.instance().getLevel().getXPos() + (iSize*8)/2 + 16 - (iSize/2%2 == 0 ? 8 : 0)), cast(int)(iXStart - 16*i));
  		}
  	}

  	for(int i = 0; i < iSize; i++) {
  		Core.instance().getGameData().getBlock(iType != 4 ? 74 : 126).draw(to!int(fXPos) + i*16 + cast(int)Core.instance().getLevel().getXPos(), to!int(fYPos));
  	}
  }

	void moveY()
  {
    switch(iType) {
  		case 5:
  			fYPos += 2;
  			break;
  		case 6:
  			fYPos += 2;
  			iXStart = to!int(fYPos);
  			Core.instance().getLevel().getPlatform(seesawPlatformID).moveYReverse();
  			if(fYPos < iYEnd) {
  				iType = 7;
  				Core.instance().getLevel().getPlatform(seesawPlatformID).setTypeID(7);
  			}
  			break;
      default: break;

  	}
  }

	void moveYReverse()
  {
    switch(iType) {
  		case 6:
  			fYPos -= 2;
  			iXStart = to!int(fYPos);
  			if(fYPos < iYEnd) {
  				iType = 7;
  				Core.instance().getLevel().getPlatform(seesawPlatformID).setTypeID(7);
  			}
  			break;
        default: break;
  	}
  }

	float getMoveX()
  {
    // switch(iType) {
  	// 	case 3:
  	// 		if(direction) {
  	// 			if(fXPos < iXStart + 16 || fXPos + 16 > iXEnd) {
  	// 				return 0.5f;
  	// 			} else {
  	// 				return 1;
  	// 			}
  	// 		} else {
  	// 			if(fXPos - 16 < iXStart || fXPos + 16 > iXEnd) {
  	// 				return - 0.5f;
  	// 			} else {
  	// 				return -1;
  	// 			}
  	// 		}
  	// 		break;
  	// 	case 4:
  	// 		return 3;
    //   default: break;
  	// }

  	return 0;
  }

	int getMoveY()
  {
    switch(iType) {
  		case 0:
  			return 2;
  		case 1:
  			return -2;
  		case 2:
  			if(direction) {
  				if(fYPos < iYStart + 32) {
  					return -1;
  				} else {
  					return -2;
  				}
  			} else {
  				if(fYPos > iYEnd - 32) {
  					return 1;
  				} else {
  					return 2;
  				}
  			}
  		case 5:
  			return 2;
  		case 6:
  			return 2;
  		case 7:
  			return 4;
      default: break;
  	}

  	return 0;
  }

  int getXPos()
  {
  	return to!int(fXPos);
  }

  int getYPos()
  {
  	return to!int(fYPos);
  }

  int getSize()
  {
  	return iSize;
  }

  void setTypeID(int iType)
  {
  	this.iType = iType;
  }

  int getTypeID()
  {
  	return iType;
  }

  void turnON()
  {
  	this.ON = true;
  }

};
