module graphics.flag;

import derelict.sdl2.sdl;
import graphics.image;
import gamecore;
import config;
import std.conv;

class Flag
{
	int iXPos, iYPos;
	int iBlockID;

	int iYTextPos;
	int iPoints;

	int castleFlagExtraXPos;
	int castleFlagY;

	this(int iXPos, int iYPos)
  {
    this.iXPos = iXPos;
    this.iYPos = iYPos;

    this.iYTextPos = 448 - 3*32;
    this.iPoints = -1;

    this.iBlockID = 168;

    this.castleFlagExtraXPos = this.castleFlagY = 0;
  }

	void update()
  {
		iYPos += 4;
		iYTextPos -= 4;
  }

	void updateCastleFlag()
  {
    if(castleFlagY <= 56)
    	castleFlagY += 2;
  }

	void draw(Image iIMG)
  {
    iIMG.draw(cast(int)(iXPos + Core.instance().getLevel().getXPos()), iYPos);

    if(iPoints > 0) {
      Core.instance().getTextDrawer().draw(to!string(iPoints), cast(int)(iXPos + Core.instance().getLevel().getXPos() + 42), iYTextPos - 22, 8, 16);
    }
  }

	void drawCastleFlag(Image iIMG)
  {
    iIMG.draw(cast(int)(iXPos + Core.instance().getLevel().getXPos() + castleFlagExtraXPos + 7*32 - 14), GameConfig.window.HEIGHT + 14 - 6*32 - castleFlagY);
  }

};
