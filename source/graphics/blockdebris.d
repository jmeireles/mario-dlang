module graphics.blockdebris;
import graphics.vector2;

import gamecore;
import config;


class BlockDebris
{
private:
	// ----- 0 = Animation, -1 = Delete
	int debrisState;

	Vector2 vPositionL;
	Vector2 vPositionR;
	Vector2 vPositionL2;
	Vector2 vPositionR2;

	int iFrameID;

	float fSpeedX, fSpeedY;

	bool bRotate;
public:
	this(int iXPos, int iYPos)
  {
    this.debrisState = 0;
    this.bRotate = false;

    this.vPositionL = new Vector2(iXPos, iYPos);
    this.vPositionR = new Vector2(iXPos + 16, iYPos);
    this.vPositionL2 = new Vector2(iXPos, iYPos + 16);
    this.vPositionR2 = new Vector2(iXPos + 16, iYPos + 16);

    this.fSpeedX = 2.15f;
    this.fSpeedY = 1;
  }

	void update()
  {
    ++iFrameID;

    if(iFrameID > 4) {
      bRotate = !bRotate;
      iFrameID = 0;
    }

    vPositionL.setX(cast(int)(vPositionL.getX() - fSpeedX));
    vPositionL.setY(cast(int)(vPositionL.getY() + (fSpeedY - 3.0f) + (fSpeedY < 3 ? fSpeedY < 2.5f ? -3 : -1.5f : fSpeedY < 3.5f ? 1.5f : 3)));

    vPositionR.setX(cast(int)(vPositionR.getX() + fSpeedX));
    vPositionR.setY(cast(int)(vPositionR.getY() + (fSpeedY - 3.0f) + (fSpeedY < 3 ? fSpeedY < 2.5f ? -3 : -1.5f : fSpeedY < 3.5f ? 1.5f : 3)));

    vPositionL2.setX(cast(int)(vPositionL2.getX() - (fSpeedX - 1.1f)));
    vPositionL2.setY(cast(int)(vPositionL2.getY() + (fSpeedY - 1.5f) + (fSpeedY < 1.5 ? fSpeedY < 1.3f ? -3 : -1 : fSpeedY < 1.8f ? 1 : 3)));

    vPositionR2.setX(cast(int)(vPositionR2.getX() + (fSpeedX - 1.1f)));
    vPositionR2.setY(cast(int)(vPositionR2.getY() + (fSpeedY - 1.5f) + (fSpeedY < 1.5 ? fSpeedY < 1.3f ? -3 : -1 : fSpeedY < 1.8f ? 1 : 3)));

    fSpeedY *= 1.09f;
    fSpeedX *= 1.005f;

    if (vPositionL.getY() >= GameConfig.window.HEIGHT) {
      debrisState = -1;
    }
  }

	void draw()
  {
    Core.instance().getGameData().getBlock(Core.instance().getLevelManager().getLevelType() == 0 || Core.instance().getLevelManager().getLevelType() == 4 ? 64 : Core.instance().getLevelManager().getLevelType() == 1 ? 65 : 66).getSprite().getTexture().draw(vPositionL.getX() + cast(int)Core.instance().getLevel().getXPos(), vPositionL.getY(), bRotate);
    Core.instance().getGameData().getBlock(Core.instance().getLevelManager().getLevelType() == 0 || Core.instance().getLevelManager().getLevelType() == 4 ? 64 : Core.instance().getLevelManager().getLevelType() == 1 ? 65 : 66).getSprite().getTexture().draw(vPositionR.getX() + cast(int)Core.instance().getLevel().getXPos(), vPositionR.getY(), bRotate);
    Core.instance().getGameData().getBlock(Core.instance().getLevelManager().getLevelType() == 0 || Core.instance().getLevelManager().getLevelType() == 4 ? 64 : Core.instance().getLevelManager().getLevelType() == 1 ? 65 : 66).getSprite().getTexture().draw(vPositionL2.getX() + cast(int)Core.instance().getLevel().getXPos(), vPositionL2.getY(), bRotate);
    Core.instance().getGameData().getBlock(Core.instance().getLevelManager().getLevelType() == 0 || Core.instance().getLevelManager().getLevelType() == 4 ? 64 : Core.instance().getLevelManager().getLevelType() == 1 ? 65 : 66).getSprite().getTexture().draw(vPositionR2.getX() + cast(int)Core.instance().getLevel().getXPos(), vPositionR2.getY(), bRotate);
  }

	int getDebrisState()
  {
    return debrisState;
  }
};
