module graphics.star;

import graphics.minion;
import graphics.image;
import gamecore;

class Star : Minion
{
private:
	bool inSpawnState;
	int inSpawnY;
	int iX, iY; // inSpawnState draw Block
public:
	this(int iXPos, int iYPos, int iX, int iY)
  {
    this.fXPos = cast(float)iXPos + 2;
  	this.fYPos = cast(float)iYPos;

  	this.collisionOnlyWithPlayer = true;

  	this.moveDirection = false;
  	this.moveSpeed = 3;

  	this.inSpawnState = true;
  	this.minionSpawned = true;
  	this.inSpawnY = 30;
  	this.startJumpSpeed = 4;

  	this.iX = iX;
  	this.iY = iY;

  	this.iBlockID = 24;

  	this.iHitBoxX = 28;
  	this.iHitBoxY = 32;
  }

	override void update()
  {
    if (inSpawnState) {
  		if (inSpawnY <= 0) {
  			inSpawnState = false;
  		} else {
  			if (fYPos > -5) {
  				inSpawnY -= 2;
  				fYPos -= 2;
  			} else {
  				inSpawnY -= 1;
  				fYPos -= 1;
  			}
  		}
  	} else {
  		if(jumpState == 0) {
  			startJump(1);
  			jumpDistance = 32;
  		}
  		updateXPos();
  	}
  }

	override bool updateMinion()
  {
    if (!inSpawnState) {
  		minionPhysics();
  	}

  	return minionSpawned;
  }

  override void minionPhysics()
  {
  	if (jumpState == 1) {
  		if(minionState == 0) {
  			updateYPos(-4 + (currentJumpDistance > 64 ? 2 : 0));
  			currentJumpDistance += 2;

  			if (jumpDistance <= currentJumpDistance) {
  				jumpState = 2;
  			}
  		}
  	} else {
  		if (!Core.instance().getLevel().checkCollisionLB(cast(int)fXPos + 2, cast(int)fYPos + 2, iHitBoxY, true) && !Core.instance().getLevel().checkCollisionRB(cast(int)fXPos - 2, cast(int)fYPos + 2, iHitBoxX, iHitBoxY, true)) {
  			this.physicsState2();
  		} else {
  			jumpState = 0;
  		}
  	}
  }

	override void draw(Image iIMG)
  {
    if(minionState >= 0) {
  		iIMG.draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos + 2, false);
  		if (inSpawnState) {
  			Core.instance().getGameData().getBlock(Core.instance().getLevel().getLevelType() == 0 || Core.instance().getLevel().getLevelType() == 4 ? 9 : 56).getSprite().getTexture().draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos + (32 - inSpawnY) - Core.instance().getLevel().getMapBlock(iX, iY).getYPos() + 2, false);
  		}
  	}
  }

	override void collisionWithPlayer(bool TOP)
  {
    if(!inSpawnState) {
  		Core.instance().getPlayer().setStarEffect(true);
  		minionState = -1;
  	}
  }

	override void setMinionState(int minionState)
  {

  }
};
