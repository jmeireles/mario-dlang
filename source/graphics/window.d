module graphics.window;

import derelict.sdl2.sdl;
import derelict.sdl2.mixer;
import std.string;
import config;

class Window
{
    string ico = "files/images/ico.bmp";
    SDL_Window* window;

    this (string title)
    {
      window = SDL_CreateWindow(
          toStringz(title),                 // window title
          SDL_WINDOWPOS_CENTERED,           // initial x position
          SDL_WINDOWPOS_CENTERED,           // initial y position
          GameConfig.window.WIDTH,          // width, in pixels
          GameConfig.window.HEIGHT,         // height, in pixels
            //SDL_WINDOW_FULLSCREEN_DESKTOP
          SDL_WINDOW_SHOWN                  // flags - see below
      );

    	SDL_Surface* loadedSurface = SDL_LoadBMP(toStringz(this.ico));
      SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface.format, 255, 0, 255));
    	SDL_SetWindowIcon(window, loadedSurface);
    	SDL_FreeSurface(loadedSurface);
      // SDL_WINDOW_FULLSCREEN_DESKTOP
      //  SDL_SetWindowFullscreen(window, 1);
    }

    ~this()
    {
        SDL_DestroyWindow(this.window);
    }

    SDL_Window* get()
    {
        return this.window;
    }

}
