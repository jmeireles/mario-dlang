module graphics.minion;

import derelict.sdl2.sdl;
import graphics.image;
import gamecore;
import std.conv;
import config;

abstract class Minion
{

  int minionState;
  int iBlockID;
  int iHitBoxX, iHitBoxY;
  int deadTime;

  bool killOtherUnits;
  bool minionSpawned;
  bool collisionOnlyWithPlayer;
  bool onAnotherMinion;

  // ----- true = LEFT, false = RIGHT
  bool moveDirection;
  int moveSpeed;

  int jumpState;

  float fXPos, fYPos;
  float startJumpSpeed;
  float currentJumpSpeed;
  float jumpDistance;
  float currentJumpDistance;
  float currentFallingSpeed;

  this()
  {
    this.minionState = 0;
  	this.iHitBoxX = this.iHitBoxY = 32;

  	this.killOtherUnits = false;
  	this.minionSpawned = false;
  	this.collisionOnlyWithPlayer = false;

  	this.moveDirection = true;
  	this.moveSpeed = 1;

  	this.jumpState = 0;
  	this.startJumpSpeed = 6.65f;
  	this.currentFallingSpeed = 2.2f;

  	this.currentJumpSpeed = 0;
  	this.jumpDistance = 0;
  	this.currentJumpDistance = 0;

  	this.deadTime = -1;

  	this.onAnotherMinion = false;
  }

  // ---------- Methods
	abstract void update();
	abstract void draw(Image iIMG);

	void updateYPos(int iN)
  {
      if (iN > 0) {
        if (!Core.instance().getLevel().checkCollisionLB(to!int(fXPos) + 2, to!int(fYPos)  + iN, iHitBoxY, true) && !Core.instance().getLevel().checkCollisionRB(to!int(fXPos) - 2, to!int(fYPos)  + iN, iHitBoxX, iHitBoxY, true)) {
          fYPos += iN;
        } else {
          if (jumpState == 1) {
            jumpState = 2;
          }
          updateYPos(iN - 1);
        }
      }
      else if (iN < 0) {
        if (!Core.instance().getLevel().checkCollisionLT(to!int(fXPos) + 2, to!int(fYPos)  + iN, true) && !Core.instance().getLevel().checkCollisionRT(to!int(fXPos) - 2, to!int(fYPos)  + iN, iHitBoxX, true)) {
          fYPos += iN;
        } else {
          if (jumpState == 1) {
            jumpState = 2;
          }
          updateYPos(iN + 1);
        }
      }

      if(to!int(fYPos)  % 2 == 1) {
        fYPos += 1;
      }
  }

	void updateXPos()
  {
    // ----- LEFT
  	if (moveDirection) {
  		if (Core.instance().getLevel().checkCollisionLB(to!int(fXPos) - moveSpeed, to!int(fYPos)  - 2, iHitBoxY, true) || Core.instance().getLevel().checkCollisionLT(to!int(fXPos) - moveSpeed, to!int(fYPos)  + 2, true)) {
  			moveDirection = !moveDirection;
  			if(killOtherUnits && fXPos > -Core.instance().getLevel().getXPos() - 64 && fXPos < -Core.instance().getLevel().getXPos() + GameConfig.window.WIDTH + 64 + iHitBoxX) Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cBLOCKHIT);
  		} else {
  			fXPos -= (jumpState == 0 ? moveSpeed : moveSpeed/2.0f);
  		}
  	}
  	// ----- RIGHT
  	else {
  		if (Core.instance().getLevel().checkCollisionRB(to!int(fXPos) + moveSpeed, to!int(fYPos)  - 2, iHitBoxX, iHitBoxY, true) || Core.instance().getLevel().checkCollisionRT(to!int(fXPos) + moveSpeed, to!int(fYPos)  + 2, iHitBoxX, true)) {
  			moveDirection = !moveDirection;
  			if(killOtherUnits && fXPos > -Core.instance().getLevel().getXPos() - 64 && fXPos < -Core.instance().getLevel().getXPos() + GameConfig.window.WIDTH + 64 + iHitBoxX) Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cBLOCKHIT);
  		} else {
  			fXPos += (jumpState == 0 ? moveSpeed : moveSpeed/2.0f);
  		}
  	}

  	if(fXPos < -iHitBoxX) {
  		minionState = -1;
  	}
  }

	 bool updateMinion()
   {
       if (!minionSpawned) {
         Spawn();
       } else {
         minionPhysics();
       }

       return minionSpawned;
   }
   
	void minionPhysics()
  {
      if (jumpState == 1) {
        physicsState1();
      } else {
        if (!Core.instance().getLevel().checkCollisionLB(to!int(fXPos) + 2, to!int(fYPos)  + 2, iHitBoxY, true) && !Core.instance().getLevel().checkCollisionRB(to!int(fXPos) - 2, to!int(fYPos)  + 2, iHitBoxX, iHitBoxY, true) && !onAnotherMinion) {
          physicsState2();
        } else {
          jumpState = 0;
          onAnotherMinion = false;
        }
      }
  }

	void collisionEffect()
  {
    if(minionSpawned)
    moveDirection = !moveDirection;
  }

	 void minionDeathAnimation()
   {
      fXPos += (moveDirection ? -1.5f : 1.5f);
     	fYPos += 2 * (deadTime > 8 ? -1 : deadTime > 2 ? 1 : 4.25f);

     	if(deadTime > 0) {
     		--deadTime;
     	}

     	if(fYPos > GameConfig.window.HEIGHT) {
     		minionState = -1;
     	}
   }

	 void physicsState1()
   {
      updateYPos(-to!int(currentJumpSpeed));
     	currentJumpDistance += to!int(currentJumpSpeed);

     	currentJumpSpeed *= (currentJumpDistance / jumpDistance > 0.75f ? 0.972f : 0.986f);

     	if (currentJumpSpeed < 2.5f) {
     		currentJumpSpeed = 2.5f;
     	}

     	if (jumpDistance <= currentJumpDistance) {
     		jumpState = 2;
   	}
   }

	 void physicsState2()
   {
     currentFallingSpeed *= 1.06f;

   	if (currentFallingSpeed > startJumpSpeed) {
   		currentFallingSpeed = startJumpSpeed;
   	}

   	updateYPos(to!int(currentFallingSpeed));

   	jumpState = 2;

   	if (fYPos >= GameConfig.window.HEIGHT) {
   		minionState = -1;
   	}
   }


	void Spawn()
  {
    if ((fXPos >= -Core.instance().getLevel().getXPos() && fXPos <= -Core.instance().getLevel().getXPos() + GameConfig.window.WIDTH) || (fXPos + iHitBoxX >= -Core.instance().getLevel().getXPos() && fXPos + iHitBoxX <= -Core.instance().getLevel().getXPos() + GameConfig.window.WIDTH)) {
      minionSpawned = true;
    }
  }

	void startJump(int iH)
  {
      jumpState = 1;
      currentJumpSpeed = startJumpSpeed;
      jumpDistance = 32 * iH + 16.0f;
      currentJumpDistance = 0;
  }

	void resetJump()
  {
    jumpState = 0;
  	currentFallingSpeed = 2.7f;
  }

	// ----- COLLISON
	abstract void collisionWithPlayer(bool TOP);
	 void collisionWithAnotherUnit() {} // -- PLAYERFIREBALL
   void lockMinion()  {}

	void points(int iPoints)
  {
    Core.instance().getLevel().addPoints(to!int(fXPos) + 7, to!int(fYPos) , to!string(iPoints * Core.instance().getPlayer().getComboPoints()), 8, 16);
    Core.instance().getPlayer().setScore(Core.instance().getPlayer().getScore() + iPoints * Core.instance().getPlayer().getComboPoints());
    Core.instance().getPlayer().addComboPoints();
  }

	// ----- get & set -----
	int getBloockID()
  {
    return iBlockID;
  }

	void setBlockID(int iBlockID)
  {
    	this.iBlockID = iBlockID;
  }

	int getMinionState()
  {
    return minionState;
  }

	void setMinionState(int minionState)
  {
    this.minionState = minionState;
    if(minionState == -2) {
      deadTime = 16;
      fYPos -= iHitBoxY/4;
      points(200);
      collisionOnlyWithPlayer = true;
      Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cSHOT);
    }
  }

	bool getPowerUP()
  {
    return true;
  }

	int getXPos()
  {
    	return cast(int)fXPos;
  }

	int getYPos()
  {
    return to!int(fYPos) ;
  }

	void setYPos(int iYPos)
  {
      this.fYPos = cast(float)iYPos;
  }

}
