module graphics.image;
import derelict.sdl2.sdl;
import derelict.sdl2.image;

import std.stdio;
import std.string;

class Image
{
  private:
    SDL_Texture* img;
    SDL_Rect rRect;
    SDL_Renderer* renderer;
    string fileName;

  public:
    this () {};

    this (string fileName, SDL_Renderer* renderer)
    {
        this.renderer = renderer;
        this.setIMG(fileName);
    };

    ~this()
    {
        SDL_DestroyTexture(this.img);
    }

    string getFileName()
    {
       return this.fileName;
    }

    void draw(int iXOffset, int iYOffset)
    {
      this.rRect.x = iXOffset;
      this.rRect.y = iYOffset;

      SDL_RenderCopy(this.renderer, this.img, null, &rRect);
    }

    void draw(int iXOffset, int iYOffset, bool bRotate)
    {
        rRect.x = iXOffset;
        rRect.y = iYOffset;

        if(!bRotate) {
          SDL_RenderCopy(this.renderer, this.img, null, &rRect);
          } else {
            SDL_RenderCopyEx(this.renderer, this.img, null, &rRect, 180.0, null, SDL_FLIP_VERTICAL);
        }
    }

    void drawVert(int iXOffset, int iYOffset)
    {
      rRect.x = iXOffset;
      rRect.y = iYOffset;

      SDL_RenderCopyEx(this.renderer, this.img, null, &rRect, 180.0, null, SDL_FLIP_HORIZONTAL);
    }

    void draw(SDL_Rect rCrop, SDL_Rect rRect)
    {
        SDL_RenderCopy(this.renderer, this.img, &rCrop, &rRect);
    }

    void setIMG(string fileName)
    {
      string full = "files/images/"~fileName~".bmp";
      this.fileName = full;
      SDL_Surface* loadedSurface = SDL_LoadBMP(toStringz(full));
      uint test = SDL_MapRGB(loadedSurface.format, 255, 0, 255);

      SDL_SetColorKey(loadedSurface, SDL_TRUE, test);
      this.img = SDL_CreateTextureFromSurface(this.renderer, loadedSurface);
      int iWidth, iHeight;

      SDL_QueryTexture(this.img, null, null, &iWidth, &iHeight);

      rRect.x  = 0;
      rRect.y = 0;
      rRect.w = iWidth;
      rRect.h = iHeight;
      SDL_FreeSurface(loadedSurface);
    }

    SDL_Rect getRect()
    {
      return this.rRect;
    }

    SDL_Texture* getIMG()
    {
        return this.img;
    }
};
