module graphics.vector2;

class Vector2
{
private:
	int X, Y;

public:
	this()
  {

  }
	this(int X, int Y)
  {
    this.X = X;
    this.Y = Y;
  }
	~this()
  {

  }

	int getX()
  {
      return X;
  }
	void setX(int X)
  {
    	this.X = X;
  }
	int getY()
  {
      	return Y;
  }
	void setY(int Y)
  {
	this.Y = Y;
  }
};
