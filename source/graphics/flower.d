module graphics.flower;

import graphics.minion;
import graphics.image;
import gamecore;

class Flower : Minion
{
private:
	bool inSpawnState;
	int inSpawnY;
	int iX, iY; // inSpawnState draw Block
public:
	this(int iXPos, int iYPos, int iX, int iY)
  {
    this.fXPos = cast(float)iXPos;
    this.fYPos = cast(float)iYPos;

    this.iBlockID = 6;
    this.moveSpeed = 2;
    this.inSpawnState = true;
    this.minionSpawned = true;
    this.inSpawnY = 32;
    this.moveDirection = false;
    this.collisionOnlyWithPlayer = true;

    this.iX = iX;
    this.iY = iY;
  }

	override void update()
  {
    if (inSpawnState) {
      if (inSpawnY <= 0) {
        inSpawnState = false;
      } else {
        if (fYPos > -5) {
          inSpawnY -= 2;
          fYPos -= 2;
        } else {
          inSpawnY -= 1;
          fYPos -= 1;
        }
      }
    }
  }

	override bool updateMinion()
  {
      return minionSpawned;
  }

	override void draw(Image iIMG)
  {
    if(minionState >= 0) {
  		iIMG.draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos + 2, false);
  		if (inSpawnState) {
  			Core.instance().getGameData().getBlock(Core.instance().getLevel().getLevelType() == 0 || Core.instance().getLevel().getLevelType() == 4 ? 9 : 56).getSprite().getTexture().draw(cast(int)fXPos + cast(int)Core.instance().getLevel().getXPos(), cast(int)fYPos + (32 - inSpawnY) - Core.instance().getLevel().getMapBlock(iX, iY).getYPos() + 2, false);
  		}
  	}
  }

	override void collisionWithPlayer(bool TOP)
  {
    if(!inSpawnState && minionState >= 0) {
      Core.instance().getPlayer().setPowerLVL(Core.instance().getPlayer().getPowerLVL() + 1);
      minionState = -1;
    }
  }

	override void setMinionState(int minionState)
  {

  }
};
