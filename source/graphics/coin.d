module graphics.coin;

import gamecore;

class Coin
{
private:
	int iXPos, iYPos;
	int iLEFT;

	int iSpriteID;
	int iStepID;

	bool bTOP;

	bool bDelete;
public:
	this(int iXPos, int iYPos)
  {
    this.iXPos = iXPos;
    this.iYPos = iYPos;
    this.iSpriteID = 0;
    this.iStepID = 0;
    this.iLEFT = 80;
    this.bTOP = true;
    this.bDelete = false;
  }

	void update()
  {
    if(iLEFT > 0) {
  		iLEFT -= 5;
  		iYPos = iYPos + (bTOP ? - 5 : 5);

  		++iStepID;
  		if(iStepID > 2) {
  			iStepID = 0;
  			++iSpriteID;
  			if(iSpriteID > 3) {
  				iSpriteID = 0;
  			}
  		}
  	} else if(bTOP) {
  		bTOP = false;
  		iLEFT = 80;
  	} else {
  		bDelete = true;
  	}
  }

	void draw()
  {
      Core.instance().getGameData().getBlock(50).getSprite().getTexture(iSpriteID).draw(iXPos + cast(int)Core.instance().getLevel().getXPos(), iYPos);
  }

	int getXPos()
  {
    return iXPos;
  }
	int getYPos()
  {
    return iYPos;
  }
	bool getDelete()
  {
  	return bDelete;
  }
};
