module drawers.text;

import graphics.image;
import derelict.sdl2.sdl;
import config;
import std.conv;

class TextDrawer
{
  private:
  	Image FONT;

    SDL_Renderer* renderer;
  	SDL_Rect rCrop;
  	SDL_Rect rRect;

  	int fontSize;
  	int extraLeft;
    int nextExtraLeft;

    int getPos(int iChar)
    {
    	if(iChar >= 43 && iChar < 91) {
    		this.checkExtra(iChar);
    		return (iChar - 43) * rCrop.w + rCrop.w;
    	}

    	if(iChar >= 118 && iChar < 123) { // v w x y z
    		return (iChar - 70) * rCrop.w + rCrop.w;
    	}

    	return 0;
    }

public:
	this(SDL_Renderer* renderer, string fileName)
  {
    rCrop.x = 0;
    rCrop.y = 0;
    rCrop.w = 8;
    rCrop.h = 8;

    rRect.x = 0;
    rRect.y = 0;
    rRect.w = 16;
    rRect.h = 16;

    this.fontSize = 16;
    this.extraLeft = 0;
    this.nextExtraLeft = 0;
    this.renderer = renderer;
    FONT = new Image(fileName, this.renderer);
  }

	~this()
  {
      destroy(FONT);
  }

  void drawWS(string sText, int X, int Y,int iR, int iG, int iB, int fontSize = 16)
  {
  	this.fontSize = fontSize;
  	this.extraLeft = 0;
  	this.nextExtraLeft = 0;

    foreach (int index, char letter; sText) {
  		SDL_SetTextureColorMod(FONT.getIMG(), 0, 0, 0);
  		rCrop.x = getPos(to!int(letter));
  		rRect.x = X + fontSize * index - extraLeft - 1;
  		rRect.y = Y + 1;
  		rRect.w = fontSize;
  		rRect.h = fontSize;
  		FONT.draw(rCrop, rRect);
  		SDL_SetTextureColorMod(FONT.getIMG(), 255, 255, 255);
  		rRect.x = X + fontSize * index - extraLeft + 1;
  		rRect.y = Y - 1;
  		FONT.draw(rCrop, rRect);
  		extraLeft += nextExtraLeft;
  		nextExtraLeft = 0;
  	}
  }


	void draw(string sText, int X, int Y, int fontSize = 16)
  {
    this.fontSize = fontSize;
    this.extraLeft = 0;
    this.nextExtraLeft = 0;

    foreach (int index, char letter; sText) {
      rCrop.x = getPos(to!int(letter));
      rRect.x = X + fontSize * index - extraLeft;
      rRect.y = Y;
      rRect.w = fontSize;
      rRect.h = fontSize;
      FONT.draw(rCrop, rRect);
      extraLeft += nextExtraLeft;
      nextExtraLeft = 0;
    }
  }

	void draw(string sText, int X, int Y, int fontSize, int iR, int iG, int iB)
  {
    this.fontSize = fontSize;
    this.extraLeft = 0;
    this.nextExtraLeft = 0;

  foreach (int index, char letter; sText) {
      SDL_SetTextureColorMod(FONT.getIMG(), to!ubyte(iR), to!ubyte(iG), to!ubyte(iB));
      rCrop.x = getPos(to!int(letter));
      rRect.x = X + fontSize * index - extraLeft;
      rRect.y = Y;
      rRect.w = fontSize;
      rRect.h = fontSize;
      FONT.draw(rCrop, rRect);
      extraLeft += nextExtraLeft;
      nextExtraLeft = 0;
      SDL_SetTextureColorMod(FONT.getIMG(), 255, 255, 255);
    }
  }

	void drawCenterX(string sText, int Y, int fontSize = 16, int iR = 255, int iG = 255, int iB = 255)
  {
    int X = GameConfig.window.WIDTH / 2 - getTextWidth(sText, fontSize) / 2;
    draw(sText, X, Y, fontSize, iR, iG, iB);
  }

	void draw(string sText, int X, int Y, int iWidth, int iHeight)
  {
    foreach (int index, char letter; sText) {
      rCrop.x = getPos(to!int(letter));
      rRect.x = X + iWidth * index - extraLeft;
      rRect.y = Y;
      rRect.w = iWidth;
      rRect.h = iHeight;
      FONT.draw(rCrop, rRect);
    }
  }
  //
	// void DrawWS(SDL_Renderer* rR, string sText, int X, int Y,int iR, int iG, int iB, int fontSize = 16)
  // {
  //
  // }

	int getTextWidth(string sText, int fontSize = 16)
  {
      int iOutput = to!int(sText.length * fontSize);
      nextExtraLeft = 0;

        foreach (int index, char letter; sText) {
          checkExtra(index);
      }

      iOutput -= nextExtraLeft;

      return iOutput;
  }

  void checkExtra(int iChar)
  {
  	switch(iChar) {
  	case 44: case 46: case 58: case 59:
  		nextExtraLeft += 4 * fontSize / rRect.w;
  		break;
      default: break;
  	}
  }

	// ----- SET FONT IMG
	void setFont(string fileName)
  {

  }

};
