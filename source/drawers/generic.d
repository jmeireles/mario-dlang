module drawers.generic;

import graphics.image;
import derelict.sdl2.sdl;
import config;
import std.conv;

class GenericDrawer
{
public static:
  void setBackgroundColor(SDL_Renderer* renderer, ubyte r, ubyte g, ubyte b, ubyte a)
  {
      SDL_SetRenderDrawColor(renderer, r, g, b, a);
      SDL_RenderFillRect(renderer, null);
  }

};
