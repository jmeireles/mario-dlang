import derelict.sdl2.sdl;
import std.conv;

import graphics.block;
import graphics.sprite;
import player;
import event;

class GameData
{
    Block[] blocks;
    Block[] minions;

    int iBlockSize;
    int iMinionSize;
    Event oEvent;

    SDL_Renderer* renderer;
    Player player;

    this(SDL_Renderer* renderer, Player player)
    {
        this.player = player;
        this.renderer = renderer;
        this.load();
        this.oEvent = new Event();
    }

    Event getEvent()
    {
        return this.oEvent;
    }

    Player getPlayer()
    {
        return this.player;
    }

    Block getBlock(int id)
    {
        return this.blocks[id];
    }

    Block getMinion(int id)
    {
        return this.minions[id];
    }

    int getMinionSize()
    {
       return this.iMinionSize;
    }

    private:
    void load()
    {
        this.loadBlocks();
        this.loadMinions();
    }

    void loadBlocks()
    {
        string[] newSprite;
        string[] tSprite;
        uint[] iDelay;
        uint[] newDelay;

        // ----- 0 Transparent -----
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(0, new Sprite(this.renderer, tSprite, iDelay, false), false, true, false, false);
        tSprite = newSprite;
        iDelay = newDelay;

        // ----- 1 -----
        tSprite ~= "gnd_red_1";
        iDelay ~= 0;
        this.blocks ~= new Block(1, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 2 -----
        tSprite ~= "coin_0";
        iDelay ~= 300;
        tSprite ~= "coin_2";
        iDelay ~= 30;
        tSprite ~= "coin_1";
        iDelay ~= 130;
        tSprite ~= "coin_2";
        iDelay ~= 140;
        this.blocks ~= new Block(2, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 3 -----
        tSprite ~= "bush_center_0";
        iDelay ~= 0;
        this.blocks ~= new Block(3, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 4 -----
        tSprite ~= "bush_center_1";
        iDelay ~= 0;
        this.blocks ~= new Block(4, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 5 -----
        tSprite ~= "bush_left";
        iDelay ~= 0;
        this.blocks ~= new Block(5, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 6 -----
        tSprite ~= "bush_right";
        iDelay ~= 0;
        this.blocks ~= new Block(6, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 7 -----
        tSprite ~= "bush_top";
        iDelay ~= 0;
        this.blocks ~= new Block(7, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 8 -----
        tSprite ~= "blockq_0";
        iDelay ~= 300;
        tSprite ~= "blockq_2";
        iDelay ~= 30;
        tSprite ~= "blockq_1";
        iDelay ~= 130;
        tSprite ~= "blockq_2";
        iDelay ~= 140;
        this.blocks ~= new Block(8, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 9 -----
        tSprite ~= "blockq_used";
        iDelay ~= 0;
        this.blocks ~= new Block(9, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 10 -----
        tSprite ~= "grass_left";
        iDelay ~= 0;
        this.blocks ~= new Block(10, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 11 -----
        tSprite ~= "grass_center";
        iDelay ~= 0;
        this.blocks ~= new Block(11, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 12 -----
        tSprite ~= "grass_right";
        iDelay ~= 0;
        this.blocks ~= new Block(12, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 13 -----
        tSprite ~= "brickred";
        iDelay ~= 0;
        this.blocks ~= new Block(13, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 14 -----
        tSprite ~= "cloud_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(14, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 15 -----
        tSprite ~= "cloud_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(15, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 16 -----
        tSprite ~= "cloud_center_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(16, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 17 -----
        tSprite ~= "cloud_center_top";
        iDelay ~= 0;
        this.blocks ~= new Block(17, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 18 -----
        tSprite ~= "cloud_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(18, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 19 -----
        tSprite ~= "cloud_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(19, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 20 -----
        tSprite ~= "pipe_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(20, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 21 -----
        tSprite ~= "pipe_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(21, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 22 -----
        tSprite ~= "pipe_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(22, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 23 -----
        tSprite ~= "pipe_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(23, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 24 BlockQ2 -----
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(24, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, false);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 25 -----
        tSprite ~= "gnd_red2";
        iDelay ~= 0;
        this.blocks ~= new Block(25, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 26 -----
        tSprite ~= "gnd1";
        iDelay ~= 0;
        this.blocks ~= new Block(26, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 27 -----
        tSprite ~= "gnd1_2";
        iDelay ~= 0;
        this.blocks ~= new Block(27, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 28 -----
        tSprite ~= "brick1";
        iDelay ~= 0;
        this.blocks ~= new Block(28, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 29 -----
        tSprite ~= "coin_use0";
        iDelay ~= 300;
        tSprite ~= "coin_use2";
        iDelay ~= 30;
        tSprite ~= "coin_use1";
        iDelay ~= 130;
        tSprite ~= "coin_use2";
        iDelay ~= 140;
        this.blocks ~= new Block(29, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 30 -----
        tSprite ~= "pipe1_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(30, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 31 -----
        tSprite ~= "pipe1_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(31, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 32 -----
        tSprite ~= "pipe1_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(32, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 33 -----
        tSprite ~= "pipe1_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(33, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 34 -----
        tSprite ~= "pipe1_hor_bot_right";
        iDelay ~= 0;
        this.blocks ~= new Block(34, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 35 -----
        tSprite ~= "pipe1_hor_top_center";
        iDelay ~= 0;
        this.blocks ~= new Block(35, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 36 -----
        tSprite ~= "pipe1_hor_top_left";
        iDelay ~= 0;
        this.blocks ~= new Block(36, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 37 -----
        tSprite ~= "pipe1_hor_bot_center";
        iDelay ~= 0;
        this.blocks ~= new Block(37, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 38 -----
        tSprite ~= "pipe1_hor_bot_left";
        iDelay ~= 0;
        this.blocks ~= new Block(38, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 39 -----
        tSprite ~= "pipe1_hor_top_right";
        iDelay ~= 0;
        this.blocks ~= new Block(39, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 40 -----
        tSprite ~= "end0_l";
        iDelay ~= 0;
        this.blocks ~= new Block(40, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 41 -----
        tSprite ~= "end0_dot";
        iDelay ~= 0;
        this.blocks ~= new Block(41, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 42 -----
        tSprite ~= "end0_flag";
        iDelay ~= 0;
        this.blocks ~= new Block(42, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 43 -----
        tSprite ~= "castle0_brick";
        iDelay ~= 0;
        this.blocks ~= new Block(43, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 44 -----
        tSprite ~= "castle0_top0";
        iDelay ~= 0;
        this.blocks ~= new Block(44, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 45 -----
        tSprite ~= "castle0_top1";
        iDelay ~= 0;
        this.blocks ~= new Block(45, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 46 -----
        tSprite ~= "castle0_center_center_top";
        iDelay ~= 0;
        this.blocks ~= new Block(46, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 47 -----
        tSprite ~= "castle0_center_center";
        iDelay ~= 0;
        this.blocks ~= new Block(47, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 48 -----
        tSprite ~= "castle0_center_left";
        iDelay ~= 0;
        this.blocks ~= new Block(48, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 49 -----
        tSprite ~= "castle0_center_right";
        iDelay ~= 0;
        this.blocks ~= new Block(49, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 50 -----
        tSprite ~= "coin_an0";
        iDelay ~= 0;
        tSprite ~= "coin_an1";
        iDelay ~= 0;
        tSprite ~= "coin_an2";
        iDelay ~= 0;
        tSprite ~= "coin_an3";
        iDelay ~= 0;
        this.blocks ~= new Block(50, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 51 -----
        tSprite ~= "castle_flag";
        iDelay ~= 0;
        this.blocks ~= new Block(51, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 52 -----
        tSprite ~= "firework0";
        iDelay ~= 0;
        this.blocks ~= new Block(52, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 53 -----
        tSprite ~= "firework1";
        iDelay ~= 0;
        this.blocks ~= new Block(53, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 54 -----
        tSprite ~= "firework2";
        iDelay ~= 0;
        this.blocks ~= new Block(54, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 55 -----
        tSprite ~= "blockq1_0";
        iDelay ~= 300;
        tSprite ~= "blockq1_2";
        iDelay ~= 30;
        tSprite ~= "blockq1_1";
        iDelay ~= 130;
        tSprite ~= "blockq1_2";
        iDelay ~= 140;
        this.blocks ~= new Block(55, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 56 -----
        tSprite ~= "blockq1_used";
        iDelay ~= 0;
        this.blocks ~= new Block(56, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 57 -----
        tSprite ~= "coin1_0";
        iDelay ~= 300;
        tSprite ~= "coin1_2";
        iDelay ~= 30;
        tSprite ~= "coin1_1";
        iDelay ~= 130;
        tSprite ~= "coin1_2";
        iDelay ~= 140;
        this.blocks ~= new Block(57, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 58 -----
        tSprite ~= "pipe_hor_bot_right";
        iDelay ~= 0;
        this.blocks ~= new Block(58, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 59 -----
        tSprite ~= "pipe_hor_top_center";
        iDelay ~= 0;
        this.blocks ~= new Block(59, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 60 -----
        tSprite ~= "pipe_hor_top_left";
        iDelay ~= 0;
        this.blocks ~= new Block(60, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 61 -----
        tSprite ~= "pipe_hor_bot_center";
        iDelay ~= 0;
        this.blocks ~= new Block(61, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 62 -----
        tSprite ~= "pipe_hor_bot_left";
        iDelay ~= 0;
        this.blocks ~= new Block(62, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 63 -----
        tSprite ~= "pipe_hor_top_right";
        iDelay ~= 0;
        this.blocks ~= new Block(63, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 64 -----
        tSprite ~= "block_debris0";
        iDelay ~= 0;
        this.blocks ~= new Block(64, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 65 -----
        tSprite ~= "block_debris1";
        iDelay ~= 0;
        this.blocks ~= new Block(65, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 66 -----
        tSprite ~= "block_debris2";
        iDelay ~= 0;
        this.blocks ~= new Block(66, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 67 -----
        tSprite ~= "t_left";
        iDelay ~= 0;
        this.blocks ~= new Block(67, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 68 -----
        tSprite ~= "t_center";
        iDelay ~= 0;
        this.blocks ~= new Block(68, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 69 -----
        tSprite ~= "t_right";
        iDelay ~= 0;
        this.blocks ~= new Block(69, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 70 -----
        tSprite ~= "t_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(70, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 71 -----
        tSprite ~= "coin_use00";
        iDelay ~= 300;
        tSprite ~= "coin_use02";
        iDelay ~= 30;
        tSprite ~= "coin_use01";
        iDelay ~= 130;
        tSprite ~= "coin_use02";
        iDelay ~= 140;
        this.blocks ~= new Block(71, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 72 -----
        tSprite ~= "coin_use20";
        iDelay ~= 300;
        tSprite ~= "coin_use22";
        iDelay ~= 30;
        tSprite ~= "coin_use21";
        iDelay ~= 130;
        tSprite ~= "coin_use22";
        iDelay ~= 140;
        this.blocks ~= new Block(72, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 73 -----
        tSprite ~= "coin_use30";
        iDelay ~= 300;
        tSprite ~= "coin_use32";
        iDelay ~= 30;
        tSprite ~= "coin_use31";
        iDelay ~= 130;
        tSprite ~= "coin_use32";
        iDelay ~= 140;
        this.blocks ~= new Block(73, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 74 -----
        tSprite ~= "platform";
        iDelay ~= 0;
        this.blocks ~= new Block(74, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 75 -----
        tSprite ~= "gnd_4";
        iDelay ~= 0;
        this.blocks ~= new Block(75, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 76 -----
        tSprite ~= "bridge_0";
        iDelay ~= 0;
        this.blocks ~= new Block(76, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 77 -----
        tSprite ~= "lava_0";
        iDelay ~= 0;
        this.blocks ~= new Block(77, new Sprite(this.renderer, tSprite, iDelay, false), false, true, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 78 -----
        tSprite ~= "lava_1";
        iDelay ~= 0;
        this.blocks ~= new Block(78, new Sprite(this.renderer, tSprite, iDelay, false), false, true, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 79 -----
        tSprite ~= "bridge_1";
        iDelay ~= 0;
        this.blocks ~= new Block(78, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 80 -----
        tSprite ~= "blockq2_used";
        iDelay ~= 0;
        this.blocks ~= new Block(80, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 81 -----
        tSprite ~= "brick2";
        iDelay ~= 0;
        this.blocks ~= new Block(81, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 82 -----
        tSprite ~= "axe_0";
        iDelay ~= 225;
        tSprite ~= "axe_1";
        iDelay ~= 225;
        tSprite ~= "axe_2";
        iDelay ~= 200;
        this.blocks ~= new Block(82, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 83 ----- END BRIDGE
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(83, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 84 -----
        tSprite ~= "tree_1";
        iDelay ~= 0;
        this.blocks ~= new Block(84, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 85 -----
        tSprite ~= "tree_2";
        iDelay ~= 0;
        this.blocks ~= new Block(85, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 86 -----
        tSprite ~= "tree_3";
        iDelay ~= 0;
        this.blocks ~= new Block(86, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 87 -----
        tSprite ~= "tree1_1";
        iDelay ~= 0;
        this.blocks ~= new Block(87, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 88 -----
        tSprite ~= "tree1_2";
        iDelay ~= 0;
        this.blocks ~= new Block(88, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 89 -----
        tSprite ~= "tree1_3";
        iDelay ~= 0;
        this.blocks ~= new Block(89, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 90 -----
        tSprite ~= "fence";
        iDelay ~= 0;
        this.blocks ~= new Block(90, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 91 -----
        tSprite ~= "tree_0";
        iDelay ~= 0;
        this.blocks ~= new Block(91, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 92 -----
        tSprite ~= "uw_0";
        iDelay ~= 0;
        this.blocks ~= new Block(92, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 93 -----
        tSprite ~= "uw_1";
        iDelay ~= 0;
        this.blocks ~= new Block(93, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 94 -----
        tSprite ~= "water_0";
        iDelay ~= 0;
        this.blocks ~= new Block(94, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 95 -----
        tSprite ~= "water_1";
        iDelay ~= 0;
        this.blocks ~= new Block(95, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 96 -----
        tSprite ~= "bubble";
        iDelay ~= 0;
        this.blocks ~= new Block(96, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 97 -----
        tSprite ~= "pipe2_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(97, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 98 -----
        tSprite ~= "pipe2_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(98, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 99 -----
        tSprite ~= "pipe2_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(99, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 100 -----
        tSprite ~= "pipe2_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(100, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 101 -----
        tSprite ~= "pipe2_hor_bot_right";
        iDelay ~= 0;
        this.blocks ~= new Block(101, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 102 -----
        tSprite ~= "pipe2_hor_top_center";
        iDelay ~= 0;
        this.blocks ~= new Block(102, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 103 -----
        tSprite ~= "pipe2_hor_top_left";
        iDelay ~= 0;
        this.blocks ~= new Block(103, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 104 -----
        tSprite ~= "pipe2_hor_bot_center";
        iDelay ~= 0;
        this.blocks ~= new Block(104, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 105 -----
        tSprite ~= "pipe2_hor_bot_left";
        iDelay ~= 0;
        this.blocks ~= new Block(105, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 106 -----
        tSprite ~= "pipe2_hor_top_right";
        iDelay ~= 0;
        this.blocks ~= new Block(106, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 107 -----
        tSprite ~= "bridge2";
        iDelay ~= 0;
        this.blocks ~= new Block(107, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 108 -----
        tSprite ~= "bridge3";
        iDelay ~= 0;
        this.blocks ~= new Block(108, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 109 -----
        tSprite ~= "platform1";
        iDelay ~= 0;
        this.blocks ~= new Block(109, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 110 -----
        tSprite ~= "water_2";
        iDelay ~= 0;
        this.blocks ~= new Block(110, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 111 -----
        tSprite ~= "water_3";
        iDelay ~= 0;
        this.blocks ~= new Block(111, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 112 -----
        tSprite ~= "pipe3_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(112, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 113 -----
        tSprite ~= "pipe3_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(113, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 114 -----
        tSprite ~= "pipe3_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(114, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 115 -----
        tSprite ~= "pipe3_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(115, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 116 -----
        tSprite ~= "pipe3_hor_bot_right";
        iDelay ~= 0;
        this.blocks ~= new Block(116, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 117 -----
        tSprite ~= "pipe3_hor_top_center";
        iDelay ~= 0;
        this.blocks ~= new Block(117, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 118 -----
        tSprite ~= "pipe3_hor_top_left";
        iDelay ~= 0;
        this.blocks ~= new Block(118, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 119 -----
        tSprite ~= "pipe3_hor_bot_center";
        iDelay ~= 0;
        this.blocks ~= new Block(119, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 120 -----
        tSprite ~= "pipe3_hor_bot_left";
        iDelay ~= 0;
        this.blocks ~= new Block(120, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 121 -----
        tSprite ~= "pipe3_hor_top_right";
        iDelay ~= 0;
        this.blocks ~= new Block(121, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 122 -----
        tSprite ~= "bridge4";
        iDelay ~= 0;
        this.blocks ~= new Block(122, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 123 -----
        tSprite ~= "end1_l";
        iDelay ~= 0;
        this.blocks ~= new Block(123, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 124 -----
        tSprite ~= "end1_dot";
        iDelay ~= 0;
        this.blocks ~= new Block(124, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 125 -----
        tSprite ~= "bonus";
        iDelay ~= 0;
        this.blocks ~= new Block(125, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 126 -----
        tSprite ~= "platformbonus";
        iDelay ~= 0;
        this.blocks ~= new Block(126, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 127 ----- // -- BONUS END
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(127, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 128 ----- // -- SPAWN VINE
        tSprite ~= "brickred";
        iDelay ~= 0;
        this.blocks ~= new Block(128, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 129 ----- // -- SPAWN VINE
        tSprite ~= "brick1";
        iDelay ~= 0;
        this.blocks ~= new Block(129, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 130 -----
        tSprite ~= "vine";
        iDelay ~= 0;
        this.blocks ~= new Block(130, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 131 -----
        tSprite ~= "vine1";
        iDelay ~= 0;
        this.blocks ~= new Block(131, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 132 -----
        tSprite ~= "seesaw_0";
        iDelay ~= 0;
        this.blocks ~= new Block(132, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 133 -----
        tSprite ~= "seesaw_1";
        iDelay ~= 0;
        this.blocks ~= new Block(133, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 134 -----
        tSprite ~= "seesaw_2";
        iDelay ~= 0;
        this.blocks ~= new Block(134, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 135 -----
        tSprite ~= "seesaw_3";
        iDelay ~= 0;
        this.blocks ~= new Block(135, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 136 -----
        tSprite ~= "pipe4_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(136, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 137 -----
        tSprite ~= "pipe4_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(137, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 138 -----
        tSprite ~= "pipe4_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(138, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 139 -----
        tSprite ~= "pipe4_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(139, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 140 -----
        tSprite ~= "t_left1";
        iDelay ~= 0;
        this.blocks ~= new Block(140, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 141 -----
        tSprite ~= "t_center1";
        iDelay ~= 0;
        this.blocks ~= new Block(141, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 142 -----
        tSprite ~= "t_right1";
        iDelay ~= 0;
        this.blocks ~= new Block(142, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 143 -----
        tSprite ~= "t_bot0";
        iDelay ~= 0;
        this.blocks ~= new Block(143, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 144 -----
        tSprite ~= "t_bot1";
        iDelay ~= 0;
        this.blocks ~= new Block(144, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 145 -----
        tSprite ~= "b_top";
        iDelay ~= 0;
        this.blocks ~= new Block(145, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 146 -----
        tSprite ~= "b_top1";
        iDelay ~= 0;
        this.blocks ~= new Block(146, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 147 -----
        tSprite ~= "b_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(147, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 148 -----
        tSprite ~= "cloud_left_bot1";
        iDelay ~= 0;
        this.blocks ~= new Block(148, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 149 -----
        tSprite ~= "cloud_center_bot1";
        iDelay ~= 0;
        this.blocks ~= new Block(149, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 150 -----
        tSprite ~= "cloud_center_top1";
        iDelay ~= 0;
        this.blocks ~= new Block(150, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 151 -----
        tSprite ~= "t_left2";
        iDelay ~= 0;
        this.blocks ~= new Block(151, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 152 -----
        tSprite ~= "t_center2";
        iDelay ~= 0;
        this.blocks ~= new Block(152, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 153 -----
        tSprite ~= "t_right2";
        iDelay ~= 0;
        this.blocks ~= new Block(153, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 154 -----
        tSprite ~= "t_bot2";
        iDelay ~= 0;
        this.blocks ~= new Block(154, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 155 -----
        tSprite ~= "castle1_brick";
        iDelay ~= 0;
        this.blocks ~= new Block(155, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 156 -----
        tSprite ~= "castle1_top0";
        iDelay ~= 0;
        this.blocks ~= new Block(156, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 157 -----
        tSprite ~= "castle1_top1";
        iDelay ~= 0;
        this.blocks ~= new Block(157, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 158 -----
        tSprite ~= "castle1_center_center_top";
        iDelay ~= 0;
        this.blocks ~= new Block(158, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 159 -----
        tSprite ~= "castle1_center_center";
        iDelay ~= 0;
        this.blocks ~= new Block(159, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 160 -----
        tSprite ~= "castle1_center_left";
        iDelay ~= 0;
        this.blocks ~= new Block(160, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 161 -----
        tSprite ~= "castle1_center_right";
        iDelay ~= 0;
        this.blocks ~= new Block(161, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 162 -----
        tSprite ~= "seesaw1_0";
        iDelay ~= 0;
        this.blocks ~= new Block(162, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 163 -----
        tSprite ~= "seesaw1_1";
        iDelay ~= 0;
        this.blocks ~= new Block(163, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 164 -----
        tSprite ~= "seesaw1_2";
        iDelay ~= 0;
        this.blocks ~= new Block(164, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 165 -----
        tSprite ~= "seesaw1_3";
        iDelay ~= 0;
        this.blocks ~= new Block(165, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 166 -----
        tSprite ~= "gnd2";
        iDelay ~= 0;
        this.blocks ~= new Block(166, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 167 -----
        tSprite ~= "gnd2_2";
        iDelay ~= 0;
        this.blocks ~= new Block(167, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 168 -----
        tSprite ~= "end1_flag";
        iDelay ~= 0;
        this.blocks ~= new Block(168, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 169 ----- TP
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(169, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 170 ----- TP2
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(170, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 171 ----- TP3 - bTP
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(171, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 172 -----
        tSprite ~= "pipe5_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(172, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 173 -----
        tSprite ~= "pipe5_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(173, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 174 -----
        tSprite ~= "pipe5_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(174, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 175 -----
        tSprite ~= "pipe5_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(175, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 176 -----
        tSprite ~= "pipe6_left_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(176, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 177 -----
        tSprite ~= "pipe6_left_top";
        iDelay ~= 0;
        this.blocks ~= new Block(177, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 178 -----
        tSprite ~= "pipe6_right_bot";
        iDelay ~= 0;
        this.blocks ~= new Block(178, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 179 -----
        tSprite ~= "pipe6_right_top";
        iDelay ~= 0;
        this.blocks ~= new Block(179, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 180 -----
        tSprite ~= "crown";
        iDelay ~= 0;
        this.blocks ~= new Block(180, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 181 -----
        tSprite ~= "gnd_5";
        iDelay ~= 0;
        this.blocks ~= new Block(166, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 182 ----- ENDUSE
        tSprite ~= "transp";
        iDelay ~= 0;
        this.blocks ~= new Block(182, new Sprite(this.renderer, tSprite, iDelay, false), false, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        this.iBlockSize = to!int(blocks.length);
    }

    void loadMinions()
    {
        string[] newSprite;
        string[] tSprite;
        uint[] iDelay;
        uint[] newDelay;

        // ----- 0 -----
        tSprite ~= "goombas_0";
        iDelay ~= 200;
        tSprite ~= "goombas_1";
        iDelay ~= 200;
        this.minions ~= new Block(0, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 1 -----
        tSprite ~= "goombas_ded";
        iDelay ~= 0;
        this.minions ~= new Block(1, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 2 -----
        tSprite ~= "mushroom";
        iDelay ~= 0;
        this.minions ~= new Block(2, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 3 -----
        tSprite ~= "mushroom_1up";
        iDelay ~= 0;
        this.minions ~= new Block(3, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 4 -----
        tSprite ~= "koopa_0";
        iDelay ~= 200;
        tSprite ~= "koopa_1";
        iDelay ~= 200;
        this.minions ~= new Block(4, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 5 -----
        tSprite ~= "koopa_ded";
        iDelay ~= 0;
        this.minions ~= new Block(5, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 6 -----
        tSprite ~= "flower0";
        iDelay ~= 50;
        tSprite ~= "flower1";
        iDelay ~= 50;
        tSprite ~= "flower2";
        iDelay ~= 50;
        tSprite ~= "flower3";
        iDelay ~= 50;
        this.minions ~= new Block(6, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 7 -----
        tSprite ~= "koopa_2";
        iDelay ~= 200;
        tSprite ~= "koopa_3";
        iDelay ~= 200;
        this.minions ~= new Block(7, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 8 -----
        tSprite ~= "goombas1_0";
        iDelay ~= 200;
        tSprite ~= "goombas1_1";
        iDelay ~= 200;
        this.minions ~= new Block(8, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 9 -----
        tSprite ~= "goombas1_ded";
        iDelay ~= 0;
        this.minions ~= new Block(9, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 10 -----
        tSprite ~= "goombas2_0";
        iDelay ~= 200;
        tSprite ~= "goombas2_1";
        iDelay ~= 200;
        this.minions ~= new Block(10, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 11 -----
        tSprite ~= "goombas2_ded";
        iDelay ~= 0;
        this.minions ~= new Block(11, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 12 -----
        tSprite ~= "koopa1_0";
        iDelay ~= 200;
        tSprite ~= "koopa1_1";
        iDelay ~= 200;
        this.minions ~= new Block(12, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 13 -----
        tSprite ~= "koopa1_ded";
        iDelay ~= 0;
        this.minions ~= new Block(13, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 14 -----
        tSprite ~= "koopa1_2";
        iDelay ~= 200;
        tSprite ~= "koopa1_3";
        iDelay ~= 200;
        this.minions ~= new Block(14, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 15 -----
        tSprite ~= "koopa2_0";
        iDelay ~= 200;
        tSprite ~= "koopa2_1";
        iDelay ~= 200;
        this.minions ~= new Block(15, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 16 -----
        tSprite ~= "koopa2_ded";
        iDelay ~= 0;
        this.minions ~= new Block(16, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 17 -----
        tSprite ~= "koopa2_2";
        iDelay ~= 200;
        tSprite ~= "koopa2_3";
        iDelay ~= 200;
        this.minions ~= new Block(17, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 18 -----
        tSprite ~= "plant_0";
        iDelay ~= 125;
        tSprite ~= "plant_1";
        iDelay ~= 125;
        this.minions ~= new Block(18, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 19 -----
        tSprite ~= "plant1_0";
        iDelay ~= 125;
        tSprite ~= "plant1_1";
        iDelay ~= 125;
        this.minions ~= new Block(19, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 20 -----
        tSprite ~= "bowser0";
        iDelay ~= 285;
        tSprite ~= "bowser1";
        iDelay ~= 285;
        this.minions ~= new Block(20, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 21 -----
        tSprite ~= "bowser2";
        iDelay ~= 285;
        tSprite ~= "bowser3";
        iDelay ~= 285;
        this.minions ~= new Block(21, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 22 -----
        tSprite ~= "fire_0";
        iDelay ~= 35;
        tSprite ~= "fire_1";
        iDelay ~= 35;
        this.minions ~= new Block(22, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 23 -----
        tSprite ~= "fireball_0";
        iDelay ~= 75;
        tSprite ~= "fireball_1";
        iDelay ~= 75;
        tSprite ~= "fireball_2";
        iDelay ~= 75;
        tSprite ~= "fireball_3";
        iDelay ~= 75;
        this.minions ~= new Block(23, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 24 -----
        tSprite ~= "star_0";
        iDelay ~= 75;
        tSprite ~= "star_1";
        iDelay ~= 75;
        tSprite ~= "star_2";
        iDelay ~= 75;
        tSprite ~= "star_3";
        iDelay ~= 75;
        this.minions ~= new Block(24, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 25 -----
        tSprite ~= "mushroom1_1up";
        iDelay ~= 0;
        this.minions ~= new Block(25, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 26 -----
        tSprite ~= "toad";
        iDelay ~= 0;
        this.minions ~= new Block(26, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 27 -----
        tSprite ~= "peach";
        iDelay ~= 0;
        this.minions ~= new Block(27, new Sprite(this.renderer, tSprite, iDelay, false), false, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 28 -----
        tSprite ~= "squid0";
        iDelay ~= 0;
        this.minions ~= new Block(28, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 29 -----
        tSprite ~= "squid1";
        iDelay ~= 0;
        this.minions ~= new Block(29, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 30 -----
        tSprite ~= "cheep0";
        iDelay ~= 120;
        tSprite ~= "cheep1";
        iDelay ~= 120;
        this.minions ~= new Block(30, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 31 -----
        tSprite ~= "cheep2";
        iDelay ~= 110;
        tSprite ~= "cheep3";
        iDelay ~= 110;
        this.minions ~= new Block(31, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 32 -----
        tSprite ~= "upfire";
        iDelay ~= 0;
        this.minions ~= new Block(32, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 33 -----
        tSprite ~= "vine_top";
        iDelay ~= 0;
        this.minions ~= new Block(33, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 34 -----
        tSprite ~= "vine";
        iDelay ~= 0;
        this.minions ~= new Block(34, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 35 -----
        tSprite ~= "vine1_top";
        iDelay ~= 0;
        this.minions ~= new Block(35, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 36 -----
        tSprite ~= "vine1";
        iDelay ~= 0;
        this.minions ~= new Block(36, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 37 -----
        tSprite ~= "spring_0";
        iDelay ~= 0;
        this.minions ~= new Block(37, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 38 -----
        tSprite ~= "spring_1";
        iDelay ~= 0;
        this.minions ~= new Block(38, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 39 -----
        tSprite ~= "spring_2";
        iDelay ~= 0;
        this.minions ~= new Block(39, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 40 -----
        tSprite ~= "spring1_0";
        iDelay ~= 0;
        this.minions ~= new Block(40, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 41 -----
        tSprite ~= "spring1_1";
        iDelay ~= 0;
        this.minions ~= new Block(41, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 42 -----
        tSprite ~= "spring1_2";
        iDelay ~= 0;
        this.minions ~= new Block(42, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 43 -----
        tSprite ~= "hammerbro_0";
        iDelay ~= 175;
        tSprite ~= "hammerbro_1";
        iDelay ~= 175;
        this.minions ~= new Block(43, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 44 -----
        tSprite ~= "hammerbro_2";
        iDelay ~= 155;
        tSprite ~= "hammerbro_3";
        iDelay ~= 155;
        this.minions ~= new Block(44, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 45 -----
        tSprite ~= "hammerbro1_0";
        iDelay ~= 175;
        tSprite ~= "hammerbro1_1";
        iDelay ~= 175;
        this.minions ~= new Block(45, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 46 -----
        tSprite ~= "hammerbro1_2";
        iDelay ~= 155;
        tSprite ~= "hammerbro1_3";
        iDelay ~= 155;
        this.minions ~= new Block(46, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 47 -----
        tSprite ~= "hammer_0";
        iDelay ~= 95;
        tSprite ~= "hammer_1";
        iDelay ~= 95;
        tSprite ~= "hammer_2";
        iDelay ~= 95;
        tSprite ~= "hammer_3";
        iDelay ~= 95;
        this.minions ~= new Block(47, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 48 -----
        tSprite ~= "hammer1_0";
        iDelay ~= 95;
        tSprite ~= "hammer1_1";
        iDelay ~= 95;
        tSprite ~= "hammer1_2";
        iDelay ~= 95;
        tSprite ~= "hammer1_3";
        iDelay ~= 95;
        this.minions ~= new Block(48, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 49 -----
        tSprite ~= "lakito_0";
        iDelay ~= 0;
        this.minions ~= new Block(49, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 50 -----
        tSprite ~= "lakito_1";
        iDelay ~= 0;
        this.minions ~= new Block(50, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 51 -----
        tSprite ~= "spikey0_0";
        iDelay ~= 135;
        tSprite ~= "spikey0_1";
        iDelay ~= 135;
        this.minions ~= new Block(51, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 52 -----
        tSprite ~= "spikey1_0";
        iDelay ~= 75;
        tSprite ~= "spikey1_1";
        iDelay ~= 75;
        this.minions ~= new Block(52, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 53 -----
        tSprite ~= "beetle_0";
        iDelay ~= 155;
        tSprite ~= "beetle_1";
        iDelay ~= 155;
        this.minions ~= new Block(53, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 54 -----
        tSprite ~= "beetle_2";
        iDelay ~= 0;
        this.minions ~= new Block(54, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 55 -----
        tSprite ~= "beetle1_0";
        iDelay ~= 155;
        tSprite ~= "beetle1_1";
        iDelay ~= 155;
        this.minions ~= new Block(55, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 56 -----
        tSprite ~= "beetle1_2";
        iDelay ~= 0;
        this.minions ~= new Block(56, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 57 -----
        tSprite ~= "beetle2_0";
        iDelay ~= 155;
        tSprite ~= "beetle2_1";
        iDelay ~= 155;
        this.minions ~= new Block(57, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 58 -----
        tSprite ~= "beetle2_2";
        iDelay ~= 0;
        this.minions ~= new Block(58, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 59 -----
        tSprite ~= "bulletbill";
        iDelay ~= 0;
        this.minions ~= new Block(59, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 60 -----
        tSprite ~= "bulletbill1";
        iDelay ~= 0;
        this.minions ~= new Block(60, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 61 -----
        tSprite ~= "hammer1_0";
        iDelay ~= 0;
        this.minions ~= new Block(61, new Sprite(this.renderer, tSprite, iDelay, false), true, false, true, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 62 -----
        tSprite ~= "fireball_0";
        iDelay ~= 155;
        tSprite ~= "fireball_1";
        iDelay ~= 155;
        tSprite ~= "fireball_2";
        iDelay ~= 155;
        tSprite ~= "fireball_3";
        iDelay ~= 155;
        this.minions ~= new Block(62, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 63 -----
        tSprite ~= "firework0";
        iDelay ~= 0;
        this.minions ~= new Block(63, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 64 -----
        tSprite ~= "firework1";
        iDelay ~= 0;
        this.minions ~= new Block(64, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;
        // ----- 65 -----
        tSprite ~= "firework1";
        iDelay ~= 0;
        this.minions ~= new Block(65, new Sprite(this.renderer, tSprite, iDelay, false), true, false, false, true);
        tSprite = newSprite;
        iDelay = newDelay;

        this.iMinionSize = to!int(this.minions.length);
    }
}
