
import derelict.sdl2.sdl;

class Event
{

	private:
	bool bState; // true = OLD, false = NEW
	uint stepID;

	public:
	enum animationType {
		eTOP,
		eRIGHT,
		eRIGHTEND,
		eBOT,
		eLEFT,
		eBOTRIGHTEND,
		eENDBOT1,
		eENDBOT2,
		eENDPOINTS,
		eDEATHNOTHING,
		eDEATHTOP,
		eDEATHBOT,
		eNOTHING,
		ePLAYPIPERIGHT,
		ePLAYPIPETOP,
		eLOADINGMENU,
		eGAMEOVER,
		eBOSSEND1,
		eBOSSEND2,
		eBOSSEND3,
		eBOSSEND4,
		eBOTRIGHTBOSS,
		eBOSSTEXT1,
		eBOSSTEXT2,
		eENDGAMEBOSSTEXT1,
		eENDGAMEBOSSTEXT2,
		eMARIOSPRITE1,
		eVINE1,
		eVINE2,
		eVINESPAWN,
	};

	animationType[] vOLDDir;
	int[] vOLDLength;

	animationType[] vNEWDir;
	int[] vNEWLength;

	int[] reDrawX;
	int[] reDrawY;
	int iSpeed;

	int newLevelType;
	int newMapXPos;
	int newPlayerXPos;
	int newPlayerYPos;
	bool newMoveMap;

	uint iTime;
	int iDelay;

	int newCurrentLevel;
	bool inEvent;
	bool newUnderWater;
	bool endGame;

	enum eventType {
		eNormal,
		eEnd,
		eBossEnd,
	};

	int eventTypeID;

	void Normal()
	{

	}

	void end()
	{

	}

	void resetData()
	{

	}

	// ----- Methods

	void Draw(SDL_Renderer* rR)
	{

	}

	void Animation()
	{

	}

	void newLevel()
	{

	}

	void resetRedraw()
	{

	}

};
