module inputhandler;

import derelict.sdl2.sdl;
import event;

class InputHandler
{
    SDL_Event* mainEvent;

    this (SDL_Event* mainEvent)
    {
        this.mainEvent = mainEvent;
    }

    SDL_Event* getEvent()
    {
        return mainEvent;
    }
}
