module menus.mainmenu;
import menus.menuinterface;
import menus.menuoption;
import graphics.image;
import drawers.text;
import derelict.sdl2.sdl;
import std.conv;
import inputhandler;
import gamecore;

class MainMenu : MenuInterface
{
    private:
      MenuOption[] menuOptions;
      TextDrawer textDrawer;
      bool selectWorld;
  	  int activeWorldID, activeSecondWorldID;
      SDL_Rect rSelectWorld;
      SDL_Renderer* renderer;
      InputHandler inputHandler;
      Image activeOption;
      int activeMenuOption;
      int numOfMenuOptions = 3;

      static bool movePressed, keyMenuPressed, keyS, keyW, keyA, keyD, keyShift;
    	static bool keyAPressed, keyDPressed;

    public:
    this(SDL_Renderer* renderer, TextDrawer textDrawer, InputHandler inputHandler)
    {
        this.inputHandler = inputHandler;
        this.renderer = renderer;
        this.textDrawer = textDrawer;

        this.menuOptions ~= new MenuOption("1 PLAYER GAME", 178, 276);
        this.menuOptions ~= new MenuOption("OPTIONS", 222, 308);
        this.menuOptions ~= new MenuOption("ABOUT", 237, 340);

        this.activeMenuOption = 0;
        this.selectWorld = false;
        this.activeOption = new Image("active_option", renderer);
        this.rSelectWorld.x = 122;
        this.rSelectWorld.y = 280;
        this.rSelectWorld.w = 306;
        this.rSelectWorld.h = 72;
        this.activeWorldID = this.activeSecondWorldID = 0;
    }

    void listenInput()
    {

      if(this.inputHandler.getEvent().type == SDL_KEYDOWN) {
          switch(this.inputHandler.getEvent().key.keysym.sym) {
            case SDLK_s: case SDLK_DOWN:
              if(!keyMenuPressed) {
                this.keyPressed(2);
                keyMenuPressed = true;
              }
              break;
            case SDLK_w: case SDLK_UP:
              if(!keyMenuPressed) {
                this.keyPressed(0);
                keyMenuPressed = true;
              }
              break;
            case SDLK_KP_ENTER: case SDLK_RETURN:
              if(!keyMenuPressed) {
                this.enter();
                keyMenuPressed = true;
              }
              break;
            case SDLK_ESCAPE:
              if(!keyMenuPressed) {
                this.selectWorld = false;
                keyMenuPressed = true;
              }
              break;
            case SDLK_LEFT: case SDLK_d:
              if(!keyMenuPressed) {
                this.keyPressed(3);
                keyMenuPressed = true;
              }
              break;
            case SDLK_RIGHT: case SDLK_a:
              if(!keyMenuPressed) {
                this.keyPressed(1);
                keyMenuPressed = true;
              }
              break;
            default: break;
          }
      }

      if(this.inputHandler.getEvent().type == SDL_KEYUP) {
        switch(this.inputHandler.getEvent().key.keysym.sym) {
          case SDLK_s: case SDLK_DOWN: case SDLK_w: case SDLK_UP: case SDLK_KP_ENTER: case SDLK_RETURN: case SDLK_ESCAPE: case SDLK_a: case SDLK_RIGHT: case SDLK_LEFT: case SDLK_d:
            keyMenuPressed = false;
            break;
          default:
            break;
        }
      }
    }

    void enter() {
    	switch(activeMenuOption) {
    		case 0:
    			if(!selectWorld) {
    				selectWorld = true;
    			} else {
            // writeln(activeWorldID);
            // writeln(activeWorldID * 4 + activeSecondWorldID);

            Core.instance().getLevelManager().loading();

    				// CCFG::getMM()->getLoadingMenu()->updateTime();
    				// CCore::getMap()->resetGameData();
    				// CCore::getMap()->setCurrentLevelID(activeWorldID * 4 + activeSecondWorldID);
    				// CCFG::getMM()->setViewID(CCFG::getMM()->eGameLoading);
    				// CCFG::getMM()->getLoadingMenu()->loadingType = true;
    				// CCore::getMap()->setSpawnPointID(0);
    				selectWorld = false;
    			}
    			break;
    		// case 1:
    		// 	CCFG::getMM()->getOptions()->setEscapeToMainMenu(true);
    		// 	CCFG::getMM()->resetActiveOptionID(CCFG::getMM()->eOptions);
    		// 	CCFG::getMM()->getOptions()->updateVolumeRect();
    		// 	CCFG::getMM()->setViewID(CCFG::getMM()->eOptions);
    		// 	break;
    		// case 2:
    		// 	CCFG::getMM()->getAboutMenu()->updateTime();
    		// 	CCFG::getMM()->setViewID(CCFG::getMM()->eAbout);
    		// 	CCFG::getMusic()->PlayMusic(CCFG::getMusic()->mOVERWORLD);
        //  break;
          default:
          break;
    	}
    }

    private void keyPressed(int iDir)
    {
      switch(iDir) {
        case 0: case 2:
          if(!selectWorld) {
            switch(iDir) {
              case 0:
                if (activeMenuOption - 1 < 0) {
                  activeMenuOption = numOfMenuOptions - 1;
                } else {
                  --activeMenuOption;
                }
                break;
              case 2:
                if (activeMenuOption + 1 >= numOfMenuOptions) {
                  activeMenuOption = 0;
                } else {
                  ++activeMenuOption;
                }
                break;
              default:
                break;
            }
          } else {
            switch(iDir) {
              case 0:
                if(activeSecondWorldID < 1) {
                  activeSecondWorldID = 3;
                } else {
                  --activeSecondWorldID;
                }
                break;
              case 2:
                if(activeSecondWorldID > 2) {
                  activeSecondWorldID = 0;
                } else {
                  ++activeSecondWorldID;
                }
                break;
              default: break;
            }
          }
          break;
        case 1:
          if(selectWorld) {
            if(activeWorldID < 7) {
              ++activeWorldID;
            } else {
              activeWorldID = 0;
            }
          }
          break;
        case 3:
          if(selectWorld) {
            if(activeWorldID > 0) {
              --activeWorldID;
            } else {
              activeWorldID = 7;
            }
          }
          break;
          default: break;
      }
    }

    void draw()
    {
        foreach (MenuOption mO; this.menuOptions) {
            this.textDrawer.draw(mO.getText(), mO.getXPos(), mO.getYPos());
        }

        this.activeOption.draw(this.menuOptions[activeMenuOption].getXPos() - 32, this.menuOptions[activeMenuOption].getYPos());

        if(this.selectWorld) {
      		SDL_SetRenderDrawBlendMode(this.renderer, SDL_BLENDMODE_BLEND);
      		SDL_SetRenderDrawColor(this.renderer, 4, 4, 4, 235);
      		SDL_RenderFillRect(this.renderer, &rSelectWorld);
      		SDL_SetRenderDrawColor(this.renderer, 255, 255, 255, 255);
      		rSelectWorld.x += 1;
      		rSelectWorld.y += 1;
      		rSelectWorld.h -= 2;
      		rSelectWorld.w -= 2;

      		SDL_RenderDrawRect(this.renderer, &rSelectWorld);
      		rSelectWorld.x -= 1;
      		rSelectWorld.y -= 1;
      		rSelectWorld.h += 2;
      		rSelectWorld.w += 2;

      		this.textDrawer.draw("SELECT WORLD", rSelectWorld.x + rSelectWorld.w/2 - this.textDrawer.getTextWidth("SELECT WORLD")/2, rSelectWorld.y + 16, 16, 255, 255, 255);

      		for(int i = 0, extraX = 0; i < 8; i++) {
      			if(i == activeWorldID) {
      				this.textDrawer.draw(to!string(i + 1) ~ "-" ~ to!string(activeSecondWorldID + 1), rSelectWorld.x + 16*(i + 1) + 16*i + extraX, rSelectWorld.y + 16 + 24, 16, 255, 255, 255);
      				extraX = 32;
      			} else {
      				this.textDrawer.draw(to!string(i + 1), rSelectWorld.x + 16*(i + 1) + 16*i + extraX, rSelectWorld.y + 16 + 24, 16, 90, 90, 90);
      			}
      		}

    		SDL_SetRenderDrawBlendMode(this.renderer, SDL_BLENDMODE_NONE);
    		SDL_SetRenderDrawColor(this.renderer, 93, 148, 252, 255);
    	}


    }

    void update()
    {

    }

}
