module menus.menuinterface;
import derelict.sdl2.sdl;

interface MenuInterface
{
   void draw();
   void update();
}
