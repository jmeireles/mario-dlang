module menus.menuoption;

class MenuOption
{

private:
	string sText;
	int iXPos, iYPos;

public:
	this(string sText, int iXPos, int iYPos)
  {
    	this.sText = sText;
    	this.iXPos = iXPos;
    	this.iYPos = iYPos;
  }

	~this() {}

	string getText()
	{
      return this.sText;
  }

	void setText(string sText)
  {
    	this.sText = sText;
  }

	int getXPos()
  {
      return this.iXPos;
  }

	int getYPos()
  {
      return this.iYPos;
  }

};
