module levels.leveltext;

class LevelText
{
private:
	int iXPos, iYPos;
	string sText;

public:
	this(int iXPos, int iYPos, string sText)
  {
    this.iXPos = iXPos;
    this.iYPos = iYPos;
    this.sText = sText;
  }

	~this(){}

	int getXPos()
  {
    return this.iXPos;
  }

	int getYPos()
  {
      return this.iYPos;
  }

	string getText()
  {
      return this.sText;
  }

	void setText(string sText)
  {
     this.sText = sText;
  }
}
