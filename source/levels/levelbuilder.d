module levels.levelbuilder;

struct Coords
{
    int x;
    int y;
    bool direction;
}

struct Data
{
    int x;
    int y;
    int size;
};

struct CastleData
{
    int x;
    int y;
}

struct MinionsData
{
    enum minionsType {
        GOOMBAS
    };

    int type;
    Coords coords;

    this(minionsType type, int x, int y, bool dir)
    {
        this.coords = Coords(x, y, dir);
    }
}

class LevelHelper
{
  static:

    CastleData createCastle(int x, int y)
    {
        return CastleData(x, y);
    }

    Data createEnd(int x, int y, int size)
    {
        return Data(x, y, size);
    }

    Data createCloud(int x, int y, int size)
    {
        return Data(x, y, size);
    }

    Data createBush(int x, int y, int size)
    {
        return Data(x, y, size);
    }

    Data createBrick(int x, int y, int size)
    {
        return Data(x, y, size);
    }

    Data createPipe(int x, int y, int size)
    {
        return Data(x, y, size);
    }

    Data createGrass(int x, int y, int size)
    {
        return Data(x, y, size);
    }

    MinionsData createGoombas(int x, int y, bool dir)
    {
        return MinionsData(MinionsData.minionsType.GOOMBAS, x, y, dir);
    }

    private:
      MinionsData generateEnemy(MinionsData.minionsType type, int x, int y, bool dir)
      {
          return MinionsData(type, x, y, dir);
      }
}
