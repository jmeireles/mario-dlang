module levels.loading;

import derelict.sdl2.sdl;
import std.conv;

import menus.menuinterface;
import graphics.image;
import drawers.text;
import inputhandler;
import graphics.block;
import levels.levelinterface;
import levels.level;
import gamedata;
import drawers.text;
import std.stdio;
import inputhandler;
import drawers.generic;
import gamecore;

class Loading : AbstractLevel
{
  protected uint iTime = 0;

  this (GameData gameData, TextDrawer textDrawer, SDL_Renderer* renderer, InputHandler inputHandler)
  {
     super(gameData, textDrawer, renderer);
  }

  void update()
  {

     this.gameData.getBlock(2).getSprite().update();
     this.gameData.getBlock(180).getSprite().update();

     if(SDL_GetTicks() >= iTime + 4000) {
        Core.instance().getLevelManager().start();
    }
  }

  void load()
  {
      Core.instance().getMusic().StopMusic();
      this.iMapWidth = 260;
      this.iMapHeight = 25;
      this.iLevelType = 0;
      this.iMapTime = 400;
  }

  void draw()
  {
      GenericDrawer.setBackgroundColor(this.renderer, 0, 0, 0, 255);
      this.levelLayout();

      this.textDrawer.draw("WORLD", 320, 144);
      this.textDrawer.draw("1-1", 416, 144);
      this.gameData.getPlayer().getMarioSprite().getTexture().draw(342, 210 - this.gameData.getPlayer().getHitBoxY()/2);
      this.textDrawer.draw("y", 384, 208);

      if(this.gameData.getPlayer().getNumOfLives() > 9) {
        this.gameData.getBlock(180).getSprite().getTexture().draw(410, 210);
      }

      this.textDrawer.draw(to!string(this.gameData.getPlayer().getNumOfLives()), 432, 208);
      this.textDrawer.drawCenterX("REMEMBER THAT YOU CAN RUN WITH " ~ "LEFT SHIFT", 400, 16);
  }

  void updateTime()
  {
      this.iTime = SDL_GetTicks();
  }

  override string getLevelName()
  {
      return "1-1";
  }

};
