module levels.maplevel;

class MapLevel
{
private:
	int iBlockID;

	bool spawnMushroom;
	bool powerUP; // -- true = powerUP, false = 1UP
	bool spawnStar;
	int iNumOfUse;

	// ----- Animation -----

	bool blockAnimation;
	int iYPos;
	bool bYDirection;	// ----- true TOP, false BOTTOM

	// ----- Animation -----

public:
	this(int iBlockID)
	{
		this.iBlockID = iBlockID;

		this.iNumOfUse = 0;

		this.blockAnimation = false;
		this.iYPos = 0;
		this.bYDirection = true;

		this.spawnMushroom = false;
		this.spawnStar = false;

		this.powerUP = true;
	}

	~this()	{}

	void startBlockAnimation()
	{
		this.blockAnimation = true;
		this.iYPos = 0;
		this.bYDirection = true;
	}

	int updateYPos()
	{
		if(blockAnimation) {
			if (bYDirection)
			{
				if (iYPos < 10)
				{
					if (iYPos < 5) {
						iYPos += 2;
					} else {
						 iYPos += 1;
					}
				}
				else {
					bYDirection = false;
				}
			}
			else {
				if (iYPos > 0) {
					if (iYPos > 5) {
						iYPos -= 2;
					} else {
						iYPos -= 1;
					}
				}
				else {
					blockAnimation = false;
				}
			}
		}

		return iYPos;
	}

	int getBlockID()
	{
			return iBlockID;
	}

	void setBlockID(int iBlockID)
	{
			this.iBlockID = iBlockID;
	}

	int getYPos()
	{
			return iYPos;
	}

	int getNumOfUse()
	{
			return iNumOfUse;
	}

	void setNumOfUse(int iNumOfUse)
	{
			this.iNumOfUse = iNumOfUse;
	}

	bool getSpawnMushroom()
	{
			return spawnMushroom;
	}

	void setSpawnMushroom(bool spawnMushroom)
	{
			this.spawnMushroom = spawnMushroom;
	}

	bool getPowerUP()
	{
			return powerUP;
	}

	void setPowerUP(bool powerUP)
	{
			this.powerUP = powerUP;
	}

	bool getSpawnStar()
	{
			return spawnStar;
	}

	void setSpawnStar(bool spawnStar)
	{
			this.spawnStar = spawnStar;
	}

};
