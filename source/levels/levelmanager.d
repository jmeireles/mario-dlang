module levels.levelmanager;
import derelict.sdl2.sdl;

import levels.levelinterface;
import gamedata;
import levels.level;
import levels.level1;
import levels.intro;
import levels.loading;
import levels.gameover;
import levels.levelloader;

import drawers.text;
import inputhandler;
import std.stdio;

class LevelManager
{

  enum levelsList {
      INTRO,
      LOADING,
      GAMEOVER,
      LEVEL1,
      LEVEL1_1
  };

  int currentLevel = levelsList.INTRO;
  LevelInterface[int] levels;
  LevelLoader levelLoader;

  this(GameData gameData, TextDrawer textDrawer, SDL_Renderer* renderer, InputHandler inputHandler)
  {
     levelLoader = new LevelLoader(gameData, textDrawer, renderer);

     levels[levelsList.INTRO] = new Intro(gameData, textDrawer, renderer, inputHandler);
     levels[levelsList.LOADING] = new Loading(gameData, textDrawer, renderer, inputHandler);
     levels[levelsList.GAMEOVER] = new GameOver(gameData, textDrawer, renderer, inputHandler);
     levels[levelsList.LEVEL1] = new Level1( gameData, textDrawer, renderer, inputHandler );
  }

  void setGameOver()
  {
      this.currentLevel = levelsList.GAMEOVER;
      this.loadLevel();
  }

  int getLevelType()
  {
      return 1;
  }

  LevelInterface getCurrentLevel()
  {
     return levels[currentLevel];
    //  return levelLoader;
  }

  void loading()
  {
    this.currentLevel = levelsList.LOADING;
    this.loadLevel();
  }

  void start()
  {
    this.currentLevel = levelsList.LEVEL1;
    this.loadLevel();
  }

  void setCurrentLevel(int level)
  {
      this.currentLevel = level;
  }

  void loadLevel()
  {
      levels[currentLevel].load();
    //  levelLoader.load();
  }

  void update()
  {
      levels[currentLevel].update();
    //  levelLoader.update();
  }

  void draw()
  {
      levels[currentLevel].draw();
      //levelLoader.draw();
  }

}
