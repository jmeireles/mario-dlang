module levels.gameover;

import derelict.sdl2.sdl;
import std.conv;

import menus.menuinterface;
import drawers.generic;
import graphics.image;
import drawers.text;
import inputhandler;
import graphics.block;
import levels.levelinterface;
import levels.level;
import gamedata;
import drawers.text;
import std.stdio;
import inputhandler;

class GameOver : AbstractLevel
{
  protected uint iTime = 0;

  this (GameData gameData, TextDrawer textDrawer, SDL_Renderer* renderer, InputHandler inputHandler)
  {
     super(gameData, textDrawer, renderer);
  }

  void update()
  {
     if(SDL_GetTicks() >= iTime + 2500) {
       //CCFG::getMusic()->StopMusic();
       //CCore::getMap()->UpdateBlocks();
    }
  }

  void load()
  {

  }

  void draw()
  {
     GenericDrawer.setBackgroundColor(this.renderer, 0, 0, 0, 255);
     this.levelLayout();
     this.textDrawer.drawCenterX("GAME OVER", 240, 16);
  }

  void updateTime()
  {
      this.iTime = SDL_GetTicks();
  }

  override string getLevelName()
  {
      return "gameover";
  }

};
