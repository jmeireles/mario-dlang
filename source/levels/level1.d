module levels.level1;

import derelict.sdl2.sdl;
import std.conv;

import graphics.image;
import graphics.block;
import levels.levelinterface;
import levels.level;
import gamedata;
import drawers.text;
import std.stdio;
import menus.mainmenu;
import inputhandler;
import graphics.pipe;
import graphics.platform;
import graphics.points;
import graphics.coin;
import config;
import drawers.generic;
import graphics.minion;
import gamecore;
import std.algorithm : remove;

class Level1 : AbstractLevel
{

  private InputHandler inputHandler;

  int[11] blocksToLoad = [
    57, 2, 8, 29, 55, 57, 70, 71, 72, 73, 82
  ];

  this (GameData gameData, TextDrawer textDrawer, SDL_Renderer* renderer, InputHandler inputHandler)
  {
     super(gameData, textDrawer, renderer);
     this.inputHandler = inputHandler;
     this.id = 1;
     //this.mainMenu = new MainMenu(renderer, textDrawer, inputHandler);
  }

  void update()
  {
    foreach (int block; this.blocksToLoad) {
        this.gameData.getBlock(block).getSprite().update();
    }

     updateMinionsCollisions();

     if(!Core.instance().getPlayer().getInLevelAnimation()) {
   		UpdateMinionBlokcs();
   		UpdateMinions();

   		if(!inEvent) {
   			UpdatePlayer();

   			++iFrameID;
   			if(iFrameID > 32) {
   				iFrameID = 0;
   				if(iMapTime > 0) {
   					--iMapTime;
   					if(iMapTime == 90) {
   						Core.instance().getMusic().StopMusic();
   						Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cLOWTIME);
   					} else if(iMapTime == 86) {
   						Core.instance().getMusic().changeMusic(true, true);
   					}

   					if(iMapTime <= 0) {
   						playerDeath(true, true);
   					}
   				}
   			}
   		} else {
   			//oEvent->Animation();
   		}

      foreach (Platform plat; this.platforms) {
          plat.update();
      }

   	} else {
   		Core.instance().getPlayer().powerUPAnimation();
   	}

    foreach (int i, Points points; this.lPoints) {
  		if(!points.getDelete()) {
  			points.update();
  		} else {
        this.lPoints = this.lPoints.remove(i);
  		}
  	}

    foreach (int i, Coin coin; this.lCoin) {
        if(!coin.getDelete()) {
          coin.update();
        } else {
          lPoints ~= new Points(coin.getXPos(), coin.getYPos(), "200");
          this.lCoin = this.lCoin.remove(i);
        }
    }


  }

  void draw()
  {
      GenericDrawer.setBackgroundColor(this.renderer, 93, 148, 252, 255);

      foreach (Platform plat; this.platforms) {
          plat.draw();
      }

      foreach (Points points; this.lPoints) {
          points.draw();
      }

      foreach (Coin coin; this.lCoin) {
          coin.draw();
      }

      // for(int i = 0; i < iMinionListSize; i++) {
      //   for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
      //     lMinion[i][j]->Draw(rR, vMinion[lMinion[i][j]->getBloockID()]->getSprite()->getTexture());
      //     //CCFG::getText()->DrawWS(rR, std::to_string(i), lMinion[i][j]->getXPos() + (int)fXPos, lMinion[i][j]->getYPos(), 0, 0, 0, 8);
      //   }
      // }
      // foreach (Points lBlockDebris; this.lBlockDebris) {
      //     lBlockDebris.draw();
      // }

      // foreach (Points points; this.lBubble) {
      //     points.draw();
      // }

    	// for(uint i = 0; i < lBlockDebris.length; i++) {
    	// 	lBlockDebris[i].draw();
    	// }

    	// for(unsigned int i = 0; i < vLevelText.size(); i++) {
    	// 	CCFG::getText().Draw(vLevelText[i]->getText(), vLevelText[i]->getXPos() + (int)fXPos, vLevelText[i]->getYPos());
    	// }

    	// for(uint i = 0; i < lBubble.length; i++) {
    	// 	lBubble[i].draw(this.gameData.getBlock(lBubble[i]->getBlockID()).getSprite().getTexture());
    	// }

      this.drawMap();
    	// if(inEvent) {
    	// 	//oEvent->Draw(rR);
    	// }

      foreach (Minion[] minion; this.lMinion) {
        foreach (int i, Minion min; minion) {
            min.draw(this.gameData.getMinion(min.getBloockID()).getSprite().getTexture());
        }
      }

    	this.gameData.getPlayer().draw();
      this.levelLayout();
  }

  void load()
  {
    Core.instance().getMusic().changeMusic(true, true);
    Core.instance().getMusic().PlayMusic();
    clearMap();

    this.iMapWidth = 260;
    this.iMapHeight = 25;
    this.iLevelType = 0;
    this.iMapTime = 400;

    // ---------- LOAD LISTS ----------
    createMap();

    // ----- MINIONS
    clearMinions();

  	addGoombas(704, 368, true);

  	addGoombas(1280, 368, true);

  	addGoombas(1632, 368, true);
  	addGoombas(1680, 368, true);

  	addGoombas(2560, 112, true);
  	addGoombas(2624, 112, true);

  	addGoombas(3104, 368, true);
  	addGoombas(3152, 368, true);

  	//addKoppa(107*32, 368, 1, true);

  	addGoombas(3648, 368, true);
  	addGoombas(3696, 368, true);

  	addGoombas(3968, 368, true);
  	addGoombas(4016, 368, true);

  	addGoombas(4096, 368, true);
  	addGoombas(4144, 368, true);

  	addGoombas(5568, 368, true);
  	addGoombas(5612, 368, true);

    // ----- PIPEEVENT
    //lPipe ~= (new Pipe(0, 57, 5, 58, 5, 242 * 32, 32, currentLevelID, 1, false, 350, 1, false));
    //lPipe  ~= (new Pipe(1, 253, 3, 253, 2, 163 * 32, GameConfig.window.HEIGHT - 16 - 3 * 32, currentLevelID, 0, true, 350, 1, false));


    // ----- Bush -----

    structBush(0, 2, 2);
    structBush(16, 2, 1);
    structBush(48, 2, 2);
    structBush(64, 2, 1);
    structBush(96, 2, 2);
    structBush(112, 2, 1);
    structBush(144, 2, 2);
    structBush(160, 2, 1);
    structBush(192, 2, 2);
    structBush(208, 2, 1);

    // ----- Bush -----

    // ----- Clouds -----

    structCloud(8, 10, 1);
    structCloud(19, 11, 1);
    structCloud(27, 10, 3);
    structCloud(36, 11, 2);
    structCloud(56, 10, 1);
    structCloud(67, 11, 1);
    structCloud(75, 10, 3);
    structCloud(84, 11, 2);
    structCloud(104, 10, 1);
    structCloud(115, 11, 1);
    structCloud(123, 10, 3);
    structCloud(132, 11, 2);
    structCloud(152, 10, 1);
    structCloud(163, 11, 1);
    structCloud(171, 10, 3);
    structCloud(180, 11, 2);
    structCloud(200, 10, 1);
    structCloud(211, 11, 1);
    structCloud(219, 10, 3);

    // ----- Clouds -----

    // ----- Grass -----

    structGrass(11, 2, 3);
    structGrass(23, 2, 1);
    structGrass(41, 2, 2);
    structGrass(59, 2, 3);
    structGrass(71, 2, 1);
    structGrass(89, 2, 2);
    structGrass(107, 2, 3);
    structGrass(119, 2, 1);
    structGrass(137, 2, 2);
    structGrass(157, 2, 1);
    structGrass(167, 2, 1);
    structGrass(205, 2, 1);
    structGrass(215, 2, 1);

    // ----- Grass -----

    // ----- GND -----

    structGND(0, 0, 69, 2);
    structGND(71, 0, 15, 2);
    structGND(89, 0, 64, 2);
    structGND(155, 0, 85, 2);

    // ----- GND -----

    // ----- GND 2 -----

    structGND2(134, 2, 4, true);
    structGND2(140, 2, 4, false);
    structGND2(148, 2, 4, true);
    structGND2(152, 2, 1, 4);
    structGND2(155, 2, 4, false);
    structGND2(181, 2, 8, true);
    structGND2(189, 2, 1, 8);

    structGND2(198, 2, 1, 1);

    // ----- GND 2 -----

    // ----- BRICK -----

    struckBlockQ(16, 5, 1);
    structBrick(20, 5, 1, 1);
    struckBlockQ(21, 5, 1);
    lMap[21][5].setSpawnMushroom(true);
    structBrick(22, 5, 1, 1);
    struckBlockQ(22, 9, 1);
    struckBlockQ(23, 5, 1);
    structBrick(24, 5, 1, 1);

    struckBlockQ2(64, 6, 1);
    lMap[64][6].setSpawnMushroom(true);
    lMap[64][6].setPowerUP(false);

    structBrick(77, 5, 1, 1);
    struckBlockQ(78, 5, 1);
    lMap[78][5].setSpawnMushroom(true);
    structBrick(79, 5, 1, 1);

    structBrick(80, 9, 8, 1);
    structBrick(91, 9, 3, 1);
    struckBlockQ(94, 9, 1);
    structBrick(94, 5, 1, 1);
    lMap[94][5].setNumOfUse(4);

    structBrick(100, 5, 2, 1);

    struckBlockQ(106, 5, 1);
    struckBlockQ(109, 5, 1);
    struckBlockQ(109, 9, 1);
    lMap[109][9].setSpawnMushroom(true);
    struckBlockQ(112, 5, 1);

    structBrick(118, 5, 1, 1);

    structBrick(121, 9, 3, 1);

    structBrick(128, 9, 1, 1);
    struckBlockQ(129, 9, 2);
    structBrick(131, 9, 1, 1);

    structBrick(129, 5, 2, 1);

    structBrick(168, 5, 2, 1);
    struckBlockQ(170, 5, 1);
    structBrick(171, 5, 1, 1);

    lMap[101][5].setSpawnStar(true);

    // ----- BRICK -----

    // ----- PIPES -----

    structPipe(28, 2, 1);
    structPipe(38, 2, 2);
    structPipe(46, 2, 3);
    structPipe(57, 2, 3);
    structPipe(163, 2, 1);
    structPipe(179, 2, 1);

    // ----- PIPES -----

    // ----- END

    structEnd(198, 3, 9);
    structCastleSmall(202, 2);

    // ----- MAP 1_1_2 -----

    this.iLevelType = 1;

    structGND(240, 0, 17, 2);

    structBrick(240, 2, 1, 11);
    structBrick(244, 2, 7, 3);
    structBrick(244, 12, 7, 1);

    structPipeVertical(255, 2, 10);
    structPipeHorizontal(253, 2, 1);

    structCoins(244, 5, 7, 1);
    structCoins(244, 7, 7, 1);
    structCoins(245, 9, 5, 1);

    // ----- END LEVEL

    this.iLevelType = 0;
  }


  override string getLevelName()
  {
      return "level1-1";
  }
}
