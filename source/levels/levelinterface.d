module levels.levelinterface;

import gamedata;
import derelict.sdl2.sdl;
import graphics.vector2;
import graphics.platform;
import levels.maplevel;

interface LevelInterface
{
    void update();
    void load();
    void draw();
    string getLevelName();
    int getStartBlock();
    int getEndBlock();
    Vector2 getBlockID(int nX, int nY);
    int getBlockIDX(int nX);
    int getBlockIDY(int nY);
    bool checkCollisionLB(int nX, int nY, int nHitBoxY, bool checkVisible);
    bool checkCollisionLT(int nX, int nY, bool checkVisible);
    bool checkCollisionLC(int nX, int nY, int nHitBoxY, bool checkVisible);
    bool checkCollisionRC(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible);
    bool checkCollisionRB(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible);
    bool checkCollisionRT(int nX, int nY, int nHitBoxX, bool checkVisible);
    int checkCollisionWithPlatform(int nX, int nY, int iHitBoxX, int iHitBoxY);
    bool checkCollision(Vector2 nV, bool checkVisible);
    void checkCollisionOnTopOfTheBlock(int nX, int nY);
    Platform  getPlatform(int iID);
    void addPoints(int X, int Y, string sText, int iW, int iH);
    float getXPos();
    void setXPos(float iXPos);
    float getYPos();
    void setYPos(float iYPos);
    int getID();
    bool getUnderWater();
    MapLevel getMapBlock(int iX, int iY);
    bool getInEvent();
    void playerDeath(bool animation, bool instantDeath);
    bool getMoveMap();
    void setMoveMap(bool bMoveMap);
    void moveMap(int nX, int nY);
    int getLevelType();
      bool blockUse(int nX, int nY, int iBlockID, int POS);
        void addPlayerFireBall(int X, int Y, bool moveDirection);
}
