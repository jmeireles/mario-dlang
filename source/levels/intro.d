module levels.intro;
import derelict.sdl2.sdl;
import std.conv;

import graphics.image;
import graphics.block;
import levels.levelinterface;
import levels.level;
import gamedata;
import drawers.text;
import drawers.generic;
import std.stdio;
import menus.mainmenu;
import inputhandler;
import gamecore;

class Intro : AbstractLevel
{

  private:
    Image tSMBLOGO;
    MainMenu mainMenu;

    int[11] blocksToLoad = [
      57, 2, 8, 29, 55, 57, 70, 71, 72, 73, 82
    ];

  public:
   this (GameData gameData, TextDrawer textDrawer, SDL_Renderer* renderer, InputHandler inputHandler)
   {
      super(gameData, textDrawer, renderer);
      this.mainMenu = new MainMenu(renderer, textDrawer, inputHandler);
      this.tSMBLOGO = new Image("super_mario_bros_new", this.renderer);
   }

   void load()
   {
      Core.instance().getMusic().changeMusic(true, true);
      Core.instance().getMusic().PlayMusic();
      GenericDrawer.setBackgroundColor(this.renderer, 93, 148, 252, 255);

      this.iMapWidth = 260;
      this.iMapHeight = 25;
      this.iLevelType = 0;
      this.iMapTime = 400;

      createMap();

      // addGoombas(704, 368, true);

      structBush(0, 2, 2);
      structBush(16, 2, 1);
      structBush(48, 2, 2);
      structBush(64, 2, 1);
      structBush(96, 2, 2);
      structBush(112, 2, 1);
      structBush(144, 2, 2);
      structBush(160, 2, 1);
      structBush(192, 2, 2);
      structBush(208, 2, 1);

      structCloud(8, 10, 1);
      structCloud(21, 12, 1);

      structCloud(19, 11, 1);

      structGrass(11, 2, 3);
      structGrass(23, 2, 1);
      structGrass(41, 2, 2);
      structGrass(59, 2, 3);
      structGrass(71, 2, 1);
      structGrass(89, 2, 2);
      structGrass(107, 2, 3);
      structGrass(119, 2, 1);
      structGrass(137, 2, 2);
      structGrass(157, 2, 1);
      structGrass(167, 2, 1);
      structGrass(205, 2, 1);
      structGrass(215, 2, 1);

      // ----- Grass -----

      // ----- GND -----

      structGND(0, 0, 69, 2);
      structGND(71, 0, 15, 2);
      structGND(89, 0, 64, 2);
      structGND(155, 0, 85, 2);

      // ----- GND -----

      // ----- GND 2 -----

      structGND2(134, 2, 4, true);
      structGND2(140, 2, 4, false);
      structGND2(148, 2, 4, true);
      structGND2(152, 2, 1, 4);
      structGND2(155, 2, 4, false);
      structGND2(181, 2, 8, true);
      structGND2(189, 2, 1, 8);
      structGND2(198, 2, 1, 1);

      struckBlockQ(16, 5, 1);
      structBrick(20, 5, 1, 1);
      struckBlockQ(21, 5, 1);
      lMap[21][5].setSpawnMushroom(true);
      structBrick(22, 5, 1, 1);
      struckBlockQ(22, 9, 1);
      struckBlockQ(23, 5, 1);
      structBrick(24, 5, 1, 1);

      struckBlockQ2(64, 6, 1);
      lMap[64][6].setSpawnMushroom(true);
      lMap[64][6].setPowerUP(false);

      structBrick(77, 5, 1, 1);
      struckBlockQ(78, 5, 1);
      lMap[78][5].setSpawnMushroom(true);
      structBrick(79, 5, 1, 1);

      structBrick(80, 9, 8, 1);
      structBrick(91, 9, 3, 1);
      struckBlockQ(94, 9, 1);
      structBrick(94, 5, 1, 1);
      lMap[94][5].setNumOfUse(4);

      structBrick(100, 5, 2, 1);

      struckBlockQ(106, 5, 1);
      struckBlockQ(109, 5, 1);
      struckBlockQ(109, 9, 1);
      lMap[109][9].setSpawnMushroom(true);
      struckBlockQ(112, 5, 1);

      structBrick(118, 5, 1, 1);

      structBrick(121, 9, 3, 1);

      structBrick(128, 9, 1, 1);
      struckBlockQ(129, 9, 2);
      structBrick(131, 9, 1, 1);

      structBrick(129, 5, 2, 1);

      structBrick(168, 5, 2, 1);
      struckBlockQ(170, 5, 1);
      structBrick(171, 5, 1, 1);

      lMap[101][5].setSpawnStar(true);

      // ----- BRICK -----

      // ----- PIPES -----

      structPipe(28, 2, 1);
      structPipe(38, 2, 2);
      structPipe(46, 2, 3);
      structPipe(57, 2, 3);
      structPipe(163, 2, 1);
      structPipe(179, 2, 1);

      // ----- PIPES -----
      structEnd(198, 3, 9);
      structCastleSmall(202, 2);
   }

   void update()
   {
      foreach (int block; this.blocksToLoad) {
          this.gameData.getBlock(block).getSprite().update();
      }
      UpdatePlayer();
   }

   void draw()
   {
      this.mainMenu.listenInput();
      this.mainMenu.draw();
      this.tSMBLOGO.draw(80, 48);
      this.textDrawer.draw( "MARIO", 54, 16);
      this.textDrawer.draw( "000000", 54, 32);
      this.textDrawer.draw( "WORLD", 462, 16);
      this.textDrawer.draw( this.getLevelName(), 480, 32);
      this.gameData.getBlock(57).draw( 268, 32);
      this.textDrawer.draw( "y", 286, 32);
      this.textDrawer.draw( (100 < 10 ? "0" : "") ~ to!string(100), 302, 32);
      this.drawMap();
      this.gameData.getPlayer().draw();
   }

   override string getLevelName()
   {
      return "Intro";
   }

   override int getID()
   {
      return 0;
   }

}
