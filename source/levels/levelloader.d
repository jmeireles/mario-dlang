module levels.levelloader;

import levels.maplevel;
import levels.leveltext;
import graphics.flag;
import graphics.platform;
import graphics.minion;
import event;
import std.conv;
import gamedata;
import std.stdio;
import derelict.sdl2.sdl;
import drawers.text;
import graphics.vector2;
import graphics.coin;
import graphics.points;
import graphics.pipe;
import gamecore;
import enemies.goomba;
import config;
import levels.levelinterface;
import graphics.blockdebris;
import graphics.mushroom;
import graphics.flower;
import graphics.star;
import graphics.vine;
import graphics.playerfireball;
import drawers.generic;
import std.algorithm;
import std.math;
import levels.data.leveldatainterface;

class LevelLoader : LevelInterface
{

  this (GameData gameData, TextDrawer textDrawer, SDL_Renderer* renderer)
  {
     this.textDrawer = textDrawer;
     this.gameData = gameData;
     this.renderer = renderer;
  }

  void setLevel(LevelDataInterface currentLevel)
  {
      this.currentLevel = currentLevel;
  }

  protected:
    LevelDataInterface currentLevel;

    int iFrameID = 0;
    int iSpawnPointID = 0;
    bool bMoveMap = true;
    bool inEvent;
    int id = 1;
    MapLevel[][] lMap;
    Minion[][] lMinion;
    LevelText[] vLevelText;
    TextDrawer textDrawer;
    GameData gameData;
    SDL_Renderer* renderer;
    Platform[] platforms;
    Coin[] lCoin;
    Points[] lPoints;
    Pipe[] lPipe;
    BlockDebris[] lBlockDebris;
    int iMinionListSize;

    bool underWater = false;
    bool bTP;
    int iLevelType = 0;
    int iMapWidth, iMapHeight;
    int iMapTime;
    int currentLevelID = 0;
    Flag oFlag;
    float fXPos = 0;
    float fYPos = 0;

    int[11] blocksToLoad = [
      57, 2, 8, 29, 55, 57, 70, 71, 72, 73, 82
    ];

    public void load()
    {
      Core.instance().getMusic().changeMusic(true, true);
      Core.instance().getMusic().PlayMusic();
      clearMap();

      this.iMapWidth = 260;
      this.iMapHeight = 25;
      this.iLevelType = 0;
      this.iMapTime = 400;

      // ---------- LOAD LISTS ----------
      createMap();

      // ----- MINIONS
      clearMinions();

    	addGoombas(704, 368, true);

    	addGoombas(1280, 368, true);

    	addGoombas(1632, 368, true);
    	addGoombas(1680, 368, true);

    	addGoombas(2560, 112, true);
    	addGoombas(2624, 112, true);

    	addGoombas(3104, 368, true);
    	addGoombas(3152, 368, true);

    	//addKoppa(107*32, 368, 1, true);

    	addGoombas(3648, 368, true);
    	addGoombas(3696, 368, true);

    	addGoombas(3968, 368, true);
    	addGoombas(4016, 368, true);

    	addGoombas(4096, 368, true);
    	addGoombas(4144, 368, true);

    	addGoombas(5568, 368, true);
    	addGoombas(5612, 368, true);

      // ----- PIPEEVENT
      //lPipe ~= (new Pipe(0, 57, 5, 58, 5, 242 * 32, 32, currentLevelID, 1, false, 350, 1, false));
      //lPipe  ~= (new Pipe(1, 253, 3, 253, 2, 163 * 32, GameConfig.window.HEIGHT - 16 - 3 * 32, currentLevelID, 0, true, 350, 1, false));


      // ----- Bush -----

      structBush(0, 2, 2);
      structBush(16, 2, 1);
      structBush(48, 2, 2);
      structBush(64, 2, 1);
      structBush(96, 2, 2);
      structBush(112, 2, 1);
      structBush(144, 2, 2);
      structBush(160, 2, 1);
      structBush(192, 2, 2);
      structBush(208, 2, 1);

      // ----- Bush -----

      // ----- Clouds -----

      structCloud(8, 10, 1);
      structCloud(19, 11, 1);
      structCloud(27, 10, 3);
      structCloud(36, 11, 2);
      structCloud(56, 10, 1);
      structCloud(67, 11, 1);
      structCloud(75, 10, 3);
      structCloud(84, 11, 2);
      structCloud(104, 10, 1);
      structCloud(115, 11, 1);
      structCloud(123, 10, 3);
      structCloud(132, 11, 2);
      structCloud(152, 10, 1);
      structCloud(163, 11, 1);
      structCloud(171, 10, 3);
      structCloud(180, 11, 2);
      structCloud(200, 10, 1);
      structCloud(211, 11, 1);
      structCloud(219, 10, 3);

      // ----- Clouds -----

      // ----- Grass -----

      structGrass(11, 2, 3);
      structGrass(23, 2, 1);
      structGrass(41, 2, 2);
      structGrass(59, 2, 3);
      structGrass(71, 2, 1);
      structGrass(89, 2, 2);
      structGrass(107, 2, 3);
      structGrass(119, 2, 1);
      structGrass(137, 2, 2);
      structGrass(157, 2, 1);
      structGrass(167, 2, 1);
      structGrass(205, 2, 1);
      structGrass(215, 2, 1);

      // ----- Grass -----

      // ----- GND -----

      structGND(0, 0, 69, 2);
      structGND(71, 0, 15, 2);
      structGND(89, 0, 64, 2);
      structGND(155, 0, 85, 2);

      // ----- GND -----

      // ----- GND 2 -----

      structGND2(134, 2, 4, true);
      structGND2(140, 2, 4, false);
      structGND2(148, 2, 4, true);
      structGND2(152, 2, 1, 4);
      structGND2(155, 2, 4, false);
      structGND2(181, 2, 8, true);
      structGND2(189, 2, 1, 8);

      structGND2(198, 2, 1, 1);

      // ----- GND 2 -----

      // ----- BRICK -----
      struckBlockQ(16, 5, 1);
      structBrick(20, 5, 1, 1);
      struckBlockQ(21, 5, 1);
      lMap[21][5].setSpawnMushroom(true);
      structBrick(22, 5, 1, 1);
      struckBlockQ(22, 9, 1);
      struckBlockQ(23, 5, 1);
      structBrick(24, 5, 1, 1);

      struckBlockQ2(64, 6, 1);
      lMap[64][6].setSpawnMushroom(true);
      lMap[64][6].setPowerUP(false);

      structBrick(77, 5, 1, 1);
      struckBlockQ(78, 5, 1);
      lMap[78][5].setSpawnMushroom(true);
      structBrick(79, 5, 1, 1);

      structBrick(80, 9, 8, 1);
      structBrick(91, 9, 3, 1);
      struckBlockQ(94, 9, 1);
      structBrick(94, 5, 1, 1);
      lMap[94][5].setNumOfUse(4);

      structBrick(100, 5, 2, 1);

      struckBlockQ(106, 5, 1);
      struckBlockQ(109, 5, 1);
      struckBlockQ(109, 9, 1);
      lMap[109][9].setSpawnMushroom(true);
      struckBlockQ(112, 5, 1);

      structBrick(118, 5, 1, 1);

      structBrick(121, 9, 3, 1);

      structBrick(128, 9, 1, 1);
      struckBlockQ(129, 9, 2);
      structBrick(131, 9, 1, 1);

      structBrick(129, 5, 2, 1);

      structBrick(168, 5, 2, 1);
      struckBlockQ(170, 5, 1);
      structBrick(171, 5, 1, 1);

      lMap[101][5].setSpawnStar(true);

      // ----- BRICK -----

      // ----- PIPES -----

      structPipe(28, 2, 1);
      structPipe(38, 2, 2);
      structPipe(46, 2, 3);
      structPipe(57, 2, 3);
      structPipe(163, 2, 1);
      structPipe(179, 2, 1);

      // ----- PIPES -----

      // ----- END

      structEnd(198, 3, 9);
      structCastleSmall(202, 2);

      // ----- MAP 1_1_2 -----

      this.iLevelType = 1;

      structGND(240, 0, 17, 2);

      structBrick(240, 2, 1, 11);
      structBrick(244, 2, 7, 3);
      structBrick(244, 12, 7, 1);

      structPipeVertical(255, 2, 10);
      structPipeHorizontal(253, 2, 1);

      structCoins(244, 5, 7, 1);
      structCoins(244, 7, 7, 1);
      structCoins(245, 9, 5, 1);

      // ----- END LEVEL

      this.iLevelType = 0;
    }

    public void draw()
    {
        GenericDrawer.setBackgroundColor(this.renderer, 93, 148, 252, 255);

        foreach (Platform plat; this.platforms) {
            plat.draw();
        }

        foreach (Points points; this.lPoints) {
            points.draw();
        }

        foreach (Coin coin; this.lCoin) {
            coin.draw();
        }

        // for(int i = 0; i < iMinionListSize; i++) {
        //   for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
        //     lMinion[i][j]->Draw(rR, vMinion[lMinion[i][j]->getBloockID()]->getSprite()->getTexture());
        //     //CCFG::getText()->DrawWS(rR, std::to_string(i), lMinion[i][j]->getXPos() + (int)fXPos, lMinion[i][j]->getYPos(), 0, 0, 0, 8);
        //   }
        // }
        // foreach (Points lBlockDebris; this.lBlockDebris) {
        //     lBlockDebris.draw();
        // }

        // foreach (Points points; this.lBubble) {
        //     points.draw();
        // }

      	// for(uint i = 0; i < lBlockDebris.length; i++) {
      	// 	lBlockDebris[i].draw();
      	// }

      	// for(unsigned int i = 0; i < vLevelText.size(); i++) {
      	// 	CCFG::getText().Draw(vLevelText[i]->getText(), vLevelText[i]->getXPos() + (int)fXPos, vLevelText[i]->getYPos());
      	// }

      	// for(uint i = 0; i < lBubble.length; i++) {
      	// 	lBubble[i].draw(this.gameData.getBlock(lBubble[i]->getBlockID()).getSprite().getTexture());
      	// }

        this.drawMap();
      	// if(inEvent) {
      	// 	//oEvent->Draw(rR);
      	// }

        foreach (Minion[] minion; this.lMinion) {
          foreach (int i, Minion min; minion) {
              min.draw(this.gameData.getMinion(min.getBloockID()).getSprite().getTexture());
          }
        }

      	this.gameData.getPlayer().draw();
        this.levelLayout();
    }

    public void update()
    {
      foreach (int block; this.blocksToLoad) {
          this.gameData.getBlock(block).getSprite().update();
      }

       updateMinionsCollisions();

       if(!Core.instance().getPlayer().getInLevelAnimation()) {
     		UpdateMinionBlokcs();
     		UpdateMinions();

     		if(!inEvent) {
     			UpdatePlayer();

     			++iFrameID;
     			if(iFrameID > 32) {
     				iFrameID = 0;
     				if(iMapTime > 0) {
     					--iMapTime;
     					if(iMapTime == 90) {
     						Core.instance().getMusic().StopMusic();
     						Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cLOWTIME);
     					} else if(iMapTime == 86) {
     						Core.instance().getMusic().changeMusic(true, true);
     					}

     					if(iMapTime <= 0) {
     						playerDeath(true, true);
     					}
     				}
     			}
     		} else {
     			//oEvent->Animation();
     		}

        foreach (Platform plat; this.platforms) {
            plat.update();
        }

     	} else {
     		Core.instance().getPlayer().powerUPAnimation();
     	}

      foreach (int i, Points points; this.lPoints) {
    		if(!points.getDelete()) {
    			points.update();
    		} else {
          this.lPoints = this.lPoints.remove(i);
    		}
    	}

      foreach (int i, Coin coin; this.lCoin) {
          if(!coin.getDelete()) {
            coin.update();
          } else {
            lPoints ~= new Points(coin.getXPos(), coin.getYPos(), "200");
            this.lCoin = this.lCoin.remove(i);
          }
      }
    }

    void UpdateMinionBlokcs()
    {
    	this.gameData.getMinion(0).getSprite().update();
    	this.gameData.getMinion(4).getSprite().update();
    	this.gameData.getMinion(6).getSprite().update();
    	this.gameData.getMinion(7).getSprite().update();
    	this.gameData.getMinion(8).getSprite().update();
    	this.gameData.getMinion(10).getSprite().update();
    	this.gameData.getMinion(12).getSprite().update();
    	this.gameData.getMinion(14).getSprite().update();
    	this.gameData.getMinion(15).getSprite().update();
    	this.gameData.getMinion(17).getSprite().update();
    	this.gameData.getMinion(18).getSprite().update();
    	this.gameData.getMinion(19).getSprite().update();
    	this.gameData.getMinion(20).getSprite().update();
    	this.gameData.getMinion(21).getSprite().update();
    	this.gameData.getMinion(22).getSprite().update();
    	this.gameData.getMinion(23).getSprite().update();
    	this.gameData.getMinion(24).getSprite().update();
    	this.gameData.getMinion(30).getSprite().update();
    	this.gameData.getMinion(31).getSprite().update();
    	this.gameData.getMinion(43).getSprite().update();
    	this.gameData.getMinion(44).getSprite().update();
    	this.gameData.getMinion(45).getSprite().update();
    	this.gameData.getMinion(46).getSprite().update();
    	this.gameData.getMinion(47).getSprite().update();
    	this.gameData.getMinion(48).getSprite().update();
    	this.gameData.getMinion(51).getSprite().update();
    	this.gameData.getMinion(52).getSprite().update();
    	this.gameData.getMinion(53).getSprite().update();
    	this.gameData.getMinion(55).getSprite().update();
    	this.gameData.getMinion(57).getSprite().update();
    	this.gameData.getMinion(62).getSprite().update();
    }

    void UpdateMinions()
    {
      foreach (Minion[] minion; this.lMinion) {
        foreach (Minion min; minion) {
          if(min.updateMinion()) {
    				min.update();
    			}
        }
      }

    foreach (int indexF, Minion[] minion; this.lMinion) {
      foreach (int index, Minion min; minion) {
        if(min.minionSpawned) {
          if(min.minionState == -1) {
    				  this.lMinion.remove(indexF);
              continue;
        	}
          // if(floor(min.fXPos / 160) != indexF) {
          //   // writeln("WTH");
      		// 		// this.lMinion[cast(int)floor(cast(int)lMinion[i][j].fXPos / 160)] ~= lMinion;
          //     //remove(lMinion[i], j);
          //     // minion.remove(index);
        	// }
        }
      }
    }

    	// for(unsigned int i = 0; i < lBubble.size(); i++) {
    	// 	lBubble[i]->Update();
      //
    	// 	if(lBubble[i]->getDestroy()) {
    	// 		delete lBubble[i];
    	// 		lBubble.erase(lBubble.begin() + i);
    	// 	}
    	// }
    }

    void levelLayout()
    {
        this.textDrawer.draw("MARIO", 54, 16);

      	if(this.gameData.getPlayer().getScore() < 100) {
      		this.textDrawer.draw("00000" ~ to!string(this.gameData.getPlayer().getScore()), 54, 32);
      	} else if(this.gameData.getPlayer().getScore() < 1000) {
      		this.textDrawer.draw("000" ~ to!string(this.gameData.getPlayer().getScore()), 54, 32);
      	} else if(this.gameData.getPlayer().getScore() < 10000) {
      		this.textDrawer.draw("00" ~ to!string(this.gameData.getPlayer().getScore()), 54, 32);
      	} else if(this.gameData.getPlayer().getScore() < 100000) {
      		this.textDrawer.draw("0" ~ to!string(this.gameData.getPlayer().getScore()), 54, 32);
      	} else {
      		this.textDrawer.draw(to!string(this.gameData.getPlayer().getScore()), 54, 32);
      	}

      	this.textDrawer.draw("WORLD", 462, 16);
      	this.textDrawer.draw("1-1", 480, 32);

      	if(iLevelType != 1) {
      		this.gameData.getBlock(2).draw(268, 32);
      	} else {
      		this.gameData.getBlock(57).draw(268, 32);
      	}

      	this.textDrawer.draw("y", 286, 32);
      	this.textDrawer.draw((this.gameData.getPlayer().getCoins() < 10 ? "0" : "") ~ to!string(this.gameData.getPlayer().getCoins()), 302, 32);

      	this.textDrawer.draw("TIME", 672, 16);
      	// if(CCFG::getMM().getViewID() == CCFG::getMM().eGame) {
      		if(iMapTime > 100) {
      			this.textDrawer.draw(to!string(iMapTime), 680, 32);
            } else if(iMapTime <= 0) {
                Core.instance().setGameOver();
      		} else if(iMapTime > 10) {
      			this.textDrawer.draw("0" ~ to!string(iMapTime), 680, 32);
      		} else {
      			this.textDrawer.draw("00" ~ to!string(iMapTime), 680, 32);
      		}
      	// }

    }

    string getLevelName()
    {
        return "override me";
    }

    void drawMap()
    {
        for(int i = this.getStartBlock(), iEnd = this.getEndBlock(); i < iEnd && i < iMapWidth; i++) {
          for(int j = iMapHeight - 1; j >= 0; j--) {
            if(lMap[i][j].getBlockID() != 0) {
              this.gameData.getBlock(lMap[i][j].getBlockID()).draw(32 * i + (to!int(fXPos)), 448 - 32 * j - 16 - lMap[i][j].updateYPos());
            }
          }
        }
    }

    int getListID(int nX)
    {
    	return to!int(nX / 160);
    }

    void clearMinions()
    {
      // Simulate a clear()
      // Minion[][] lMinions;
      // lMinion = lMinions;
    	clearPlatforms();
    }

    void clearPlatforms()
    {
       Platform[] platforms;
    	 this.platforms = platforms;
    }

    void addGoombas(int iX, int iY, bool moveDirection) {
    	 this.lMinion[getListID(iX)] ~= new Goombas(iX, iY, iLevelType == 0 || iLevelType == 4 ? 0 : iLevelType == 1 ? 8 : 10, moveDirection);
    }

    void structCastleWall(int X, int Y, int iWidth, int iHeight)
    {
      for(int i = 0; i < iWidth; i++) {
        for(int j = 0; j  < iHeight - 1; j++) {
          lMap[X + i][Y + j].setBlockID(iLevelType == 3 ? 155 : 43);
        }
      }

      for(int i = 0; i < iWidth; i++) {
        lMap[X + i][Y + iHeight - 1].setBlockID(iLevelType == 3 ? 157 : 45);
      }
    }

    void structPipe(int X, int Y, int iHeight)
    {
      for(int i = 0; i < iHeight; i++) {
        lMap[X][Y + i].setBlockID(iLevelType == 0 ? 20 : iLevelType == 2 ? 97 : iLevelType == 4 ? 112 : iLevelType == 5 ? 136 : iLevelType == 3 ? 176 : iLevelType == 7 ? 172 : 30);
        lMap[X + 1][Y + i].setBlockID(iLevelType == 0 ? 22 : iLevelType == 2 ? 99 : iLevelType == 4 ? 114 : iLevelType == 5 ? 138 : iLevelType == 3 ? 178 : iLevelType == 7 ? 174 : 32);
      }

      lMap[X][Y + iHeight].setBlockID(iLevelType == 0 ? 21 : iLevelType == 2 ? 98 : iLevelType == 4 ? 113 : iLevelType == 5 ? 137 : iLevelType == 3 ? 177 : iLevelType == 7 ? 173 : 31);
      lMap[X + 1][Y + iHeight].setBlockID(iLevelType == 0 ? 23 : iLevelType == 2 ? 100 : iLevelType == 4 ? 115 : iLevelType == 5 ? 139 : iLevelType == 3 ? 179 : iLevelType == 7 ? 175 : 33);
    }

    void structPipeVertical(int X, int Y, int iHeight)
    {
      for(int i = 0; i < iHeight + 1; i++) {
        lMap[X][Y + i].setBlockID(iLevelType == 0 ? 20 : iLevelType == 2 ? 97 : iLevelType == 4 ? 112 : 30);
        lMap[X + 1][Y + i].setBlockID(iLevelType == 0 ? 22 : iLevelType == 2 ? 99 : iLevelType == 4 ? 114 : 32);
      }
    }

    void structPipeHorizontal(int X, int Y, int iWidth)
    {
      lMap[X][Y].setBlockID(iLevelType == 0 ? 62 : iLevelType == 2 ? 105 : iLevelType == 4 ? 120 : 38);
      lMap[X][Y + 1].setBlockID(iLevelType == 0 ? 60 : iLevelType == 2 ? 103 : iLevelType == 4 ? 118 : 36);

      for(int i = 0 ; i < iWidth; i++) {
        lMap[X + 1 + i][Y].setBlockID(iLevelType == 0 ? 61 : iLevelType == 2 ? 104 : iLevelType == 4 ? 119 : 37);
        lMap[X + 1 + i][Y + 1].setBlockID(iLevelType == 0 ? 59 : iLevelType == 2 ? 102 : iLevelType == 4 ? 117 : 35);
      }

      lMap[X + 1 + iWidth][Y].setBlockID(iLevelType == 0 ? 58 : iLevelType == 2 ? 101 : iLevelType == 4 ? 116 : 34);
      lMap[X + 1 + iWidth][Y + 1].setBlockID(iLevelType == 0 ? 63 : iLevelType == 2 ? 106 : iLevelType == 4 ? 121 : 39);
    }

    void structBrick(int X, int Y, int iWidth, int iHeight)
    {
      for(int i = 0; i < iWidth; i++) {
        for(int j = 0; j < iHeight; j++) {
          lMap[X + i][Y + j].setBlockID(iLevelType == 0 || iLevelType == 4 ? 13 : iLevelType == 3 ? 81 : 28);
        }
      }
    }

    void struckBlockQ(int X, int Y, int iWidth)
    {
      for(int i = 0; i < iWidth; i++) {
        lMap[X + i][Y].setBlockID(iLevelType == 0 || iLevelType == 4 ? 8 : 55);
      }
    }

    void struckBlockQ2(int X, int Y, int iWidth)
    {
      for(int i = 0; i < iWidth; i++) {
        lMap[X + i][Y].setBlockID(24);
      }
    }

    // ----- true = LEFT, false = RIGHT -----
    void structGND2(int X, int Y, int iSize, bool bDir)
    {
      if(bDir) {
        for(int i = 0, k = 1; i < iSize; i++) {
          for(int j = 0; j < k; j++) {
            lMap[X + i][Y + j].setBlockID(iLevelType == 0 || iLevelType == 4 ? 25 : iLevelType == 3 ? 167 : 27);
          }
          ++k;
        }
      } else {
        for(int i = 0, k = 1; i < iSize; i++) {
          for(int j = 0; j < k; j++) {
            lMap[X + iSize - 1 - i][Y + j].setBlockID(iLevelType == 0 || iLevelType == 4 ? 25 : iLevelType == 3 ? 167 : 27);
          }
          ++k;
        }
      }
    }

    void structGND2(int X, int Y, int iWidth, int iHeight)
    {
      for(int i = 0; i < iWidth; i++) {
        for(int j = 0; j < iHeight; j++) {
          lMap[X + i][Y + j].setBlockID(iLevelType == 0 || iLevelType == 4 ? 25 : iLevelType == 3 ? 167 : 27);
        }
      }
    }


    void structGND(int X, int Y, int iWidth, int iHeight)
    {
      for(int i = 0; i < iWidth; i++) {
        for(int j = 0; j < iHeight; j++) {
          lMap[X + i][Y + j].setBlockID(iLevelType == 0 || iLevelType == 4 ? 1 : iLevelType == 1 ? 26 : iLevelType == 2 ? 92 : iLevelType == 6 ? 166 : iLevelType == 7 ? 181 : 75);
        }
      }
    }


    void structGrass(int X, int Y, int iSize)
    {
      lMap[X][Y].setBlockID(10);
      for(int i = 0; i < iSize; i++) {
        lMap[X + 1 + i][Y].setBlockID(11);
      }
      lMap[X + iSize + 1][Y].setBlockID(12);
    }

    void structCloud(int X, int Y, int iSize)
    {
      // ----- LEFT
      lMap[X][Y].setBlockID(iLevelType == 3 ? 148 : 14);
      lMap[X][Y + 1].setBlockID(15);

      for(int i = 0; i < iSize; i++) {
        lMap[X + 1 + i][Y].setBlockID(iLevelType == 3 ? 149 : 16);
        lMap[X + 1 + i][Y + 1].setBlockID(iLevelType == 3 ? 150 : 17);
      }

      lMap[X + iSize + 1][Y].setBlockID(18);
      lMap[X + iSize + 1][Y + 1].setBlockID(19);
    }

    void createMap()
    {
      for(int i = 0; i < iMapWidth; i++) {
        MapLevel[] temp;
        Minion[] temp2;
        for(int ix = 0; ix < iMapHeight; ix++) {
          temp ~= new MapLevel(0);
        }
        lMinion ~= temp2;
        lMap ~= temp;
      }

      this.iMinionListSize = cast(int)lMinion.length;
      this.underWater = false;
      this.bTP = false;
    }

    void clearMap()
    {
      // Simulate a clear()
      MapLevel[][] lMaps;
      lMap = lMaps;

      this.iMapWidth = this.iMapHeight = 0;
      Core.instance().getGameData().getEvent().eventTypeID = Core.instance().getGameData().getEvent().eventType.eNormal;
      this.clearLevelText();
    }

    void clearLevelText()
    {
       LevelText[] vLevelText;
       this.vLevelText = vLevelText;
    }

    void structEnd(int X, int Y, int iHeight)
    {
      for(int i = 0; i < iHeight; i++) {
        lMap[X][Y + i].setBlockID(iLevelType == 4 ? 123 : 40);
      }

      oFlag = new Flag(X*32 - 16, Y + iHeight + 72);

      lMap[X][Y + iHeight].setBlockID(iLevelType == 4 ? 124 : 41);

      for(int i = Y + iHeight + 1; i < Y + iHeight + 4; i++) {
        lMap[X][i].setBlockID(182);
      }
    }

    void structCastleSmall(int X, int Y)
    {
      for(int i = 0; i < 2; i++){
        lMap[X][Y + i].setBlockID(iLevelType == 3 ? 155 : 43);
        lMap[X + 1][Y + i].setBlockID(iLevelType == 3 ? 155 : 43);
        lMap[X + 3][Y + i].setBlockID(iLevelType == 3 ? 155 : 43);
        lMap[X + 4][Y + i].setBlockID(iLevelType == 3 ? 155 : 43);

        lMap[X + 2][Y + i].setBlockID(iLevelType == 3 ? 159 : 47);
      }

      lMap[X + 2][Y + 1].setBlockID(iLevelType == 3 ? 158 : 46);

      for(int i = 0; i < 5; i++) {
        lMap[X + i][Y + 2].setBlockID(i == 0 || i == 4 ? iLevelType == 3 ? 157 : 45 : iLevelType == 3 ? 156 : 44);
      }

      lMap[X + 1][Y + 3].setBlockID(iLevelType == 3 ? 160 : 48);
      lMap[X + 2][Y + 3].setBlockID(iLevelType == 3 ? 155 : 43);
      lMap[X + 3][Y + 3].setBlockID(iLevelType == 3 ? 161 : 49);

      for(int i = 0; i < 3; i++) {
        lMap[X + i + 1][Y + 4].setBlockID(iLevelType == 3 ? 157 : 45);
      }
    }

    void structBush(int X, int Y, int iSize)
    {
      // ----- LEFT & RIGHT
      for(int i = 0; i < iSize; i++) {
        lMap[X + i][Y + i].setBlockID(5);
        lMap[X + iSize + 1 + i][Y + iSize - 1 - i].setBlockID(6);
      }

      // ----- CENTER LEFT & RIGHT
      for(int i = 0, k = 1; i < iSize - 1; i++) {
        for(int j = 0; j < k; j++) {
          lMap[X + 1 + i][Y + j].setBlockID((i%2 == 0 ? 3 : 4));
          lMap[X + iSize * 2 - 1 - i][Y + j].setBlockID((i%2 == 0 ? 3 : 4));
        }
        ++k;
      }

      // ----- CENTER
      for(int i = 0; i < iSize; i++) {
        lMap[X + iSize][Y + i].setBlockID((i%2 == 0 && iSize != 1 ? 4 : 3));
      }

      // ----- TOP
      lMap[X + iSize][Y + iSize].setBlockID(7);
    }

    void structCoins(int X, int Y, int iWidth, int iHeight)
    {
    	for(int i = 0; i < iWidth; i++) {
    		for(int j = 0; j < iHeight; j++) {
    			lMap[X + i][Y + j].setBlockID(iLevelType == 0 || iLevelType == 4 ? 71 : iLevelType == 1 ? 29 : iLevelType == 2 ? 73 : 29);
    		}
    	}
    }

    void spawnVine(int nX, int nY, int iBlockID)
    {
     if(iLevelType == 0 || iLevelType == 4) {
       addVine(nX, nY, 0, 34);
     } else {
       addVine(nX, nY, 0, 36);
     }
   }

   void addVine(int X, int Y, int minionState, int iBlockID)
   {
     	lMinion[getListID(X)] ~= (new Vine(X, Y, minionState, iBlockID));
     	if(minionState == 0) {
     		Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cVINE);
     	}
   }

public:
    int getStartBlock()
    {
      return to!int(-fXPos - (-(to!int(fXPos))) % 32) / 32;
    }

    int getEndBlock()
    {
      return to!int(-fXPos - (-(to!int(fXPos))) % 32 + 800) / 32 + 2;
    }

    Vector2 getBlockID(int nX, int nY)
    {
    	return new Vector2(cast(int)(nX < 0 ? 0 : nX) / 32, cast(int)(nY > GameConfig.window.HEIGHT - 16 ? 0 : (GameConfig.window.HEIGHT - 16 - nY + 32) / 32));
    }

    int getBlockIDX(int nX)
    {
    	return cast(int)(nX < 0 ? 0 : nX) / 32;
    }

    int getBlockIDY(int nY)
    {
    	return cast(int)(nY > GameConfig.window.HEIGHT - 16 ? 0 : (GameConfig.window.HEIGHT - 16 - nY + 32) / 32);
    }

    bool checkCollisionLB(int nX, int nY, int nHitBoxY, bool checkVisible) {
    	return checkCollision(getBlockID(nX, nY + nHitBoxY), checkVisible);
    }

    bool checkCollisionLT(int nX, int nY, bool checkVisible) {
    	return checkCollision(getBlockID(nX, nY), checkVisible);
    }

    bool checkCollisionLC(int nX, int nY, int nHitBoxY, bool checkVisible) {
    	return checkCollision(getBlockID(nX, nY + nHitBoxY), checkVisible);
    }

    bool checkCollisionRC(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible) {
    	return checkCollision(getBlockID(nX + nHitBoxX, nY + nHitBoxY), checkVisible);
    }

    bool checkCollisionRB(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible) {
    	return checkCollision(getBlockID(nX + nHitBoxX, nY + nHitBoxY), checkVisible);
    }

    bool checkCollisionRT(int nX, int nY, int nHitBoxX, bool checkVisible) {
    	return checkCollision(getBlockID(nX + nHitBoxX, nY), checkVisible);
    }

    int checkCollisionWithPlatform(int nX, int nY, int iHitBoxX, int iHitBoxY) {
    	for(uint i = 0; i < platforms.length; i++) {
    		if(-fXPos + nX + iHitBoxX >= platforms[i].getXPos() && - fXPos + nX <= platforms[i].getXPos() + platforms[i].getSize() * 16) {
    			if(nY + iHitBoxY >= platforms[i].getYPos() && nY + iHitBoxY <= platforms[i].getYPos() + 16) {
    				return i;
    			}
    		}
    	}

    	return -1;
    }

    bool checkCollision(Vector2 nV, bool checkVisible) {
    	bool output = this.gameData.getBlock(lMap[nV.getX()][nV.getY()].getBlockID()).getCollision() && (checkVisible ? this.gameData.getBlock(lMap[nV.getX()][nV.getY()].getBlockID()).getVisible() : true);
    	delete nV;
    	return output;
    }

    void checkCollisionOnTopOfTheBlock(int nX, int nY) {
      bool returns = false;

    	switch(lMap[nX][nY + 1].getBlockID()) {
    		case 29: case 71: case 72: case 73:// COIN
      			lMap[nX][nY + 1].setBlockID(0);
      			lCoin ~= new Coin(nX * 32 + 7, GameConfig.window.HEIGHT - nY * 32 - 48);
      			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cCOIN);
      			Core.instance().getPlayer().setCoins(Core.instance().getPlayer().getCoins() + 1);
      			returns = true;
    		break;
        default: break;
    	}

      if (returns) {
        return;
      }

    	for(int i = (nX - nX%5)/5, iEnd = i + 3; i < iEnd && i < this.iMinionListSize; i++) {
    		for(uint j = 0; j < lMinion[i].length; j++) {
    			if(!lMinion[i][j].collisionOnlyWithPlayer && lMinion[i][j].getMinionState() >= 0 && ((lMinion[i][j].getXPos() >= nX*32 && lMinion[i][j].getXPos() <= nX*32 + 32) || (lMinion[i][j].getXPos() + lMinion[i][j].iHitBoxX >= nX*32 && lMinion[i][j].getXPos() + lMinion[i][j].iHitBoxX <= nX*32 + 32))) {
    				if(lMinion[i][j].getYPos() + lMinion[i][j].iHitBoxY >= GameConfig.window.HEIGHT - 24 - nY*32 && lMinion[i][j].getYPos() + lMinion[i][j].iHitBoxY <= GameConfig.window.HEIGHT - nY*32 + 16) {
    					lMinion[i][j].moveDirection = !lMinion[i][j].moveDirection;
    					lMinion[i][j].setMinionState(-2);
    				}
    			}
    		}
    	}
    }

    void playerDeath(bool animation, bool instantDeath)
    {
         //Core.instance().setGameOver();
         if((Core.instance().getPlayer().getPowerLVL() == 0 && !Core.instance().getPlayer().getUnkillAble()) || instantDeath) {
       		inEvent = true;


       	// 	oEvent.resetData();
       		Core.instance().getPlayer().resetJump();
       		Core.instance().getPlayer().stopMove();
          //
       	// 	oEvent.iDelay = 150;
       	// 	oEvent.newCurrentLevel = currentLevelID;
          //
       	// 	oEvent.newMoveMap = bMoveMap;
          //
       	// 	oEvent.eventTypeID = oEvent.eNormal;

       		Core.instance().getPlayer().resetPowerLVL();

       		if(animation) {
       		// 	oEvent.iSpeed = 4;
       		// 	oEvent.newLevelType = iLevelType;

       			Core.instance().getPlayer().setYPos(Core.instance().getPlayer().getYPos() + 4.0f);
            //
       		// 	oEvent.vOLDDir.push_back(oEvent.eDEATHNOTHING);
       		// 	oEvent.vOLDLength.push_back(30);
            //
       		// 	oEvent.vOLDDir.push_back(oEvent.eDEATHTOP);
       		// 	oEvent.vOLDLength.push_back(64);
            //
       		// 	oEvent.vOLDDir.push_back(oEvent.eDEATHBOT);
       		// 	oEvent.vOLDLength.push_back(GameConfig.window.HEIGHT - Core.instance().getPlayer().getYPos() + 128);
       		} else {
       		// 	oEvent.iSpeed = 4;
       		// 	oEvent.newLevelType = iLevelType;
            //
       		// 	oEvent.vOLDDir.push_back(oEvent.eDEATHTOP);
       		// 	oEvent.vOLDLength.push_back(1);
       		}

       	// 	oEvent.vOLDDir.push_back(oEvent.eNOTHING);
       	// 	oEvent.vOLDLength.push_back(64);

       		if(Core.instance().getPlayer().getNumOfLives() > 1) {
       		// 	oEvent.vOLDDir.push_back(oEvent.eLOADINGMENU);
       		// 	oEvent.vOLDLength.push_back(90);

       			Core.instance().getPlayer().setNumOfLives(Core.instance().getPlayer().getNumOfLives() - 1);

       			Core.instance().getMusic().StopMusic();
       			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cDEATH);
       		} else {
       			//oEvent.vOLDDir.push_back(oEvent.eGAMEOVER);
       			//oEvent.vOLDLength.push_back(90);

       			Core.instance().getPlayer().setNumOfLives(Core.instance().getPlayer().getNumOfLives() - 1);
       			Core.instance().getMusic().StopMusic();
       			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cDEATH);
       		}
       	} else if(!Core.instance().getPlayer().getUnkillAble()) {
       		Core.instance().getPlayer().setPowerLVL(Core.instance().getPlayer().getPowerLVL() - 1);
       	}

    }

    void moveMap(int nX, int nY)
    {

    	if (fXPos + nX > 0) {
    		Core.instance().getPlayer().updateXPos(cast(int)(nX - fXPos));
    		fXPos = 0;
    	}
    	else {
    		this.fXPos += nX;
    	}

    }

    bool getMoveMap()
    {
    	return bMoveMap;
    }

    void setMoveMap(bool bMoveMap)
    {
    	this.bMoveMap = bMoveMap;
    }

    Platform  getPlatform(int iID)
    {
    	return this.platforms[iID];
    }

    void addPoints(int X, int Y, string sText, int iW, int iH)
    {
    	//lPoints ~= new Points(X, Y, sText, iW, iH));
    }

    float getXPos()
    {
      return fXPos;
    }

    void setXPos(float iXPos)
    {
      this.fXPos = iXPos;
    }

    float getYPos()
    {
      return fYPos;
    }

    void setYPos(float iYPos)
    {
      this.fYPos = iYPos;
    }

    int getID()
    {
        return this.id;
    }

    bool getUnderWater()
    {
        return false;
    }

    bool getInEvent()
    {
      return inEvent;
    }

  	MapLevel getMapBlock(int iX, int iY)
    {
      return lMap[iX][iY];
    }

    bool blockUse(int nX, int nY, int iBlockID, int POS)
    {
    	if(POS == 0) {
    		switch(iBlockID) {
    			case 8: case 55: // ----- BlockQ
    				if(lMap[nX][nY].getSpawnMushroom()) {
    					if(lMap[nX][nY].getPowerUP()) {
    						if(Core.instance().getPlayer().getPowerLVL() == 0) {
    							lMinion[getListID(32 * nX)] ~= (new Mushroom(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, true, nX, nY));
    						} else {
    							lMinion[getListID(32 * nX)] ~= (new Flower(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, nX, nY));
    						}
    					} else {
    						lMinion[getListID(32 * nX)] ~= (new Mushroom(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, false, nX, nY));
    					}
    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cMUSHROOMAPPER);
    				} else {

    					lCoin ~= (new Coin(nX * 32 + 7, GameConfig.window.HEIGHT - nY * 32 - 48));
    					Core.instance().getPlayer().setScore(Core.instance().getPlayer().getScore() + 200);
    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cCOIN);
    					Core.instance().getPlayer().setCoins(Core.instance().getPlayer().getCoins() + 1);

    				}

    				if(lMap[nX][nY].getNumOfUse() > 1) {
    					lMap[nX][nY].setNumOfUse(lMap[nX][nY].getNumOfUse() - 1);
    				} else {
    					lMap[nX][nY].setNumOfUse(0);
    					lMap[nX][nY].setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 56);
    				}

    				lMap[nX][nY].startBlockAnimation();
    				checkCollisionOnTopOfTheBlock(nX, nY);
    				break;
    			case 13: case 28: case 81: // ----- Brick
    				if(lMap[nX][nY].getSpawnStar()) {
    					lMap[nX][nY].setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
    					lMinion[getListID(32 * nX)] ~= (new Star(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, nX, nY));
    					lMap[nX][nY].startBlockAnimation();
    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cMUSHROOMAPPER);
    				} else if(lMap[nX][nY].getSpawnMushroom()) {
    					lMap[nX][nY].setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
    					if(lMap[nX][nY].getPowerUP()) {
    						if(Core.instance().getPlayer().getPowerLVL() == 0) {
    							lMinion[getListID(32 * nX)] ~= (new Mushroom(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, true, nX, nY));
    						} else {
    							lMinion[getListID(32 * nX)] ~= (new Flower(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, nX, nY));
    						}
    					} else {
    						lMinion[getListID(32 * nX)] ~= (new Mushroom(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, false, nX, nY));
    					}
    					lMap[nX][nY].startBlockAnimation();
    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cMUSHROOMAPPER);
    				} else if(lMap[nX][nY].getNumOfUse() > 0) {
    					lCoin ~= (new Coin(nX * 32 + 7, GameConfig.window.HEIGHT - nY * 32 - 48));
    					Core.instance().getPlayer().setScore(Core.instance().getPlayer().getScore() + 200);
    					Core.instance().getPlayer().setCoins(Core.instance().getPlayer().getCoins() + 1);

    					lMap[nX][nY].setNumOfUse(lMap[nX][nY].getNumOfUse() - 1);
    					if(lMap[nX][nY].getNumOfUse() == 0) {
    						lMap[nX][nY].setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
    					}

    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cCOIN);

    					lMap[nX][nY].startBlockAnimation();
    				} else {
    					if(Core.instance().getPlayer().getPowerLVL() > 0) {
    						lMap[nX][nY].setBlockID(0);
    						lBlockDebris ~= (new BlockDebris(nX * 32, GameConfig.window.HEIGHT - 48 - nY * 32));
    						Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cBLOCKBREAK);
    					} else {
    						lMap[nX][nY].startBlockAnimation();
    						Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cBLOCKHIT);
    					}
    				}

    				checkCollisionOnTopOfTheBlock(nX, nY);
    				break;
    			case 24: // ----- BlockQ2
    				if(lMap[nX][nY].getSpawnMushroom()) {
    					if(lMap[nX][nY].getPowerUP()) {
    						if(Core.instance().getPlayer().getPowerLVL() == 0) {
    							lMinion[getListID(32 * nX)] ~= (new Mushroom(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, true, nX, nY));
    						} else {
    							lMinion[getListID(32 * nX)] ~= (new Flower(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, nX, nY));
    						}
    					} else {
    						lMinion[getListID(32 * nX)] ~= (new Mushroom(32 * nX, GameConfig.window.HEIGHT - 16 - 32 * nY, false, nX, nY));
    					}
    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cMUSHROOMAPPER);
    				} else {
    					lCoin ~= (new Coin(nX * 32 + 7, GameConfig.window.HEIGHT - nY * 32 - 48));
    					Core.instance().getPlayer().setCoins(Core.instance().getPlayer().getCoins() + 1);
    					Core.instance().getPlayer().setScore(Core.instance().getPlayer().getScore() + 200);
    					Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cCOIN);

    					lMap[nX][nY].startBlockAnimation();
    				}

    				lMap[nX][nY].setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
    				checkCollisionOnTopOfTheBlock(nX, nY);
    				break;
    			case 128: case 129:
    				spawnVine(nX, nY, iBlockID);
    				lMap[nX][nY].setBlockID(iBlockID == 128 ? 9 : 56);
    				lMap[nX][nY].startBlockAnimation();
    				break;
    			default:
    				break;
    		}
    	} else if(POS == 1) {
    		// switch(iBlockID) {
    		// 	case 21: case 23: case 31: case 33: case 98: case 100: case 113: case 115: case 137: case 139: case 177: case 179: // Pipe
    		// 		pipeUse();
    		// 		break;
    		// 	case 40: case 41: case 123: case 124: case 182: // End
    		// 		EndUse();
    		// 		break;
    		// 	case 82:
    		// 		EndBoss();
    		// 		break;
    		// 	default:
    		// 		break;
    		// }
    	}
      bool s = true;
    	switch(iBlockID) {
    		case 29:
        case 71:
        case 72:
        case 73:// COIN
    			lMap[nX][nY].setBlockID(iLevelType == 2 ? 94 : 0);
    			Core.instance().getPlayer().addCoin();
    			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cCOIN);
    			s = false;
    			break;
    		// case 36: case 38: case 60: case 62: case 103: case 105: case 118: case 120: // Pipe
    		// 	pipeUse();
    		// 	break;
    		// case 127: // BONUS END
    		// 	EndBonus();
    		// 	break;
    		// case 169:
    		// 	TPUse();
    		// 	break;
    		// case 170:
    		// 	TPUse2();
    		// 	break;
    		// case 171:
    		// 	TPUse3();
    		// 	break;
    		default:
    			break;
    	}

    	return s;
    }

    void updateMinionsCollisions()
    {
    	// // ----- COLLISIONS


    	//for(int i = 0; i < iMinionListSize; i++) {
    	// 	for(unsigned int j = 0; j < lMinion[i].size(); j++) {
    	// 		if(!minion[j].collisionOnlyWithPlayer /*&& minion[j].minionSpawned*/ && minion[j].deadTime < 0) {
    	// 			// ----- WITH MINIONS IN SAME LIST
    	// 			for(unsigned int k = j + 1; k < lMinion[i].size(); k++) {
    	// 				if(!lMinion[i][k]->collisionOnlyWithPlayer /*&& lMinion[i][k]->minionSpawned*/ && lMinion[i][k]->deadTime < 0) {
    	// 					if(minion[j].getXPos() < lMinion[i][k]->getXPos()) {
    	// 						if(minion[j].getXPos() + minion[j].iHitBoxX >= lMinion[i][k]->getXPos() && minion[j].getXPos() + minion[j].iHitBoxX <= lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX && ((minion[j].getYPos() <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && minion[j].getYPos() + minion[j].iHitBoxY >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) || (lMinion[i][k]->getYPos() <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY >= minion[j].getYPos() + minion[j].iHitBoxY))) {
    	// 							if(minion[j].killOtherUnits && minion[j].moveSpeed > 0 && lMinion[i][k]->minionSpawned) {
    	// 								lMinion[i][k]->setMinionState(-2);
    	// 								minion[j].collisionWithAnotherUnit();
    	// 							}
      //
    	// 							if(lMinion[i][k]->killOtherUnits && lMinion[i][k]->moveSpeed > 0 && minion[j].minionSpawned) {
    	// 								minion[j].setMinionState(-2);
    	// 								lMinion[i][k]->collisionWithAnotherUnit();
    	// 							}
      //
    	// 							if(minion[j].getYPos() - 4 <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && minion[j].getYPos() + 4 >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) {
    	// 								lMinion[i][k]->onAnotherMinion = true;
    	// 							} else if(lMinion[i][k]->getYPos() - 4 <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i][k]->getYPos() + 4 >= minion[j].getYPos() + minion[j].iHitBoxY) {
    	// 								minion[j].onAnotherMinion = true;
    	// 							} else {
    	// 								minion[j].collisionEffect();
    	// 								lMinion[i][k]->collisionEffect();
    	// 							}
    	// 						}
    	// 					} else {
    	// 						if(lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX >= minion[j].getXPos() && lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX <= minion[j].getXPos() + minion[j].iHitBoxX && ((minion[j].getYPos() <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && minion[j].getYPos() + minion[j].iHitBoxY >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) || (lMinion[i][k]->getYPos() <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY >= minion[j].getYPos() + minion[j].iHitBoxY))) {
    	// 							if(minion[j].killOtherUnits && minion[j].moveSpeed > 0 && lMinion[i][k]->minionSpawned) {
    	// 								lMinion[i][k]->setMinionState(-2);
    	// 								minion[j].collisionWithAnotherUnit();
    	// 							}
      //
    	// 							if(lMinion[i][k]->killOtherUnits && lMinion[i][k]->moveSpeed > 0 && minion[j].minionSpawned) {
    	// 								minion[j].setMinionState(-2);
    	// 								lMinion[i][k]->collisionWithAnotherUnit();
    	// 							}
      //
    	// 							if(minion[j].getYPos() - 4 <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && minion[j].getYPos() + 4 >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) {
    	// 								lMinion[i][k]->onAnotherMinion = true;
    	// 							} else if(lMinion[i][k]->getYPos() - 4 <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i][k]->getYPos() + 4 >= minion[j].getYPos() + minion[j].iHitBoxY) {
    	// 								minion[j].onAnotherMinion = true;
    	// 							} else {
    	// 								minion[j].collisionEffect();
    	// 								lMinion[i][k]->collisionEffect();
    	// 							}
    	// 						}
    	// 					}
    	// 				}
    	// 			}
      //
    	// 			// ----- WITH MINIONS IN OTHER LIST
    	// 			if(i + 1 < iMinionListSize) {
    	// 				for(unsigned int k = 0; k < lMinion[i + 1].size(); k++) {
    	// 					if(!lMinion[i + 1][k]->collisionOnlyWithPlayer /*&& lMinion[i + 1][k]->minionSpawned*/ && lMinion[i + 1][k]->deadTime < 0) {
    	// 						if(minion[j].getXPos() < lMinion[i + 1][k]->getXPos()) {
    	// 							if(minion[j].getXPos() + minion[j].iHitBoxX >= lMinion[i + 1][k]->getXPos() && minion[j].getXPos() + minion[j].iHitBoxX <= lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX && ((minion[j].getYPos() <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && minion[j].getYPos() + minion[j].iHitBoxY >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) || (lMinion[i + 1][k]->getYPos() <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY >= minion[j].getYPos() + minion[j].iHitBoxY))) {
    	// 								if(minion[j].killOtherUnits && minion[j].moveSpeed > 0 && lMinion[i + 1][k]->minionSpawned) {
    	// 									lMinion[i + 1][k]->setMinionState(-2);
    	// 									minion[j].collisionWithAnotherUnit();
    	// 								}
      //
    	// 								if(lMinion[i + 1][k]->killOtherUnits && lMinion[i + 1][k]->moveSpeed > 0 && minion[j].minionSpawned) {
    	// 									minion[j].setMinionState(-2);
    	// 									lMinion[i + 1][k]->collisionWithAnotherUnit();
    	// 								}
      //
    	// 								if(minion[j].getYPos() - 4 <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && minion[j].getYPos() + 4 >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
    	// 									lMinion[i + 1][k]->onAnotherMinion = true;
    	// 								} else if(lMinion[i + 1][k]->getYPos() - 4 <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i + 1][k]->getYPos() + 4 >= minion[j].getYPos() + minion[j].iHitBoxY) {
    	// 									minion[j].onAnotherMinion = true;
    	// 								} else {
    	// 									minion[j].collisionEffect();
    	// 									lMinion[i + 1][k]->collisionEffect();
    	// 								}
    	// 							}
    	// 						} else {
    	// 							if(lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX >= minion[j].getXPos() && lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX < minion[j].getXPos() + minion[j].iHitBoxX && ((minion[j].getYPos() <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && minion[j].getYPos() + minion[j].iHitBoxY >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) || (lMinion[i + 1][k]->getYPos() <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY >= minion[j].getYPos() + minion[j].iHitBoxY))) {
    	// 								if(minion[j].killOtherUnits && minion[j].moveSpeed > 0 && lMinion[i + 1][k]->minionSpawned) {
    	// 									lMinion[i + 1][k]->setMinionState(-2);
    	// 									minion[j].collisionWithAnotherUnit();
    	// 								}
      //
    	// 								if(lMinion[i + 1][k]->killOtherUnits && lMinion[i + 1][k]->moveSpeed > 0 && minion[j].minionSpawned) {
    	// 									minion[j].setMinionState(-2);
    	// 									lMinion[i + 1][k]->collisionWithAnotherUnit();
    	// 								}
    	// 								/*
    	// 								if(minion[j].getYPos() + minion[j].iHitBoxY < lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
    	// 									minion[j].onAnotherMinion = true;
    	// 									continue;
    	// 								} else {
    	// 									lMinion[i + 1][k]->onAnotherMinion = true;
    	// 									continue;
    	// 								}*/
      //
    	// 								if(minion[j].getYPos() - 4 <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && minion[j].getYPos() + 4 >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
    	// 									lMinion[i + 1][k]->onAnotherMinion = true;
    	// 								} else if(lMinion[i + 1][k]->getYPos() - 4 <= minion[j].getYPos() + minion[j].iHitBoxY && lMinion[i + 1][k]->getYPos() + 4 >= minion[j].getYPos() + minion[j].iHitBoxY) {
    	// 									minion[j].onAnotherMinion = true;
    	// 								} else {
    	// 									minion[j].collisionEffect();
    	// 									lMinion[i + 1][k]->collisionEffect();
    	// 								}
    	// 							}
    	// 						}
    	// 					}
    	// 				}
    	// 			}
    	// 		}
    	// 	}
    	// }

      foreach (Minion[] minion; this.lMinion) {
        // ----- COLLISION WITH PLAYER
        for(int i = getListID(-cast(int)fXPos + Core.instance().getPlayer().getXPos()) - (getListID(-cast(int)fXPos + Core.instance().getPlayer().getXPos()) > 0 ? 1 : 0), iSize = i + 2; i < iSize; i++) {
          for(int j = 0, jSize = cast(int)minion.length; j < jSize; j++) {
            if(minion[j].deadTime < 0) {
              if((Core.instance().getPlayer().getXPos() - fXPos >= minion[j].getXPos() && Core.instance().getPlayer().getXPos() - fXPos <= minion[j].getXPos() + minion[j].iHitBoxX) || (Core.instance().getPlayer().getXPos() - fXPos + Core.instance().getPlayer().getHitBoxX() >= minion[j].getXPos() && Core.instance().getPlayer().getXPos() - fXPos + Core.instance().getPlayer().getHitBoxX() <= minion[j].getXPos() + minion[j].iHitBoxX)) {
                if(minion[j].getYPos() - 2 <= Core.instance().getPlayer().getYPos() + Core.instance().getPlayer().getHitBoxY() && minion[j].getYPos() + 16 >= Core.instance().getPlayer().getYPos() + Core.instance().getPlayer().getHitBoxY()) {
                  minion[j].collisionWithPlayer(true);
                } else if((minion[j].getYPos() <= Core.instance().getPlayer().getYPos() + Core.instance().getPlayer().getHitBoxY() && minion[j].getYPos() + minion[j].iHitBoxY >= Core.instance().getPlayer().getYPos() + Core.instance().getPlayer().getHitBoxY()) || (minion[j].getYPos() <= Core.instance().getPlayer().getYPos() && minion[j].getYPos() + minion[j].iHitBoxY >= Core.instance().getPlayer().getYPos())) {
                  minion[j].collisionWithPlayer(false);
                }
              }
            }
          }
        }
        // foreach (int i, Minion min; minion) {
        //     min.draw(this.gameData.getMinion(min.getBloockID()).getSprite().getTexture());
        //     //this.textDrawer.drawWS(to!string(i), min.getXPos() + cast(int)fXPos, min.getYPos(), 0, 0, 0, 8);
        // }
      }
    }

    int getLevelType()
    {
        return 1;
    }


    void checkSpawnPoint() {
    	if(getNumOfSpawnPoints() > 1) {
    		for(int i = iSpawnPointID + 1; i < getNumOfSpawnPoints(); i++) {
    			if(getSpawnPointXPos(i) > Core.instance().getPlayer().getXPos() - fXPos && getSpawnPointXPos(i) < Core.instance().getPlayer().getXPos() - fXPos + 128) {
    				iSpawnPointID = i;
    			}
    		}
    	}
    }

    int getNumOfSpawnPoints() {
      int returnValue = 1;
    	switch(currentLevelID) {
    		case 0: case 1: case 2: case 4: case 5: case 8: case 9: case 10: case 13: case 14: case 16: case 17: case 18: case 20: case 21: case 22: case 24: case 25: case 26:
    			returnValue = 2;
          break;
          default: break;
    	}

    	return returnValue;
    }

    int getSpawnPointXPos(int iID) {
      int returnValue = 84;
    	// switch(currentLevelID) {
    	// 	case 0:
    	// 		switch(iID) {
      //       default: break;
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 82*32;
      //         break;
    	// 		}
    	// 	case 1:
    	// 		switch(iID) {
      //       default: break;
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 89*32;
      //         break;
    	// 		}
    	// 	case 2:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //           break;
    	// 			case 1:
    	// 				returnValue = 66*32;
      //           break;
      //           default: break;
    	// 		}
    	// 	case 3:
    	// 		returnValue = 64;
    	// 	case 4:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 98*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 5:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 86*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 6:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 114*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 7:
    	// 		returnValue = 64;
    	// 	case 8:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 90*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 9:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 98*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 10:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 66*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 13:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 95*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 14:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 65*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 16:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 97*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 17:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84 + 80*32;
      //         break;
    	// 			case 1:
    	// 				returnValue = 177*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 18:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 66*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 20:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 98*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 21:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84 + 85*32;
      //         break;
    	// 			case 1:
    	// 				returnValue = 183*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 22:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 98*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 24:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 98*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 25:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 86*32;
      //         break;
      //         default: break;
    	// 		}
    	// 	case 26:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				returnValue = 84;
      //         break;
    	// 			case 1:
    	// 				returnValue = 113*32;
      //         break;
      //         default: break;
    	// 		}
      //   default: break;
    	// }

    	return returnValue;
    }

    void UpdatePlayer() {
     Core.instance().getPlayer().update();
     checkSpawnPoint();
   }

    int getSpawnPointYPos(int iID) {
    	// switch(currentLevelID) {
    	// 	case 1:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				return 64;
      //         default: break;
    	// 		}
    	// 	case 5: case 25:
    	// 		switch(iID) {
    	// 			case 0:
    	// 				return 64;
    	// 			case 1:
    	// 				return GameConfig.window.HEIGHT - 48 - Core.instance().getPlayer().getHitBoxY();
      //       default: break;
    	// 		}
    	// 	case 3: case 7: case 11: case 15: case 19: case 23: case 27: case 31:
    	// 		return 150;
      //   default: break;
    	// }

    	return GameConfig.window.HEIGHT - 48 - Core.instance().getPlayer().getHitBoxY();
    }

    void setSpawnPoint() {
    	float X = cast(float)getSpawnPointXPos(iSpawnPointID);

    	if(X > 6*32) {
    		fXPos = -(X - 6*32);
    		Core.instance().getPlayer().setXPos(6*32);
    		Core.instance().getPlayer().setYPos(cast(float)getSpawnPointYPos(iSpawnPointID));
    	} else {
    		fXPos = 0;
    		Core.instance().getPlayer().setXPos(X);
    		Core.instance().getPlayer().setYPos(cast(float)getSpawnPointYPos(iSpawnPointID));
    	}

    	Core.instance().getPlayer().setMoveDirection(true);
    }

    void addPlayerFireBall(int X, int Y, bool moveDirection)
    {
    	 lMinion[getListID(X)] ~= new PlayerFireBall(X, Y, moveDirection);
    }

}
