module levels.data.abstractlevel;

import levels.data.leveldatainterface;
import levels.levelbuilder;


class AbstractLevel : LevelDataInterface
{
  protected:
    int time = 400;
    int width = 260;
    int height = 25;
    int levelType = 0;
    
    string title;
    MinionsData[] minions;
    Data[] grassData;
    Data[] cloudData;
    Data[] bushData;
    Data[] pipeData;
    Data endData;
    CastleData castleData;

  public:
    string getTitle()
    {
       return this.title;
    }

    int getLevelType()
    {
        return this.levelType;
    }

    int getTime()
    {
        return this.time;
    }

    int getWidth()
    {
        return this.width;
    }

    int getHeight()
    {
        return this.height;
    }

    MinionsData[] getMinions()
    {
        return this.minions;
    }

    Data[] getGrass()
    {
        return this.grassData;
    }

    Data[] getBushes()
    {
        return this.bushData;
    }

    Data[] getPipes()
    {
        return this.pipeData;
    }

    Data[] getClouds()
    {
        return this.cloudData;
    }

    Data getEnd()
    {
        return this.endData;
    }

    CastleData getCastle()
    {
        return this.castleData;
    }

}
