module levels.data.leveldatainterface;

import levels.levelbuilder;

interface LevelDataInterface
{

  MinionsData[] getMinions();
  Data[] getGrass();
  Data[] getBushes();
  Data[] getPipes();
  Data[] getClouds();
  Data getEnd();
  CastleData getCastle();
  string getTitle()
  out (result) { assert(result.length > 0); }

  int getLevelType();
  int getTime()
  out (result) { assert(result > 10); }
  int getWidth();
  int getHeight();

}
