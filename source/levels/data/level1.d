module levels.data.level1s;

import levels.data.leveldatainterface;
import levels.levelbuilder;
import levels.data.abstractlevel;

class Level1 : AbstractLevel
{
  protected:
    string title = "1-1";

  public:
    this()
    {
        minions ~= LevelHelper.createGoombas(704, 368, true);
        minions ~= LevelHelper.createGoombas(1280, 368, true);
      	minions ~= LevelHelper.createGoombas(1632, 368, true);
      	minions ~= LevelHelper.createGoombas(1680, 368, true);
      	minions ~= LevelHelper.createGoombas(2560, 112, true);
      	minions ~= LevelHelper.createGoombas(2624, 112, true);
      	minions ~= LevelHelper.createGoombas(3104, 368, true);
      	minions ~= LevelHelper.createGoombas(3152, 368, true);
      	minions ~= LevelHelper.createGoombas(3648, 368, true);
      	minions ~= LevelHelper.createGoombas(3696, 368, true);
      	minions ~= LevelHelper.createGoombas(3968, 368, true);
      	minions ~= LevelHelper.createGoombas(4016, 368, true);
      	minions ~= LevelHelper.createGoombas(4096, 368, true);
      	minions ~= LevelHelper.createGoombas(4144, 368, true);
      	minions ~= LevelHelper.createGoombas(5568, 368, true);
      	minions ~= LevelHelper.createGoombas(5612, 368, true);

        grassData ~= LevelHelper.createGrass(11, 2, 3);
        grassData ~= LevelHelper.createGrass(23, 2, 1);
        grassData ~= LevelHelper.createGrass(41, 2, 2);
        grassData ~= LevelHelper.createGrass(59, 2, 3);
        grassData ~= LevelHelper.createGrass(71, 2, 1);
        grassData ~= LevelHelper.createGrass(89, 2, 2);
        grassData ~= LevelHelper.createGrass(107, 2, 3);
        grassData ~= LevelHelper.createGrass(119, 2, 1);
        grassData ~= LevelHelper.createGrass(137, 2, 2);
        grassData ~= LevelHelper.createGrass(157, 2, 1);
        grassData ~= LevelHelper.createGrass(167, 2, 1);
        grassData ~= LevelHelper.createGrass(205, 2, 1);
        grassData ~= LevelHelper.createGrass(215, 2, 1);

        cloudData ~= LevelHelper.createCloud(8, 10, 1);
        cloudData ~= LevelHelper.createCloud(19, 11, 1);
        cloudData ~= LevelHelper.createCloud(27, 10, 3);
        cloudData ~= LevelHelper.createCloud(36, 11, 2);
        cloudData ~= LevelHelper.createCloud(56, 10, 1);
        cloudData ~= LevelHelper.createCloud(67, 11, 1);
        cloudData ~= LevelHelper.createCloud(75, 10, 3);
        cloudData ~= LevelHelper.createCloud(84, 11, 2);
        cloudData ~= LevelHelper.createCloud(104, 10, 1);
        cloudData ~= LevelHelper.createCloud(115, 11, 1);
        cloudData ~= LevelHelper.createCloud(123, 10, 3);
        cloudData ~= LevelHelper.createCloud(132, 11, 2);
        cloudData ~= LevelHelper.createCloud(152, 10, 1);
        cloudData ~= LevelHelper.createCloud(163, 11, 1);
        cloudData ~= LevelHelper.createCloud(171, 10, 3);
        cloudData ~= LevelHelper.createCloud(180, 11, 2);
        cloudData ~= LevelHelper.createCloud(200, 10, 1);
        cloudData ~= LevelHelper.createCloud(211, 11, 1);
        cloudData ~= LevelHelper.createCloud(219, 10, 3);

        bushData ~= LevelHelper.createBush(0, 2, 2);
        bushData ~= LevelHelper.createBush(16, 2, 1);
        bushData ~= LevelHelper.createBush(48, 2, 2);
        bushData ~= LevelHelper.createBush(64, 2, 1);
        bushData ~= LevelHelper.createBush(96, 2, 2);
        bushData ~= LevelHelper.createBush(112, 2, 1);
        bushData ~= LevelHelper.createBush(144, 2, 2);
        bushData ~= LevelHelper.createBush(160, 2, 1);
        bushData ~= LevelHelper.createBush(192, 2, 2);
        bushData ~= LevelHelper.createBush(208, 2, 1);

        pipeData ~= LevelHelper.createPipe(28, 2, 1);
        pipeData ~= LevelHelper.createPipe(28, 2, 1);
        pipeData ~= LevelHelper.createPipe(38, 2, 2);
        pipeData ~= LevelHelper.createPipe(46, 2, 3);
        pipeData ~= LevelHelper.createPipe(57, 2, 3);
        pipeData ~= LevelHelper.createPipe(163, 2, 1);
        pipeData ~= LevelHelper.createPipe(179, 2, 1);

        endData = LevelHelper.createEnd(198, 3, 9);
        castleData = LevelHelper.createCastle(202, 2);
    }

}
