import graphics.sprite;
import graphics.image;
import derelict.sdl2.sdl;
import graphics.vector2;
import std.conv;
import std.stdio;
import gamecore;
import config;

class Player
{
private:

	SDL_Renderer* renderer;
	Sprite[] sMario;
	int iSpriteID;
  	uint iMoveAnimationTime;

	Image tMarioLVLUP;

	float fXPos, fYPos;
	int iNumOfLives;

	bool unKillAble;
	bool starEffect;

	int unKillAbleTimeFrameID;
	int unKillAbleFrameID;

	bool inLevelDownAnimation;
	int inLevelDownAnimationFrameID;

	int iScore;
	int iCoins;

	int iComboPoints, iFrameID;

	// ----- LVL UP

	int powerLVL;
	// -- LEVEL CHANGE ANIMATION
	bool inLevelAnimation;
	bool inLevelAnimationType; // -- true = UP, false = DOWN

	int inLevelAnimationFrameID;

	// ----- LVL UP
	// ----- MOVE

	bool moveDirection; // true = LEFT, false = RIGHT
	bool bMove;
	bool changeMoveDirection;
	bool newMoveDirection;

	static const int maxMove = 4;

	int currentMaxMove;
	int moveSpeed;
	uint iTimePassed;

	bool bSquat;

	int onPlatformID;

	// ----- MOVE
	// ----- JUMP

	int jumpState;

	float startJumpSpeed;
	float currentJumpSpeed;
	float jumpDistance;
	float currentJumpDistance;

	float currentFallingSpeed;

	bool springJump;

	// ----- JUMP
	// ----- BUBBLE

	uint nextBubbleTime;
	int nextFallFrameID;

	const static int iSmallX = 24, iSmallY = 32;
	const static int iBigX = 32, iBigY = 64;

	int nextFireBallFrameID;

	// ----- Method
	void movePlayer()
  {
		if (bMove && !changeMoveDirection && (!bSquat || powerLVL == 0)) {
			if (moveSpeed > currentMaxMove) {
				--moveSpeed;
			}
			else if (SDL_GetTicks() - (100 + 35 * moveSpeed) >= iTimePassed && moveSpeed < currentMaxMove) {
				++moveSpeed;
				iTimePassed = SDL_GetTicks();
			}
			else if (moveSpeed == 0) {
				moveSpeed = 1;
			}
		} else {
			if (SDL_GetTicks() - (50 + 15 * (currentMaxMove - moveSpeed) * (bSquat && powerLVL > 0 ? 6 : 1)) > iTimePassed && moveSpeed != 0) {
				--moveSpeed;
				iTimePassed = SDL_GetTicks();
				if (jumpState == 0 && !Core.instance().getLevel().getUnderWater()) setMarioSpriteID(6);
			}

			if (changeMoveDirection && moveSpeed <= 1) {
				moveDirection = newMoveDirection;
				changeMoveDirection = false;
				bMove = true;
			}
		}

		if (moveSpeed > 0) {
			if (moveDirection) {
				updateXPos(moveSpeed);
			}
			else {
				updateXPos(-moveSpeed);
			}

			// ----- SPRITE ANIMATION
			if(Core.instance().getLevel().getUnderWater()) {
				//swimingAnimation();
			} else if (!changeMoveDirection && jumpState == 0 && bMove) {
				moveAnimation();
			}
			// ----- SPRITE ANIMATION
		}
		else if (jumpState == 0) {
			setMarioSpriteID(1);
			updateXPos(0);
		} else {
			updateXPos(0);
		}

		if(bSquat && !Core.instance().getLevel().getUnderWater() && powerLVL > 0) {
			setMarioSpriteID(7);
		}
  }

	bool checkCollisionBot(int nX, int nY)
  {
    Vector2 vLT = getBlockLB(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY);

    if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
      Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 1);
    }

    destroy(vLT);

    vLT = getBlockRB(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY);

    if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
      Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 1);
    }

    destroy(vLT);
    return true;
  }

	bool checkCollisionCenter(int nX, int nY)
  {
		if(powerLVL == 0) {
			Vector2 vLT = getBlockLC(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY);

			if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
				Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 2);
			}

			destroy(vLT);

			vLT = getBlockRC(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY);

			if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
				Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 2);
			}

			destroy(vLT);
		} else {
			Vector2 vLT = getBlockLC(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY + (powerLVL > 0 ? 16 : 0));

			if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
				Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 2);
			}

			destroy(vLT);

			vLT = getBlockRC(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY + (powerLVL > 0 ? 16 : 0));

			if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
				Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 2);
			}

			destroy(vLT);

			vLT = getBlockLC(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY - (powerLVL > 0 ? 16 : 0));

			if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
				Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 2);
			}

			destroy(vLT);

			vLT = getBlockRC(fXPos - Core.instance().getLevel().getXPos() + nX, fYPos + nY - (powerLVL > 0 ? 16 : 0));

			if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
				Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 2);
			}

			destroy(vLT);
		}

		return true;
  }

	Vector2 getBlockLB(float nX, float nY)
  {
     return Core.instance().getLevel().getBlockID(to!int(nX) + 1, to!int(nY) + getHitBoxY() + 2);
  }

	Vector2 getBlockRB(float nX, float nY)
  {
    return Core.instance().getLevel().getBlockID(to!int(nX) + getHitBoxX() - 1, to!int(nY) + getHitBoxY() + 2);
  }

	Vector2 getBlockLC(float nX, float nY)
  {
      return Core.instance().getLevel().getBlockID(to!int(nX) - 1, to!int(nY) + getHitBoxY()/2);
  }

	Vector2 getBlockRC(float nX, float nY)
  {
    	return Core.instance().getLevel().getBlockID(to!int(nX) + getHitBoxX() + 1, to!int(nY) + getHitBoxY()/2);
  }

	Vector2 getBlockLT(float nX, float nY)
  {
    	return Core.instance().getLevel().getBlockID(to!int(nX) + 1, to!int(nY));
  }

	Vector2 getBlockRT(float nX, float nY)
  {
    	return Core.instance().getLevel().getBlockID(to!int(nX) + getHitBoxX() - 1, to!int(nY));
  }

public:
  this(SDL_Renderer* rR, float fXPos, float fYPos)
  {
		this.renderer = rR;
    this.fXPos = fXPos;
    this.fYPos = fYPos;
    this.iNumOfLives = 3;

    this.iSpriteID = 1;

    this.iScore = this.iCoins = 0;
    this.iFrameID = 0, this.iComboPoints = 1;

    this.nextBubbleTime = 0;
    this.nextFallFrameID = 0;

    this.powerLVL = 0;
    this.inLevelAnimation = false;
    this.inLevelAnimationType = false;

    this.unKillAble = false;
    this.starEffect = false;
    this.unKillAbleTimeFrameID = 0;

    this.inLevelDownAnimation = false;
    this.inLevelDownAnimationFrameID = 0;

    this.moveDirection = true;
    this.currentMaxMove = 4;
    this.moveSpeed = 0;
    this.bMove = false;
    this.changeMoveDirection = false;
    this.bSquat = false;

    this.onPlatformID = -1;

    this.springJump = false;

    this.iTimePassed = SDL_GetTicks();

    this.jumpState = 0;
    this.startJumpSpeed = 7.65f;
    this.currentFallingSpeed = 2.7f;

    this.iMoveAnimationTime = 0;

    this.nextFireBallFrameID = 8;

    // ----- LOAD SPRITE
    string[] tempS;
    uint[] tempI;
    string[] tempSs;
    uint[] tempIs;

    //srand((unsigned)time(NULL));

    // ----- 0
    tempS ~= "mario/mario_death";
    tempI ~= 0;
    sMario ~= new Sprite(rR,  tempS, tempI, true);

    tempS = tempSs;

    // ----- 1
    tempS ~= "mario/mario";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 2
    tempS ~= "mario/mario_move0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ----- 3
    tempS ~= "mario/mario_move1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 4
    tempS ~= "mario/mario_move2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 5
    tempS ~= "mario/mario_jump";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 6
    tempS ~= "mario/mario_st";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 7
    tempS ~= "mario/mario"; // SQUAT
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 8
    tempS ~= "mario/mario_underwater0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 9
    tempS ~= "mario/mario_underwater1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 10
    tempS ~= "mario/mario_end";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 11
    tempS ~= "mario/mario_end1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ---------- BIG
    // ----- 12
    tempS ~= "mario/mario1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 13
    tempS ~= "mario/mario1_move0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 14
    tempS ~= "mario/mario1_move1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 15
    tempS ~= "mario/mario1_move2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 16
    tempS ~= "mario/mario1_jump";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 17
    tempS ~= "mario/mario1_st";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 18
    tempS ~= "mario/mario1_squat"; // SQUAT
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 19
    tempS ~= "mario/mario1_underwater0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 20
    tempS ~= "mario/mario1_underwater1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 21
    tempS ~= "mario/mario1_end";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 22
    tempS ~= "mario/mario1_end1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ----- 23
    tempS ~= "mario/mario2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 24
    tempS ~= "mario/mario2_move0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 25
    tempS ~= "mario/mario2_move1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 26
    tempS ~= "mario/mario2_move2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 27
    tempS ~= "mario/mario2_jump";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 28
    tempS ~= "mario/mario2_st";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 29
    tempS ~= "mario/mario2_squat"; // SQUAT
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 30
    tempS ~= "mario/mario2_underwater0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 31
    tempS ~= "mario/mario2_underwater1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 32
    tempS ~= "mario/mario2_end";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 33
    tempS ~= "mario/mario2_end1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ----- 34
    tempS ~= "mario/mario2s";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 35
    tempS ~= "mario/mario2s_move0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 36
    tempS ~= "mario/mario2s_move1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 37
    tempS ~= "mario/mario2s_move2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 38
    tempS ~= "mario/mario2s_jump";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 39
    tempS ~= "mario/mario2s_st";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 40
    tempS ~= "mario/mario2s_squat"; // SQUAT
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 41
    tempS ~= "mario/mario2s_underwater0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 42
    tempS ~= "mario/mario2s_underwater1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 43
    tempS ~= "mario/mario2s_end";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 44
    tempS ~= "mario/mario2s_end1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ----- 45
    tempS ~= "mario/mario_s0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 46
    tempS ~= "mario/mario_s0_move0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 47
    tempS ~= "mario/mario_s0_move1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 48
    tempS ~= "mario/mario_s0_move2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 49
    tempS ~= "mario/mario_s0_jump";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 50
    tempS ~= "mario/mario_s0_st";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 51
    tempS ~= "mario/mario_s0"; // SQUAT
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 52
    tempS ~= "mario/mario_s0_underwater0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 53
    tempS ~= "mario/mario_s0_underwater1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 54
    tempS ~= "mario/mario_s0_end";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 55
    tempS ~= "mario/mario_s0_end1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ----- 56
    tempS ~= "mario/mario_s1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 57
    tempS ~= "mario/mario_s1_move0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 58
    tempS ~= "mario/mario_s1_move1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 59
    tempS ~= "mario/mario_s1_move2";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 60
    tempS ~= "mario/mario_s1_jump";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 61
    tempS ~= "mario/mario_s1_st";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 62
    tempS ~= "mario/mario_s1"; // SQUAT
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 63
    tempS ~= "mario/mario_s1_underwater0";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 64
    tempS ~= "mario/mario_s1_underwater1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 65
    tempS ~= "mario/mario_s1_end";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
    // ----- 66
    tempS ~= "mario/mario_s1_end1";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;

    // ----- LOAD SPRITE

    // ----- 67
    tempS ~= "mario/mario_lvlup";
    sMario ~= new Sprite(rR,  tempS, tempI, true);
    tempS = tempSs;
  }

	void draw()
  {
		if(!inLevelDownAnimation || Core.instance().getLevel().getInEvent()) {
			sMario[getMarioSpriteID()].getTexture().draw(to!int(fXPos), to!int(fYPos) + (Core.instance().getLevel().getInEvent() ? 0 : 2), !moveDirection);
		} else {
			if(inLevelDownAnimationFrameID%15 < (inLevelDownAnimationFrameID > 120 ? 7 : inLevelDownAnimationFrameID > 90 ? 9 : inLevelDownAnimationFrameID > 60 ? 11 : inLevelDownAnimationFrameID > 30 ? 13 : 14)) {
				sMario[getMarioSpriteID()].getTexture().draw(to!int(fXPos), to!int(fYPos) + (Core.instance().getLevel().getInEvent() ? 0 : 2), !moveDirection);
			}
		}
	}

	void update()
  {
    playerPhysics();
  	movePlayer();

  	if(iFrameID > 0) {
  		--iFrameID;
  	} else if(iComboPoints > 1) {
  		--iComboPoints;
  	}

  	if(powerLVL == 2) {
  		if(nextFireBallFrameID > 0) {
  			--nextFireBallFrameID;
  		}
  	}

  	if(inLevelDownAnimation) {
  		if(inLevelDownAnimationFrameID > 0) {
  			--inLevelDownAnimationFrameID;
  		} else {
  			unKillAble = false;
  		}
  	}
  }

	void playerPhysics()
	{
		if (jumpState == 1) {
			updateYPos(-cast(int)currentJumpSpeed);
			currentJumpDistance += cast(int)currentJumpSpeed;

			currentJumpSpeed *= (currentJumpDistance / jumpDistance > 0.75f ? 0.972f : 0.986f);

			if (currentJumpSpeed < 2.5f) {
				currentJumpSpeed = 2.5f;
			}

			if(!GameConfig.keySpace && currentJumpDistance > 64 && !springJump) {
				jumpDistance = 16;
				currentJumpDistance = 0;
				currentJumpSpeed = 2.5f;
			}

			if (jumpDistance <= currentJumpDistance) {
				jumpState = 2;
			}
		} else {
			if(onPlatformID == -1) {
				onPlatformID = Core.instance().getLevel().checkCollisionWithPlatform(to!int(fXPos), to!int(fYPos), getHitBoxX(), getHitBoxY());
				if(onPlatformID >= 0) {
					if (
						Core.instance().getLevel().checkCollisionLB(
							to!int(fXPos - Core.instance().getLevel().getXPos() + 2), to!int(fYPos) + 2, getHitBoxY(), true)
							|| Core.instance().getLevel().checkCollisionRB(
								to!int(fXPos - Core.instance().getLevel().getXPos() - 2),
								to!int(fYPos) + 2, getHitBoxX(), getHitBoxY(), true)
								) {
						onPlatformID = -1;
						resetJump();
					} else {
						fYPos = cast(float)Core.instance().getLevel().getPlatform(onPlatformID).getYPos() - getHitBoxY();
						resetJump();
						Core.instance().getLevel().getPlatform(onPlatformID).turnON();
					}
				}
			} else {
				onPlatformID = Core.instance().getLevel().checkCollisionWithPlatform(to!int(fXPos), to!int(fYPos), getHitBoxX(), getHitBoxY());
			}

			if(onPlatformID >= 0) {
				if (Core.instance().getLevel().checkCollisionLB(cast(int)(fXPos - Core.instance().getLevel().getXPos() + 2), to!int(fYPos) + 2, getHitBoxY(), true) || Core.instance().getLevel().checkCollisionRB(cast(int)(fXPos - Core.instance().getLevel().getXPos() - 2), to!int(fYPos) + 2, getHitBoxX(), getHitBoxY(), true)) {
					onPlatformID = -1;
					resetJump();
				} else {
					fYPos += Core.instance().getLevel().getPlatform(onPlatformID).getMoveY();
					Core.instance().getLevel().getPlatform(onPlatformID).moveY();
					//if(moveSpeed == 0)
					Core.instance().getLevel().setXPos(Core.instance().getLevel().getXPos() - Core.instance().getLevel().getPlatform(onPlatformID).getMoveX());

					jumpState = 0;
				}
			}
			else if (!Core.instance().getLevel().checkCollisionLB(cast(int)(fXPos - Core.instance().getLevel().getXPos() + 2), to!int(fYPos) + 2, getHitBoxY(), true) &&
				!Core.instance().getLevel().checkCollisionRB(cast(int)(fXPos - Core.instance().getLevel().getXPos() - 2), to!int(fYPos) + 2, getHitBoxX(), getHitBoxY(), true)) {

				if(nextFallFrameID > 0) {
					--nextFallFrameID;
				} else {
					currentFallingSpeed *= 1.05f;

					if (currentFallingSpeed > startJumpSpeed) {
						currentFallingSpeed = startJumpSpeed;
					}

					updateYPos(cast(int)currentFallingSpeed);
				}


				jumpState = 2;

				setMarioSpriteID(5);
			} else if(jumpState == 2) {
				resetJump();
			} else {
				checkCollisionBot(0, 0);
			}
		}
	}

	void updateXPos(int iN)
	{
		checkCollisionBot(iN, 0);
		checkCollisionCenter(iN, 0);
		if (iN > 0) {
			if (!Core.instance().getLevel().checkCollisionRB(cast(int)(fXPos - Core.instance().getLevel().getXPos() + iN), cast(int)fYPos - 2, getHitBoxX(), getHitBoxY(), true) && !Core.instance().getLevel().checkCollisionRT(cast(int)(fXPos - Core.instance().getLevel().getXPos() + iN), cast(int)fYPos + 2, getHitBoxX(), true) &&
				(powerLVL == 0 ? true : (!Core.instance().getLevel().checkCollisionRC(cast(int)(fXPos - Core.instance().getLevel().getXPos() + iN), cast(int)fYPos, getHitBoxX(), getHitBoxY() / 2, true))))
			{
				if (fXPos >= 416 && Core.instance().getLevel().getMoveMap()) {
					Core.instance().getLevel().moveMap(-iN, 0);
				}
				else {
					fXPos += iN;
				}
			}
			else {
				updateXPos(iN - 1);
				if (moveSpeed > 1 && jumpState == 0) --moveSpeed;
			}
		} else if (iN < 0) {
			if (!Core.instance().getLevel().checkCollisionLB(cast(int)(fXPos - Core.instance().getLevel().getXPos() + iN), cast(int)fYPos - 2, getHitBoxY(), true) && !Core.instance().getLevel().checkCollisionLT(cast(int)(fXPos - Core.instance().getLevel().getXPos() + iN), cast(int)fYPos + 2, true) &&
					(powerLVL == 0 ? true : (!Core.instance().getLevel().checkCollisionLC(cast(int)(fXPos - Core.instance().getLevel().getXPos() + iN), cast(int)fYPos, getHitBoxY() / 2, true))))
				{
				if (fXPos <= 192 && Core.instance().getLevel().getXPos() && Core.instance().getLevel().getMoveMap() && GameConfig.controls.canMoveBackward) {

					Core.instance().getLevel().moveMap(-iN, 0);
				}
				else if(fXPos - Core.instance().getLevel().getXPos() + iN >= 0 && fXPos >= 0) {
					fXPos += iN;
				} else if(GameConfig.controls.canMoveBackward && fXPos >= 0) {
					updateXPos(iN + 1);
				}
			}
			else {
				updateXPos(iN + 1);
				if (moveSpeed > 1 && jumpState == 0) --moveSpeed;
			}
		}
	}

	void updateYPos(int iN)
	{
		bool bLEFT, bRIGHT;

		if (iN > 0) {
			bLEFT  = Core.instance().getLevel().checkCollisionLB(cast(int)(fXPos - Core.instance().getLevel().getXPos() + 2), to!int(fYPos) + iN, getHitBoxY(), true);
			bRIGHT = Core.instance().getLevel().checkCollisionRB(cast(int)(fXPos - Core.instance().getLevel().getXPos() - 2), to!int(fYPos) + iN, getHitBoxX(), getHitBoxY(), true);

			if (!bLEFT && !bRIGHT) {
				fYPos += iN;
			} else {
				if (jumpState == 2) {
					jumpState = 0;
				}
				updateYPos(iN - 1);
			}
		} else if(iN < 0) {
			bLEFT  = Core.instance().getLevel().checkCollisionLT(cast(int)(fXPos - Core.instance().getLevel().getXPos() + 2), to!int(fYPos) + iN, false);
			bRIGHT = Core.instance().getLevel().checkCollisionRT(cast(int)(fXPos - Core.instance().getLevel().getXPos() - 2), to!int(fYPos) + iN, getHitBoxX(), false);

			if(Core.instance().getLevel().checkCollisionWithPlatform(to!int(fXPos), to!int(fYPos), 0, 0) >= 0
			|| Core.instance().getLevel().checkCollisionWithPlatform(to!int(fXPos), to!int(fYPos), getHitBoxX(), 0) >= 0) {
				jumpState = 2;
			}
			else if (!bLEFT && !bRIGHT) {
				fYPos += iN;
			} else  {
				if (jumpState == 1) {
					if (!bLEFT && bRIGHT) {
						Vector2 vRT = getBlockRT(fXPos - Core.instance().getLevel().getXPos(), fYPos + iN);

						if(!Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vRT.getX(), vRT.getY()).getBlockID()).getVisible()) {
							if(Core.instance().getLevel().blockUse(vRT.getX(), vRT.getY(), Core.instance().getLevel().getMapBlock(vRT.getX(), vRT.getY()).getBlockID(), 0)) {
								jumpState = 2;
							} else {
								fYPos += iN;
							}
							fYPos += iN;
						} else if(cast(int)(fXPos + getHitBoxX() - Core.instance().getLevel().getXPos()) % 32 <= 8) {
							updateXPos(cast(int)-(cast(int)(fXPos + getHitBoxX() - Core.instance().getLevel().getXPos()) % 32));
						} else if(Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vRT.getX(), vRT.getY()).getBlockID()).getUse()) {
							if(Core.instance().getLevel().blockUse(vRT.getX(), vRT.getY(), Core.instance().getLevel().getMapBlock(vRT.getX(), vRT.getY()).getBlockID(), 0)) {
								jumpState = 2;
							} else {
								fYPos += iN;
							}
								fYPos += iN;
						} else {
							jumpState = 2;
						}

						destroy(vRT);
					} else if (bLEFT && !bRIGHT) {
						Vector2 vLT = getBlockLT(fXPos - Core.instance().getLevel().getXPos(), fYPos + iN);
						if(!Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getVisible()) {
							if(Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 0)) {
								jumpState = 2;
							} else {
								fYPos += iN;
							}
							fYPos += iN;
						} else if (cast(int)(fXPos - Core.instance().getLevel().getXPos()) % 32 >= 24) {
							updateXPos(cast(int)(32 - cast(int)(fXPos - Core.instance().getLevel().getXPos()) % 32));
						} else if(Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
							if(Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 0)) {
								jumpState = 2;
							} else {
								fYPos += iN;
							}
							fYPos += iN;
						} else {
							jumpState = 2;
						}

						destroy(vLT);
					} else {
						if (cast(int)(fXPos + getHitBoxX() - Core.instance().getLevel().getXPos()) % 32 > 32 - cast(int)(fXPos - Core.instance().getLevel().getXPos()) % 32) {
							Vector2 vRT = getBlockRT(fXPos - Core.instance().getLevel().getXPos(), fYPos + iN);

							if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vRT.getX(), vRT.getY()).getBlockID()).getUse()) {
								if(Core.instance().getLevel().blockUse(vRT.getX(), vRT.getY(), Core.instance().getLevel().getMapBlock(vRT.getX(), vRT.getY()).getBlockID(), 0)) {
									jumpState = 2;
								}
							} else {
								jumpState = 2;
							}

							destroy(vRT);
						} else {
							Vector2 vLT = getBlockLT(fXPos - Core.instance().getLevel().getXPos(), fYPos + iN);

							if (Core.instance().getGameData().getBlock(Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID()).getUse()) {
								if(Core.instance().getLevel().blockUse(vLT.getX(), vLT.getY(), Core.instance().getLevel().getMapBlock(vLT.getX(), vLT.getY()).getBlockID(), 0)) {
									jumpState = 2;
								}
							} else {
								jumpState = 2;
							}

							destroy(vLT);
						}
					}
				}

				updateYPos(iN + 1);
			}
		}

		if(to!int(fYPos) % 2 == 1) {
			fYPos += 1;
		}

		if(!Core.instance().getLevel().getInEvent() && fYPos - getHitBoxY() > GameConfig.window.HEIGHT) {
			Core.instance().getLevel().playerDeath(false, true);
			fYPos = -80;
		}
	}

	void powerUPAnimation()
	{
		if(inLevelDownAnimation) {
			if(inLevelAnimationFrameID%15 < 5) {
				iSpriteID = 12;
				if(inLevelAnimationFrameID != 0 && inLevelAnimationFrameID%15 == 0) {
					fYPos += 16;
					fXPos -= 4;
				}
			} else if(inLevelAnimationFrameID%15 < 10) {
				iSpriteID = 67;
				if(inLevelAnimationFrameID%15 == 5) {
					fYPos += 16;
					fXPos += 1;
				}
			} else {
				iSpriteID = 1;
				if(inLevelAnimationFrameID%15 == 10) {
					fYPos -= 32;
					fXPos += 3;
				}
			}

			++inLevelAnimationFrameID;
			if(inLevelAnimationFrameID > 59) {
				inLevelAnimation = false;
				fYPos += 32;
				if(jumpState != 0) {
					setMarioSpriteID(5);
				}
			}
		} else if(powerLVL == 1) {
			if(inLevelAnimationFrameID%15 < 5) {
				iSpriteID = 1;
				if(inLevelAnimationFrameID != 0 && inLevelAnimationFrameID%15 == 0) {
					fYPos += 32;
					fXPos += 4;
				}
			} else if(inLevelAnimationFrameID%15 < 10) {
				iSpriteID = 67;
				if(inLevelAnimationFrameID%15 == 5) {
					fYPos -= 16;
					fXPos -= 3;
				}
			} else {
				iSpriteID = 12;
				if(inLevelAnimationFrameID%15 == 10) {
					fYPos -= 16;
					fXPos -= 1;
				}
			}

			++inLevelAnimationFrameID;
			if(inLevelAnimationFrameID > 59) {
				inLevelAnimation = false;
				if(jumpState != 0) {
					setMarioSpriteID(5);
				}
			}
		} else if(powerLVL == 2) {
			if(inLevelAnimationFrameID%10 < 5) {
				iSpriteID = iSpriteID%11 + 22;
			} else {
				iSpriteID = iSpriteID%11 + 33;
			}

			++inLevelAnimationFrameID;
			if(inLevelAnimationFrameID > 59) {
				inLevelAnimation = false;
				if(jumpState != 0) {
					setMarioSpriteID(5);
				}
				iSpriteID = iSpriteID%11 + 22;
			}
		} else {
			inLevelAnimation = false;
		}
	}

	// ----- MOVE
	void moveAnimation()
	{
		if(SDL_GetTicks() - 65 + moveSpeed * 4 > iMoveAnimationTime) {
			iMoveAnimationTime = SDL_GetTicks();
			if (iSpriteID >= 4 + 11 * powerLVL) {
				setMarioSpriteID(2);
			}
			else {
				++iSpriteID;
			}
		}
	}

	void swimingAnimation() {}

	void startMove() {
		iMoveAnimationTime = SDL_GetTicks();
		iTimePassed = SDL_GetTicks();
		moveSpeed = 1;
		bMove = true;
		if(Core.instance().getLevel().getUnderWater()) {
			setMarioSpriteID(8);
		}
	}
	void resetMove() {
		--moveSpeed;
		bMove = false;
	}
	void stopMove() {
		moveSpeed = 0;
		bMove = false;
		changeMoveDirection = false;
		bSquat = false;
		setMarioSpriteID(1);
	}

	void setMoveDirection(bool moveDirection)
	{
			this.moveDirection = moveDirection;
	}

	bool getChangeMoveDirection() { 	return changeMoveDirection; }
	void setChangeMoveDirection() {
		this.changeMoveDirection = true;
		this.newMoveDirection = !moveDirection;
	}

	void startRun() {
		currentMaxMove = maxMove + (Core.instance().getLevel().getUnderWater() ? 0 : 2);
		createFireBall();
	}

	void resetRun() {
		currentMaxMove = maxMove;
	}

	void createFireBall()
	{
		if(powerLVL == 2) {
			if(nextFireBallFrameID <= 0) {
				Core.instance().getLevel().addPlayerFireBall(cast(int)(fXPos - Core.instance().getLevel().getXPos() + (moveDirection ? getHitBoxX() : -32)), cast(int)(fYPos + getHitBoxY()/2), !moveDirection);
				nextFireBallFrameID = 16;
				Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cFIREBALL);
			}
		}
	}

	// ----- MOVE
	// ----- JUMP
	void jump()
	{
		if(jumpState == 0) {
			startJump(4);
		}
	}

	void startJump(int iH)
	{
		currentJumpSpeed = startJumpSpeed;
		jumpDistance = 32 * iH + 24.0f;
		currentJumpDistance = 0;

		 if(!Core.instance().getLevel().getUnderWater()) {
		 		setMarioSpriteID(5);
		 } else {
			if(jumpState == 0) {
				iMoveAnimationTime = SDL_GetTicks();
				setMarioSpriteID(8);
				swimingAnimation();
			}
			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cSWIM);
	  }

		if(iH > 1) {
			if(powerLVL == 0) {
				Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cJUMP);
			} else {
				Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cJUMPBIG);
			}
		}

		jumpState = 1;
	}

	void resetJump()
	{
		jumpState = 0;
		jumpDistance = 0;
		currentJumpDistance = 0;
		currentFallingSpeed = 2.7f;
		nextFallFrameID = 0;
		springJump = false;
	}
	// ----- JUMP

	void setMarioSpriteID(int iID)
	{
			this.iSpriteID = iID + 11 * powerLVL;
	}

	int getMarioSpriteID()
  {

    // if(starEffect && !inLevelAnimation && CCFG::getMM().getViewID() == CCFG::getMM().eGame) {
			if(starEffect && !inLevelAnimation) {

  		if(unKillAbleTimeFrameID <= 0) {
  			starEffect = unKillAble = false;
  		}

  		if(unKillAbleTimeFrameID == 35) {
  			Core.instance().getMusic().changeMusic(true, true);
  		}

  		++unKillAbleFrameID;

  		--unKillAbleTimeFrameID;

  		if(unKillAbleTimeFrameID < 90) {
  			if(unKillAbleFrameID < 5) {
  				return powerLVL < 1 ? iSpriteID + 44 : powerLVL == 2 ? iSpriteID : iSpriteID + 11;
  			} else if(unKillAbleFrameID < 10) {
  				return powerLVL < 1 ? iSpriteID + 55 : powerLVL == 2 ? iSpriteID + 11 : iSpriteID + 22;
  			} else {
  				unKillAbleFrameID = 0;
  			}
  		} else {
  			if(unKillAbleFrameID < 20) {
  				return powerLVL < 1 ? iSpriteID + 44 : powerLVL == 2 ? iSpriteID : iSpriteID + 11;
  			} else if(unKillAbleFrameID < 40) {
  				return powerLVL < 1 ? iSpriteID + 55 : powerLVL == 2 ? iSpriteID + 11 : iSpriteID + 22;
  			} else {
  				unKillAbleFrameID = 0;
  			}
  		}
  	}

  	return iSpriteID;
  }

	int getHitBoxX()
  {
    return powerLVL == 0 ? iSmallX : iBigX;
  }

	int getHitBoxY()
	{
    return powerLVL == 0 ? iSmallY : bSquat ? 44 : iBigY;
  }

	// ----- get & set -----

	bool getInLevelAnimation()
  {
      return inLevelAnimation;
  }

	void setInLevelAnimation(bool inLevelAnimation)
  {
      this.inLevelAnimation = inLevelAnimation;
  }

	int getXPos()
  {
      return to!int(fXPos);
  }

	void setXPos(float fXPos)
  {
      this.fXPos = fXPos;
  }

	int getYPos()
  {
      return to!int(fYPos);
  }

	void setYPos(float fYPos)
  {
      this.fYPos = fYPos;
  }

	int getPowerLVL()
  {
      return powerLVL;
  }

	void setPowerLVL(int powerLVL)
	{
		if(powerLVL <= 2) {
			if(this.powerLVL < powerLVL) {
				Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cMUSHROOMMEAT);
				setScore(getScore() + 1000);
				Core.instance().getLevel().addPoints(cast(int)(fXPos - Core.instance().getLevel().getXPos() + getHitBoxX() / 2), cast(int)fYPos + 16, "1000", 8, 16);
				inLevelAnimation = true;
				inLevelAnimationFrameID = 0;
				inLevelDownAnimationFrameID = 0;
				inLevelDownAnimation = false;
				this.powerLVL = powerLVL;
			} else if(this.powerLVL > 0) {
				Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cSHRINK);
				inLevelDownAnimation = true;
				inLevelDownAnimationFrameID = 180;
				inLevelAnimation = true;
				inLevelAnimationFrameID = 0;
				this.powerLVL = 0;
				unKillAble = true;
			}
		} else {
			++iNumOfLives;
			Core.instance().getLevel().addPoints(cast(int)(fXPos - Core.instance().getLevel().getXPos() + getHitBoxX() / 2), cast(int)fYPos + 16, "1UP", 10, 14);
			Core.instance().getMusic().PlayChunk(Core.instance().getMusic().eChunk.cONEUP);
		}
	}

	void resetPowerLVL()
  {
      this.powerLVL = 0;
      this.iSpriteID = 1;
  }

	bool getUnkillAble()
  {
      return unKillAble;
  }

	int getNumOfLives()
  {
      return iNumOfLives;
  }

	void setNumOfLives(int iNumOfLives)
  {
      this.iNumOfLives=iNumOfLives;
  }

	bool getStarEffect()
  {
      return starEffect;
  }

	void setStarEffect(bool starEffect)
  {
    if(starEffect && this.starEffect != starEffect) {
      Core.instance().getMusic().PlayMusic(Core.instance().getMusic().eMusic.mSTAR);
    }
    this.starEffect = starEffect;
    this.unKillAble = starEffect;
    this.unKillAbleFrameID = 0;
    this.unKillAbleTimeFrameID = 550;
  }

	int getMoveSpeed() { return moveSpeed;}
	bool getMove() {	return bMove;}
	bool getMoveDirection() { 	return moveDirection; }

	void setNextFallFrameID(int nextFallFrameID)
  {
      this.nextFallFrameID = nextFallFrameID;
  }

	void setCurrentJumpSpeed(float currentJumpSpeed)
  {
      this.currentJumpSpeed = currentJumpSpeed;
  }

	void setMoveSpeed(int moveSpeed)
  {
      this.moveSpeed = moveSpeed;
  }

	int getJumpState()
  {
      return jumpState;
  }

	bool getSquat() { return bSquat; }
	void setSquat(bool bSquat)
  {
    if(bSquat && this.bSquat != bSquat) {
  		if(powerLVL > 0) {
  			fYPos += 20;
  		}
  		this.bSquat = bSquat;
  	} else if(this.bSquat != bSquat) {
  		if(powerLVL > 0) {
  			fYPos -= 20;
  		}
  		this.bSquat = bSquat;
  	}
  }

	Image getMarioLVLUP()
  {
      return tMarioLVLUP;
  }

	Sprite getMarioSprite()
  {
      return sMario[1 + 11 * powerLVL];
  }

	void addCoin()
  {
    ++iCoins;
    iScore += 100;
  }

	uint getScore()
  {
      return iScore;
  }

	void setScore(uint iScore)
  {
      this.iScore = iScore;
  }

	void addComboPoints()
  {
    ++iComboPoints;
    iFrameID = 40;
  }

	int getComboPoints()
  {
    return iComboPoints;
  }

	uint getCoins()
  {
    	return iCoins;
  }

	void setCoins(uint iCoins)
  {
      	this.iCoins = iCoins;
  }

	void setSpringJump(bool springJump)
	{
		this.springJump = springJump;
	}

}
