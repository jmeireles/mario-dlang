import derelict.sdl2.sdl;

class GameConfig
{
    static:
      bool showFpsCounter = true;

      const string TITLE = "Super Mario Bros v0.1";

      enum window : int {
        FULLSCREEN = 0,
        WIDTH = 800,
        HEIGHT = 448
    };

    enum controls : int {
      keyIDA = SDLK_a,
      keyIDS = SDLK_s,
      keyIDD = SDLK_d,
      keyIDSpace = SDLK_SPACE,
      keyIDShift = SDLK_LSHIFT,
      canMoveBackward = 1
    };

    bool keySpace = false;
}
