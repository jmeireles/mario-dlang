module gamecore;
import music;

import levels.levelmanager;
import levels.levelinterface;
import player;
import derelict.sdl2.sdl;
import gamedata;
import drawers.text;

class Core
{
  Music music;
  bool running = true;
  LevelManager levelManager;
  SDL_Renderer* renderer;
  Player player;
  GameData gameData;
  TextDrawer textDrawer;
  bool gameOver = false;

  static Core instance()
  {
    if (!instantiated_) {
      synchronized {
        if (instance_ is null) {
          instance_ = new Core;
        }
        instantiated_ = true;
      }
    }
    return instance_;
  }

  void setGameOver()
  {
      this.levelManager.setGameOver();
  }

  void setTextDrawer(TextDrawer textDrawer)
  {
      this.textDrawer = textDrawer;
  }

  TextDrawer getTextDrawer()
  {
      return this.textDrawer;
  }


  void setGameData(GameData gameData)
  {
      this.gameData = gameData;
  }

  GameData getGameData()
  {
      return this.gameData;
  }

  void setRenderer(SDL_Renderer* renderer)
  {
      this.renderer = renderer;
  }

  SDL_Renderer* getRenderer()
  {
      return this.renderer;
  }

  LevelInterface getLevel()
  {
      return this.levelManager.getCurrentLevel();
  }

  Player getPlayer()
  {
      return this.player;
  }

  void setPlayer(Player player)
  {
      this.player = player;
  }

  LevelManager getLevelManager()
  {
      return this.levelManager;
  }

  void setLevelManager(LevelManager levelManager)
  {
      this.levelManager = levelManager;
  }

  void setMusic(Music music)
  {
      this.music = music;
  }

  Music getMusic()
  {
     return this.music;
  }

  void quit()
  {
     this.running = false;
  }

 private:
  this() {}
  static bool instantiated_;  // Thread local
  __gshared Core instance_;

 }
