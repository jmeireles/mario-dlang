import derelict.sdl2.mixer;
import std.string;

class Music
{
private:
	Mix_Music*[] vMusic;
	Mix_Chunk*[] vChunk;
	int iVolume;

public:

bool musicStopped;

enum eMusic {
  mNOTHING,
  mOVERWORLD,
  mOVERWORLDFAST,
  mUNDERWORLD,
  mUNDERWORLDFAST,
  mUNDERWATER,
  mUNDERWATERFAST,
  mCASTLE,
  mCASTLEFAST,
  mLOWTIME,
  mSTAR,
  mSTARFAST,
  mSCORERING,
};

int currentMusic;

enum eChunk {
  cCOIN,
  cBLOCKBREAK,
  cBLOCKHIT,
  cBOOM,
  cBOWSERFALL,
  cBRIDGEBREAK,
  cBULLETBILL,
  cDEATH,
  cFIRE,
  cFIREBALL,
  cGAMEOVER,
  cINTERMISSION,
  cJUMP,
  cJUMPBIG,
  cLEVELEND,
  cLOWTIME,
  cMUSHROOMAPPER,
  cMUSHROOMMEAT,
  cONEUP,
  cPASUE,
  cPIPE,
  cRAINBOOM,
  cSHOT,
  cSHRINK,
  cSTOMP,
  cSWIM,
  cVINE,
  cCASTLEEND,
  cPRINCESSMUSIC,
};

	this()
  {
      Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
      vMusic ~= loadMusic("overworld");
    	vMusic ~= loadMusic("overworld-fast");
    	vMusic ~= loadMusic("underground");
    	vMusic ~= loadMusic("underground-fast");
    	vMusic ~= loadMusic("underwater");
    	vMusic ~= loadMusic("underwater-fast");
    	vMusic ~= loadMusic("castle");
    	vMusic ~= loadMusic("castle-fast");
    	vMusic ~= loadMusic("lowtime");
    	vMusic ~= loadMusic("starmusic");
    	vMusic ~= loadMusic("starmusic-fast");
    	vMusic ~= loadMusic("scorering");

    	vChunk ~= loadChunk("coin");
    	vChunk ~= loadChunk("blockbreak");
    	vChunk ~= loadChunk("blockhit");
    	vChunk ~= loadChunk("boom");
    	vChunk ~= loadChunk("bowserfall");
    	vChunk ~= loadChunk("bridgebreak");
    	vChunk ~= loadChunk("bulletbill");
    	vChunk ~= loadChunk("death");
    	vChunk ~= loadChunk("fire");
    	vChunk ~= loadChunk("fireball");
    	vChunk ~= loadChunk("gameover");
    	vChunk ~= loadChunk("intermission");
    	vChunk ~= loadChunk("jump");
    	vChunk ~= loadChunk("jumpbig");
    	vChunk ~= loadChunk("levelend");
    	vChunk ~= loadChunk("lowtime");
    	vChunk ~= loadChunk("mushroomappear");
    	vChunk ~= loadChunk("mushroomeat");
    	vChunk ~= loadChunk("oneup");
    	vChunk ~= loadChunk("pause");
    	vChunk ~= loadChunk("shrink");
    	vChunk ~= loadChunk("rainboom");
    	vChunk ~= loadChunk("shot");
    	vChunk ~= loadChunk("shrink");
    	vChunk ~= loadChunk("stomp");
    	vChunk ~= loadChunk("swim");
    	vChunk ~= loadChunk("vine");
    	vChunk ~= loadChunk("castleend");
    	vChunk ~= loadChunk("princessmusic");

    	setVolume(100);
    	this.currentMusic = eMusic.mNOTHING;
  }

	~this()
  {
    Mix_Music*[] vMusics;
    Mix_Chunk*[] vChunks;
    for(uint i = 0; i < vMusic.length; i++) {
        Mix_FreeMusic(vMusic[i]);
    }

    vMusic = vMusics;

    for(uint i = 0; i < vChunk.length; i++) {
      Mix_FreeChunk(vChunk[i]);
    }
    vChunk = vChunks;

  }

	void changeMusic(bool musicByLevel, bool forceChange)
  {
    int eNew = currentMusic;
    //
    // if(musicByLevel) {
    //   if(CCore::getMap()->getInEvent() && CCore::getMap()->getEvent()->inEvent) {
    //     eNew = mNOTHING;
    //     PlayChunk(cINTERMISSION);
    //   } else {
    //     switch(CCore::getMap()->getLevelType()) {
    //       case 0: case 4:
    //         eNew = CCore::getMap()->getMapTime() > 90 ? mOVERWORLD : mOVERWORLDFAST;
    //         break;
    //       case 1:
    //         eNew = CCore::getMap()->getMapTime() > 90 ? mUNDERWORLD : mUNDERWORLDFAST;
    //         break;
    //> 90 ? mUNDERWATER : mUNDERWATERFAST;
    //         break;
    //       case 3:
    //         eNew = CCore::getMap()->getMapTime() > 90 ? mCASTLE : mCASTLEFAST;
    //         break;
    //       case 100:
    //         eNew = mNOTHING;
    //         PlayChunk(cINTERMISSION);
    //         CCore::getMap()->setLevelType(0);
    //         break;
    //       default:
    //         eNew = mNOTHING;
    //         break;
    //     }
    //   }
    // }
    //

    if(currentMusic != eNew) {
      StopMusic();
      currentMusic = eNew;
      PlayMusic();
    }
  }

	void PlayMusic()
  {
    import std.stdio;
    Mix_PlayMusic(vMusic[0], -1);
    musicStopped = false;
    // if(currentMusic != eMusic.mNOTHING) {
    //   Mix_PlayMusic(vMusic[currentMusic - 1], -1);
    //   musicStopped = false;
    // } else {
    //   StopMusic();
    // }
  }

	void PlayMusic(eMusic musicID)
  {
    if(musicID != eMusic.mNOTHING) {
      Mix_PlayMusic(vMusic[musicID - 1], -1);
      musicStopped = false;
      currentMusic = musicID;
    } else {
      StopMusic();
      currentMusic = eMusic.mNOTHING;
    }
  }

	void StopMusic()
  {
    if(!musicStopped) {
      Mix_HaltMusic();
      musicStopped = true;
    }
  }

	void PauseMusic()
  {
    if(Mix_PausedMusic() == 1) {
      Mix_ResumeMusic();
      musicStopped = false;
    } else {
      Mix_PauseMusic();
      musicStopped = true;
    }
  }

	void PlayChunk(eChunk chunkID)
  {
    Mix_VolumeChunk(vChunk[chunkID], iVolume);
    Mix_PlayChannel(-1, vChunk[chunkID], 0);
  }


	Mix_Music* loadMusic(string fileName)
  {
    fileName = "files/sounds/" ~ fileName ~ ".wav";
    return Mix_LoadMUS(toStringz(fileName));
  }

	Mix_Chunk* loadChunk(string fileName)
  {
    fileName = "files/sounds/" ~ fileName ~ ".wav";
    return Mix_LoadWAV(toStringz(fileName));
  }


	// -- get & set

	int getVolume()
  {
      	return iVolume;
  }

	void setVolume(int iVolume)
  {
    this.iVolume = iVolume;
    Mix_VolumeMusic(iVolume);
  }

};
