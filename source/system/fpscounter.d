module system.fpscounter;

import derelict.sdl2.sdl;
import drawers.text;
import std.conv;
import std.stdio;

class FpsCounter
{
  private:
    int fps = 0;
    int nfps = 0;
    ulong lFPSTime = 60;
    TextDrawer textDrawer;

  public:
    this (TextDrawer textDrawer)
    {
        this.lFPSTime = SDL_GetTicks();
        this.textDrawer = textDrawer;
    }

    void draw()
    {
        this.calculateFPS();
        string fpsText = "FPS:" ~ to!string(nfps);
        this.textDrawer.draw(fpsText, to!int(800 - this.textDrawer.getTextWidth(fpsText, 8)) - 8, 5, 8);
    }

    private void calculateFPS()
    {
      if(SDL_GetTicks() - 1000 >= lFPSTime) {
        lFPSTime = SDL_GetTicks();
        nfps = fps;
        fps = 0;
      }
      ++fps;
    }
}
