import std.stdio;

import graphics.window;
import derelict.sdl2.sdl;
import derelict.sdl2.mixer;
import derelict.sdl2.image;
import std.conv;
import std.array;
import config;
import std.string;
import gamemanager;
import std.stdio;
import std.json;
import std.file;

struct Coords
{
    int x;
    int y;
    bool direction;
}

struct GrassData
{
    Coords coords;
    alias coords this;
};

struct MinionsData
{
    enum minionsType {
        GOOMBAS
    };

    int type;
    Coords coords;

    this(minionsType type, int x, int y, bool dir)
    {
        this.coords = Coords(x, y, dir);
    }
}

class LevelHelper
{
  static:
    MinionsData createGoombas(int x, int y, bool dir)
    {
        return MinionsData(MinionsData.minionsType.GOOMBAS, x, y, dir);
    }

    private:
      MinionsData generateEnemy(MinionsData.minionsType type, int x, int y, bool dir)
      {
          return MinionsData(type, x, y, dir);
      }
}

import levels.data.leveldatainterface;

void test (LevelDataInterface t)
{

}

int main()
{

      import levels.data.level1s;

      Level1 l = new Level1();
      test(l);
      // string content = to!string(read("level1.json"));
      // JSONValue doc = parseJSON(content);
      //
      // Level newLevel = Level(to!int(doc["id"].integer), doc["title"].str());
      //
      // if (const(JSONValue)* minions = "minions" in doc) {
      //     if (minions.type == JSON_TYPE.ARRAY) {
      //         foreach (const(JSONValue) min; minions[0].array) {
      //             if (min["type"].str() == "goomba") {
      //                 newLevel.minions ~= LevelHelper.createGoombas(
      //                   to!int(min["coords"]["x"].integer),
      //                   to!int(min["coords"]["y"].integer),
      //                   to!bool(min["coords"]["dir"].integer)
      //                   );
      //             }
      //         }
      //     }
      // }

      // // Load the SDL 2 library.
      DerelictSDL2.load();
      DerelictSDL2Mixer.load();
      DerelictSDL2Image.load();

      SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

      Window myWindow = new Window(GameConfig.TITLE);

      // Check that the window was successfully created
      if (!myWindow.get()) {
          // In the case that the window could not be made...
          writefln("Could not create window: %s\n", SDL_GetError());
          return 1;
      }

      GameManager gameManager = new GameManager(myWindow);
      gameManager.mainLoop();

      // Close and destroy the window
      SDL_DestroyWindow(myWindow.get());

      // Clean up
      SDL_Quit();

    return 0;
}
